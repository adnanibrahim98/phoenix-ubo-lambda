package com.be.explorer.ownership.lambda.entitysearch.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class ShareholdingDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String shareHolderName;
	private Double noOfShares;
	private Double totalShareValuePer;
	private String identifier;

	public String getShareHolderName() {
		return shareHolderName;
	}

	public void setShareHolderName(String shareHolderName) {
		this.shareHolderName = shareHolderName;
	}

	public Double getNoOfShares() {
		return noOfShares;
	}

	public void setNoOfShares(Double noOfShares) {
		this.noOfShares = noOfShares;
	}

	public Double getTotalShareValuePer() {
		return totalShareValuePer;
	}

	public void setTotalShareValuePer(Double totalShareValuePer) {
		this.totalShareValuePer = totalShareValuePer;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}
