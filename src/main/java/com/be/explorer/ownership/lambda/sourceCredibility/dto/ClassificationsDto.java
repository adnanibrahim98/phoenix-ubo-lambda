package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author suresh
 *
 */
public class ClassificationsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long classificationId;

	private String classifcationName;

	private SourcesHideStatusDto hideStatusDto;

	private List<SubClassificationsDto> subClassifications;

	public ClassificationsDto() {
	}

	public ClassificationsDto(Long classificationId, String classifcationName) {
		super();
		this.classificationId = classificationId;
		this.classifcationName = classifcationName;
	}

	public Long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(Long classificationId) {
		this.classificationId = classificationId;
	}

	public String getClassifcationName() {
		return classifcationName;
	}

	public void setClassifcationName(String classifcationName) {
		this.classifcationName = classifcationName;
	}

	public List<SubClassificationsDto> getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(List<SubClassificationsDto> subClassifications) {
		this.subClassifications = subClassifications;
	}

	public SourcesHideStatusDto getHideStatusDto() {
		return hideStatusDto;
	}

	public void setHideStatusDto(SourcesHideStatusDto hideStatusDto) {
		this.hideStatusDto = hideStatusDto;
	}

}
