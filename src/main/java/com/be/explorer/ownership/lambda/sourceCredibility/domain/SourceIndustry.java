package com.be.explorer.ownership.lambda.sourceCredibility.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_industry")
public class SourceIndustry implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long industryId;

	@Column(name = "industryName")
	private String industryName;

	public SourceIndustry() {
	}

	public SourceIndustry(String industryName) {
		super();
		this.industryName = industryName;
	}

	public Long getIndustryId() {
		return industryId;
	}

	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

}
