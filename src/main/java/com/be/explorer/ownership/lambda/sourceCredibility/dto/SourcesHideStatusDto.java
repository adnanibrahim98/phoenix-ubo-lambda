package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class SourcesHideStatusDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long sourcesHideStatusId;

	private Boolean visible;

	private Boolean hideLink;

	private Boolean hideDomain;

	private Boolean hideIndustry;

	private Boolean hideCategory;

	private Boolean hideJurisdiction;

	private Boolean hideMedia;

	private Long sourceId;

	private Long classificationId;

	public SourcesHideStatusDto() {
	}

	public SourcesHideStatusDto(Boolean visible, Boolean hideLink, Boolean hideDomain, Boolean hideIndustry,
			Boolean hideCategory, Boolean hideJurisdiction, Long sourceId, Long classificationId) {
		super();
		this.visible = visible;
		this.hideLink = hideLink;
		this.hideDomain = hideDomain;
		this.hideIndustry = hideIndustry;
		this.hideCategory = hideCategory;
		this.hideJurisdiction = hideJurisdiction;
		this.sourceId = sourceId;
		this.classificationId = classificationId;
	}
	
	public SourcesHideStatusDto(Boolean visible, Boolean hideLink, Boolean hideDomain, Boolean hideIndustry,
			Boolean hideCategory, Boolean hideJurisdiction, Long sourceId, Long classificationId,Boolean hideMedia) {
		super();
		this.visible = visible;
		this.hideLink = hideLink;
		this.hideDomain = hideDomain;
		this.hideIndustry = hideIndustry;
		this.hideCategory = hideCategory;
		this.hideJurisdiction = hideJurisdiction;
		this.sourceId = sourceId;
		this.classificationId = classificationId;
		this.hideMedia = hideMedia;
	}

	public Long getSourcesHideStatusId() {
		return sourcesHideStatusId;
	}

	public void setSourcesHideStatusId(Long sourcesHideStatusId) {
		this.sourcesHideStatusId = sourcesHideStatusId;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Boolean getHideLink() {
		return hideLink;
	}

	public void setHideLink(Boolean hideLink) {
		this.hideLink = hideLink;
	}

	public Boolean getHideDomain() {
		return hideDomain;
	}

	public void setHideDomain(Boolean hideDomain) {
		this.hideDomain = hideDomain;
	}

	public Boolean getHideIndustry() {
		return hideIndustry;
	}

	public void setHideIndustry(Boolean hideIndustry) {
		this.hideIndustry = hideIndustry;
	}

	public Boolean getHideCategory() {
		return hideCategory;
	}

	public void setHideCategory(Boolean hideCategory) {
		this.hideCategory = hideCategory;
	}

	public Boolean getHideJurisdiction() {
		return hideJurisdiction;
	}

	public void setHideJurisdiction(Boolean hideJurisdiction) {
		this.hideJurisdiction = hideJurisdiction;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(Long classificationId) {
		this.classificationId = classificationId;
	}

	public Boolean getHideMedia() {
		return hideMedia;
	}

	public void setHideMedia(Boolean hideMedia) {
		this.hideMedia = hideMedia;
	}

}
