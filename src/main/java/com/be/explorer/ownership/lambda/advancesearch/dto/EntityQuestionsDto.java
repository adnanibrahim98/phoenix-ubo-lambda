package com.be.explorer.ownership.lambda.advancesearch.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rambabu
 *
 */
public class EntityQuestionsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	@JsonProperty(required = true)
	private String questionName;

	private String questionAnswer;

	@JsonProperty(required = true)
	private String groupName;

	private Date createdDate;

	private Long createdBy;
	
	private String userName;

	@JsonProperty(required = true)
	private String entityId;
	
	@JsonProperty(required = true)
	private String questionDivId;
	
	private String questionOptions;
	
	private String questionType;
	
	private String schemaAttribute;
	
	private String schemaAttributeValue;
	
	private Long surveyId;
	

	public EntityQuestionsDto() {
		super();
	}

	public EntityQuestionsDto(String questionName, String questionAnswer, String groupName, Date createdDate,
			Long createdBy, String entityId,String userName,String questionDivId,String questionOptions,String questionType,String schemaAttribute,String schemaAttributeValue,Long surveyId) {
		super();
		this.questionName = questionName;
		this.questionAnswer = questionAnswer;
		this.groupName = groupName;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.entityId = entityId;
		this.userName = userName;
		this.questionDivId = questionDivId;
		this.questionOptions=questionOptions;
		this.questionType=questionType;
		this.schemaAttribute = schemaAttribute;
		this.schemaAttributeValue=schemaAttributeValue;
		this.surveyId=surveyId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getQuestionAnswer() {
		return questionAnswer;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setQuestionAnswer(String questionAnswer) {
		this.questionAnswer = questionAnswer;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getQuestionDivId() {
		return questionDivId;
	}

	public void setQuestionDivId(String questionDivId) {
		this.questionDivId = questionDivId;
	}

	public String getQuestionOptions() {
		return questionOptions;
	}

	public void setQuestionOptions(String questionOptions) {
		this.questionOptions = questionOptions;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getSchemaAttribute() {
		return schemaAttribute;
	}

	public void setSchemaAttribute(String schemaAttribute) {
		this.schemaAttribute = schemaAttribute;
	}

	public String getSchemaAttributeValue() {
		return schemaAttributeValue;
	}

	public void setSchemaAttributeValue(String schemaAttributeValue) {
		this.schemaAttributeValue = schemaAttributeValue;
	}

	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	
	
	
}
