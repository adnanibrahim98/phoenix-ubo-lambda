package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.ClassificationsDto;



public interface ClassificationCloudService {
	
	public List<ClassificationsDto> getClassifications(String type);

}
