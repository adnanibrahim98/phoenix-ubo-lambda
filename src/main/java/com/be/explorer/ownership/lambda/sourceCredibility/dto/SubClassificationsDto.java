package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;
import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.enumType.CredibilityEnums;



/**
 * @author suresh
 *
 */
public class SubClassificationsDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long subClassificationId;

	private String subClassifcationName;

	private CredibilityEnums subClassificationCredibility;

	private List<DataAttributesDto> dataAttributes;

	public Long getSubClassificationId() {
		return subClassificationId;
	}

	public void setSubClassificationId(Long subClassificationId) {
		this.subClassificationId = subClassificationId;
	}

	public String getSubClassifcationName() {
		return subClassifcationName;
	}

	public void setSubClassifcationName(String subClassifcationName) {
		this.subClassifcationName = subClassifcationName;
	}

	public List<DataAttributesDto> getDataAttributes() {
		return dataAttributes;
	}

	public void setDataAttributes(List<DataAttributesDto> dataAttributes) {
		this.dataAttributes = dataAttributes;
	}

	public CredibilityEnums getSubClassificationCredibility() {
		return subClassificationCredibility;
	}

	public void setSubClassificationCredibility(CredibilityEnums subClassificationCredibility) {
		this.subClassificationCredibility = subClassificationCredibility;
	}

}
