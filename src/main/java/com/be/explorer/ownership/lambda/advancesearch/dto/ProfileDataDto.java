package com.be.explorer.ownership.lambda.advancesearch.dto;

import java.util.List;

public class ProfileDataDto {
	
	private String keyword;

	private String role;
	
	private String dateOfBirth;
	
	private List<ProfileResponseDto> profileResponseDto;
	
	private String status;

	public List<ProfileResponseDto> getProfileResponseDto() {
		return profileResponseDto;
	}

	public void setProfileResponseDto(List<ProfileResponseDto> profileResponseDto) {
		this.profileResponseDto = profileResponseDto;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
