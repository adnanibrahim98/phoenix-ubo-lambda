package com.be.explorer.ownership.lambda.sourceCredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.be.explorer.ownership.lambda.rest.util.ServiceCallHelper;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.DataAttributesCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.ClassificationCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.DataAttributesCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.ClassificationsDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.DataAttributesDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SubClassificationsDto;
@PropertySource(value = "classpath:application.properties")
@Service
public class ClassificationsCloudServiceImpl implements  ClassificationCloudService{
	
	/*
	 * @Autowired Environment env;
	 */

	@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL;
	
	@Value("${client_id}")
	private String clientId;
	
	private static String genericEntity="listitems";
	
	@Autowired
	private DataAttributesCloudService dataAttributesCloudService;
	
	@Override
	public List<ClassificationsDto> getClassifications(String type) {
		List<ClassificationsDto> classificationDtoList = new ArrayList<>();
		List<DataAttributesCloudDto> allList = dataAttributesCloudService.getDatAttributes();
		String url = SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?item_type=";
		url=url+type;
		JSONArray credibility = new JSONArray();
		try {
		String getResponse=getCloudApiData(url);
		if(getResponse!=null) {
			JSONObject getResponseJson=new JSONObject(getResponse);
			if(getResponseJson.has("data")) {
				JSONArray data =getResponseJson.getJSONArray("data");
					for (int i = 0; i < data.length(); i++) {
						ClassificationsDto classificationDto = new ClassificationsDto();
						JSONObject singleJson = new JSONObject();
						singleJson = data.getJSONObject(i);
						if (singleJson.has("item_id") && singleJson.get("item_id") != null) {
							classificationDto.setClassificationId(singleJson.getLong("item_id")) ;
						}
						if (singleJson.has("name") && singleJson.get("name") != null) {
							classificationDto.setClassifcationName(singleJson.getString("name")) ;
						}
						classificationDto.setHideStatusDto(null);
						List<SubClassificationsDto> subClassificationDtoList= new ArrayList<>();
						if (singleJson.has("sub_items") && singleJson.get("sub_items") !=null) {
							JSONArray subClassArray= new JSONArray();
							subClassArray=singleJson.getJSONArray("sub_items");
							for(int j=0;j<subClassArray.length();j++) {
								SubClassificationsDto sub_class= new SubClassificationsDto();
								JSONObject subClassJson= new JSONObject();
								subClassJson=subClassArray.getJSONObject(j);
								if (subClassJson.has("item_id"))
									sub_class.setSubClassificationId(subClassJson.getLong("item_id"));
								if (subClassJson.has("name"))
									sub_class.setSubClassifcationName(subClassJson.getString("name"));
								List<DataAttributesDto> attributeList = new ArrayList<>();
								for (DataAttributesCloudDto dto1 : allList) {
									DataAttributesDto dto = new DataAttributesDto();
									if (dto1.getSubClassifications().getSubClassificationId()
											.equals(subClassJson.getLong("item_id"))) {
										dto.setAttributeId(dto1.getAttributeId());
										dto.setSourceAttributeName(dto1.getSourceAttributeName());
										dto.setSourceAttributeSchema(dto1.getSourceAttributeSchema());
										dto.setSourceAttributeSchemaGroup(dto1.getSourceAttributeSchemaGroup());
										attributeList.add(dto);
									}
								}
								sub_class.setDataAttributes(attributeList);
								sub_class.setSubClassificationCredibility(null);
								subClassificationDtoList.add(sub_class);
							}
							
						}
						classificationDto.setSubClassifications(subClassificationDtoList);
						classificationDtoList.add(classificationDto);
					}
			}
		}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return classificationDtoList;
	}
	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("data")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}

}
