package com.be.explorer.ownership.lambda.sourceCredibility.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_domain")
public class SourceDomain implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long domainId;

	@Column(name = "domainName")
	private String domainName;

	public SourceDomain() {
	}

	public SourceDomain(String domainName) {
		super();
		this.domainName = domainName;
	}

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
