package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceMediaDto;



public interface MediaCloudService {
	
	public List<SourceMediaDto> getSourceMedia(String type);
	
}
