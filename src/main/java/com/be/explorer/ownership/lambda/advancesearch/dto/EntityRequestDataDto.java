package com.be.explorer.ownership.lambda.advancesearch.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EntityRequestDataDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String identifier;

	private String jurisdiction;

	private String keyword;

	@JsonProperty("light-weight")
	private String light_weight;

	private String searchType;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getLight_weight() {
		return light_weight;
	}

	public void setLight_weight(String light_weight) {
		this.light_weight = light_weight;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

}
