package com.be.explorer.ownership.lambda.advancesearch.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.CreationTimestamp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Rambabu
 *
 */
@ApiModel("entity_questions")
@Entity
@Table(name = "entity_questions")
public class EntityQuestions implements Serializable {

	private static final long serialVersionUID = 990560358470513738L;

	@ApiModelProperty(value = "The ID of question")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "question_id")
	private Long id;

	@ApiModelProperty(value = "question name")
	@Column(name = "question_name", columnDefinition = "LONGTEXT")
	private String questionName;

	@ApiModelProperty(value = "question answer")
	@Column(name = "question_answer", columnDefinition = "LONGTEXT")
	private String questionAnswer;

	@ApiModelProperty(value = "gorupName answer")
	@Column(name = "group_name")
	private String groupName;

	@CreationTimestamp
	@Column(name = "created_date")
	private Date createdDate;

	@ApiModelProperty(value = "created by")
	@Column(name = "created_by")
	private Long createdBy;

	@ApiModelProperty(value = "entity id")
	@Column(name = "entity_id")
	private String entityId;
	
	@ApiModelProperty(value = "question div id")
	@Column(name = "question_div_id")
	private String questionDivId;
	
	@ApiModelProperty(value = "Options of the questions")
	@Column(name = "question_options",columnDefinition = "LONGTEXT")
	private String questionOptions;
	
	@ApiModelProperty(value = "Type of the question")
	@Column(name = "question_type")
	private String questionType;
	
	@ApiModelProperty(value = "Schema attribute name")
	@Column(name = "schema_attribute")
	private String schemaAttribute;
	
	@ApiModelProperty(value = "Schema attribute value")
	@Column(name = "schema_attribute_value")
	private String schemaAttributeValue;
	
	@ApiModelProperty(value = "Survey Id")
	@Column(name = "survey_id")
	private Long surveyId;

	public EntityQuestions() {
		super();
	}

	public EntityQuestions(String questionName, String questionAnswer, String groupName, Date createdDate,
			Long createdBy, String entityId,String questionDivId,String questionOptions,String questionType,String schemaAttribute,String schemaAttributeValue,Long surveyId) {
		super();
		this.questionName = questionName;
		this.questionAnswer = questionAnswer;
		this.groupName = groupName;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.entityId = entityId;
		this.questionDivId = questionDivId;
		this.questionOptions=questionOptions;
		this.questionType=questionType;
		this.schemaAttribute=schemaAttribute;
		this.schemaAttributeValue=schemaAttributeValue;
		this.surveyId=surveyId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getQuestionAnswer() {
		return questionAnswer;
	}

	public void setQuestionAnswer(String questionAnswer) {
		this.questionAnswer = questionAnswer;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}	

	public String getQuestionDivId() {
		return questionDivId;
	}

	public void setQuestionDivId(String questionDivId) {
		this.questionDivId = questionDivId;
	}
	
	public String getQuestionOptions() {
		return questionOptions;
	}

	public void setQuestionOptions(String questionOptions) {
		this.questionOptions = questionOptions;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getSchemaAttribute() {
		return schemaAttribute;
	}

	public void setSchemaAttribute(String schemaAttribute) {
		this.schemaAttribute = schemaAttribute;
	}

	public String getSchemaAttributeValue() {
		return schemaAttributeValue;
	}

	public void setSchemaAttributeValue(String schemaAttributeValue) {
		this.schemaAttributeValue = schemaAttributeValue;
	}

	
	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
