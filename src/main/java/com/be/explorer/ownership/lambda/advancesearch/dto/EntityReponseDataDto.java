package com.be.explorer.ownership.lambda.advancesearch.dto;

import java.io.Serializable;

public class EntityReponseDataDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String officership;

	private String shareholders;

	private String ownedBy;

	private String ownership;

	private String subsidiaries;

	private String identifier;

	private String keyword;

	public String getOfficership() {
		return officership;
	}

	public void setOfficership(String officership) {
		this.officership = officership;
	}

	public String getShareholders() {
		return shareholders;
	}

	public void setShareholders(String shareholders) {
		this.shareholders = shareholders;
	}

	public String getOwnedBy() {
		return ownedBy;
	}

	public void setOwnedBy(String ownedBy) {
		this.ownedBy = ownedBy;
	}

	public String getOwnership() {
		return ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getSubsidiaries() {
		return subsidiaries;
	}

	public void setSubsidiaries(String subsidiaries) {
		this.subsidiaries = subsidiaries;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
