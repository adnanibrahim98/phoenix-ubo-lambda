package com.be.explorer.ownership.lambda.sourceCredibility.cloudDto;

import java.io.Serializable;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.DataAttributesDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SubClassificationsDto;
import com.be.explorer.ownership.lambda.sourceCredibility.enumType.CredibilityEnums;



public class CredibilityCloudDto implements Serializable {

	private Long sourceCredibilityId;

	private Long sourceId;

	private SubClassificationsDto subClassifications;

	private DataAttributesDto dataAttributes;

	private CredibilityEnums credibility;

	public Long getSourceCredibilityId() {
		return sourceCredibilityId;
	}

	public void setSourceCredibilityId(Long sourceCredibilityId) {
		this.sourceCredibilityId = sourceCredibilityId;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public SubClassificationsDto getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(SubClassificationsDto subClassifications) {
		this.subClassifications = subClassifications;
	}

	public DataAttributesDto getDataAttributes() {
		return dataAttributes;
	}

	public void setDataAttributes(DataAttributesDto dataAttributes) {
		this.dataAttributes = dataAttributes;
	}

	public CredibilityEnums getCredibility() {
		return credibility;
	}

	public void setCredibility(CredibilityEnums credibility) {
		this.credibility = credibility;
	}
}
