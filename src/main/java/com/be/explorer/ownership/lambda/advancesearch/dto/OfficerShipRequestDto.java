package com.be.explorer.ownership.lambda.advancesearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OfficerShipRequestDto {

	private Long size;
	
	@JsonProperty("vcard:hasName")
	private String vcardhasName;
	
	@JsonProperty("vcard:hasAddress")
	private String vcardhasAddress;
	
	private String keyword;
	
	private String role;
	
	private String dateOfBirth;

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getVcardhasName() {
		return vcardhasName;
	}

	public void setVcardhasName(String vcardhasName) {
		this.vcardhasName = vcardhasName;
	}

	public String getVcardhasAddress() {
		return vcardhasAddress;
	}

	public void setVcardhasAddress(String vcardhasAddress) {
		this.vcardhasAddress = vcardhasAddress;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
}
