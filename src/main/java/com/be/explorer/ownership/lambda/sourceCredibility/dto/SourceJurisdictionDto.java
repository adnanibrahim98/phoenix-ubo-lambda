package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class SourceJurisdictionDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long jurisdictionId;

	private String jurisdictionName;

	private String jurisdictionOriginalName;

	private Boolean selected;

	public SourceJurisdictionDto() {
	}

	public SourceJurisdictionDto(String jurisdictionName) {
		super();
		this.jurisdictionName = jurisdictionName;
	}

	public Long getJurisdictionId() {
		return jurisdictionId;
	}

	public void setJurisdictionId(Long jurisdictionId) {
		this.jurisdictionId = jurisdictionId;
	}

	public String getJurisdictionName() {
		return jurisdictionName;
	}

	public void setJurisdictionName(String jurisdictionName) {
		this.jurisdictionName = jurisdictionName;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getJurisdictionOriginalName() {
		return jurisdictionOriginalName;
	}

	public void setJurisdictionOriginalName(String jurisdictionOriginalName) {
		this.jurisdictionOriginalName = jurisdictionOriginalName;
	}

}
