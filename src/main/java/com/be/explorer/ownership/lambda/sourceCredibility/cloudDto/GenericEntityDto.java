package com.be.explorer.ownership.lambda.sourceCredibility.cloudDto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class GenericEntityDto implements Serializable {
	
	private boolean allowDelete;
	
	private String clientId;
	
	private String name;
	
	private String code;
	
	private Long color;
	
	private Long icon;
	
	private Long itemId;
	
	private List<GenericEntityDto> subItems;
	
	private String type;
	
	private String parentId;

	public boolean isAllowDelete() {
		return allowDelete;
	}

	public void setAllowDelete(boolean allowDelete) {
		this.allowDelete = allowDelete;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<GenericEntityDto> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<GenericEntityDto> subItems) {
		this.subItems = subItems;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}



	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public Long getColor() {
		return color;
	}

	public void setColor(Long color) {
		this.color = color;
	}

	public Long getIcon() {
		return icon;
	}

	public void setIcon(Long icon) {
		this.icon = icon;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
}
