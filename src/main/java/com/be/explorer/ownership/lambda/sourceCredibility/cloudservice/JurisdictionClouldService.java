package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceJurisdictionDto;



public interface JurisdictionClouldService {
 
	public List<SourceJurisdictionDto> getSourceJurisdiction();
}
