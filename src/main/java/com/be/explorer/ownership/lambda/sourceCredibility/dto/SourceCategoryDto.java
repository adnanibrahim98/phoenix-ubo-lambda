package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;

/*
 * @author Prateek Maurya
 */
public class SourceCategoryDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long categoryId;

	private String categoryName;
	
	public SourceCategoryDto(){
		
	}
	
	public SourceCategoryDto(String categoryName){
		super();
		this.categoryName = categoryName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
}
