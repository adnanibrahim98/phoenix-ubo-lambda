package com.be.explorer.ownership.lambda.sourceCredibility.cloudDto;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourcesDto;



public class SourcesPaginationDto {
	List<SourcesDto> sourceList;
	Long totalRecord;
	public List<SourcesDto> getSourceList() {
		return sourceList;
	}
	public void setSourceList(List<SourcesDto> sourceList) {
		this.sourceList = sourceList;
	}
	public Long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Long totalRecord) {
		this.totalRecord = totalRecord;
	}
	
	

}
