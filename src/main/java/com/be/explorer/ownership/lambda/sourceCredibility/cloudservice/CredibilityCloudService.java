package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.CredibilityCloudDto;



public interface CredibilityCloudService {

	public List<CredibilityCloudDto> getCredibility();

	public Boolean saveCredibility(CredibilityCloudDto credibilityDto);
	
	public Boolean updateCredibility(CredibilityCloudDto credibilityDto);
	
	public Boolean deleteCredibilityById(Long id);
}
