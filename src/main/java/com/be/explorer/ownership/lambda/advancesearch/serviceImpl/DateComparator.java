package com.be.explorer.ownership.lambda.advancesearch.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.json.JSONObject;

public class DateComparator implements Comparator<JSONObject> {

	@Override
	public int compare(JSONObject firstJson, JSONObject secondJson) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date firstDate = null;
		Date secondDate = new Date();
		String firstValue=null;
		String secondValue =null;
		if(firstJson.has("from"))
			 firstValue = firstJson.getString("from");
		else
			 firstValue = firstJson.getString("date");

		if(firstJson.has("from"))
			 secondValue = secondJson.getString("from");
		else
			 secondValue = secondJson.getString("date");

		try {
			firstDate = sdf.parse(firstValue);
			secondDate = sdf.parse(secondValue);

		} catch (ParseException e) {
			//ElementLogger.log(ElementLoggerLevel.ERROR, e.getMessage(), this.getClass());
		}

		return secondDate.compareTo(firstDate);
	}

}
