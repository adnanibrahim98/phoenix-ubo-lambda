package com.be.explorer.ownership.lambda.advancesearch.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CustomShareHolder {

	private String childIdentifier;

	private String entityName;

	private String identifier;

	private String jurisdiction;

	private String from;

	private String source;

	private String natureOfControl;

	private Double totalPercentage;

	private String streetAddress;

	private String city;

	private String country;

	private String zip;

	private String fullAddress;

	private String hasURL;

	private String entityType;

	private String childLevel;

	private String dateOfBirth;

	private String sourceUrl;

	private String role;

	private String classification;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getNatureOfControl() {
		return natureOfControl;
	}

	public void setNatureOfControl(String natureOfControl) {
		this.natureOfControl = natureOfControl;
	}

	public Double getTotalPercentage() {
		return totalPercentage;
	}

	public void setTotalPercentage(Double totalPercentage) {
		this.totalPercentage = totalPercentage;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(String jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getChildIdentifier() {
		return childIdentifier;
	}

	public void setChildIdentifier(String childIdentifier) {
		this.childIdentifier = childIdentifier;
	}

	public String getChildLevel() {
		return childLevel;
	}

	public void setChildLevel(String childLevel) {
		this.childLevel = childLevel;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getHasURL() {
		return hasURL;
	}

	public void setHasURL(String hasURL) {
		this.hasURL = hasURL;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	

	



}
