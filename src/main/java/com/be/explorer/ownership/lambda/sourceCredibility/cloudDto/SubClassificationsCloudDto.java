package com.be.explorer.ownership.lambda.sourceCredibility.cloudDto;

import java.io.Serializable;
import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.enumType.CredibilityEnums;



public class SubClassificationsCloudDto implements Serializable {


	private static final long serialVersionUID = 1L;

	private Long subClassificationId;

	private String subClassifcationName;

	private CredibilityEnums subClassificationCredibility;

	private List<DataAttributeForSourceByIdDto> dataAttributes;
	
	private Long sourceCredibilityId;

	public Long getSubClassificationId() {
		return subClassificationId;
	}

	public void setSubClassificationId(Long subClassificationId) {
		this.subClassificationId = subClassificationId;
	}

	public String getSubClassifcationName() {
		return subClassifcationName;
	}

	public void setSubClassifcationName(String subClassifcationName) {
		this.subClassifcationName = subClassifcationName;
	}
	
	public List<DataAttributeForSourceByIdDto> getDataAttributes() {
		return dataAttributes;
	}

	public void setDataAttributes(List<DataAttributeForSourceByIdDto> dataAttributes) {
		this.dataAttributes = dataAttributes;
	}

	public CredibilityEnums getSubClassificationCredibility() {
		return subClassificationCredibility;
	}

	public void setSubClassificationCredibility(CredibilityEnums subClassificationCredibility) {
		this.subClassificationCredibility = subClassificationCredibility;
	}

	public Long getSourceCredibilityId() {
		return sourceCredibilityId;
	}

	public void setSourceCredibilityId(Long sourceCredibilityId) {
		this.sourceCredibilityId = sourceCredibilityId;
	}
	
}
