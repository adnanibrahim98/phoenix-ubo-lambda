package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceIndustryDto;



public interface IndustryCloudSerive {
	
	public List<SourceIndustryDto> getInsdustrySource(String type);

}
