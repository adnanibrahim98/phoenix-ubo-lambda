package com.be.explorer.ownership.lambda.advancesearch.dto;

public class ShareholderLinks {
	
	private ShareHoldersDto shareHolders;
	
	private String officership;
	
	private String overview;
		
	private String subsidiaries;

	public ShareHoldersDto getShareHolders() {
		return shareHolders;
	}

	public void setShareHolders(ShareHoldersDto shareHolders) {
		this.shareHolders = shareHolders;
	}

	public String getOfficership() {
		return officership;
	}

	public void setOfficership(String officership) {
		this.officership = officership;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getSubsidiaries() {
		return subsidiaries;
	}

	public void setSubsidiaries(String subsidiaries) {
		this.subsidiaries = subsidiaries;
	}


}
