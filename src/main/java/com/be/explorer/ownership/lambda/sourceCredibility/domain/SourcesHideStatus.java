package com.be.explorer.ownership.lambda.sourceCredibility.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_sources_hide_status")
public class SourcesHideStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long sourcesHideStatusId;

	@Column(name = "visible")
	private Boolean visible;

	@Column(name = "hideLink")
	private Boolean hideLink;

	@Column(name = "hideDomain")
	private Boolean hideDomain;

	@Column(name = "hideIndustry")
	private Boolean hideIndustry;

	@Column(name = "hideMedia")
	private Boolean hideMedia;

	@Column(name = "hideJurisdiction")
	private Boolean hideJurisdiction;

	@Column(name = "sourceId")
	private Long sourceId;

	@Column(name = "classificationId")
	private Long classificationId;

	public SourcesHideStatus() {
	}

	public SourcesHideStatus(Boolean visible, Boolean hideLink, Boolean hideDomain, Boolean hideIndustry,
			Boolean hideJurisdiction, Long sourceId, Long classificationId) {
		super();
		this.visible = visible;
		this.hideLink = hideLink;
		this.hideDomain = hideDomain;
		this.hideIndustry = hideIndustry;
		this.hideJurisdiction = hideJurisdiction;
		this.sourceId = sourceId;
		this.classificationId = classificationId;
	}
	
	public SourcesHideStatus(Boolean visible, Boolean hideLink, Boolean hideDomain, Boolean hideIndustry,
			Boolean hideMedia, Boolean hideJurisdiction, Long sourceId, Long classificationId) {
		super();
		this.visible = visible;
		this.hideLink = hideLink;
		this.hideDomain = hideDomain;
		this.hideIndustry = hideIndustry;
		this.hideMedia = hideMedia;
		this.hideJurisdiction = hideJurisdiction;
		this.sourceId = sourceId;
		this.classificationId = classificationId;
	}

	public Long getSourcesHideStatusId() {
		return sourcesHideStatusId;
	}

	public void setSourcesHideStatusId(Long sourcesHideStatusId) {
		this.sourcesHideStatusId = sourcesHideStatusId;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Boolean getHideLink() {
		return hideLink;
	}

	public void setHideLink(Boolean hideLink) {
		this.hideLink = hideLink;
	}

	public Boolean getHideDomain() {
		return hideDomain;
	}

	public void setHideDomain(Boolean hideDomain) {
		this.hideDomain = hideDomain;
	}

	public Boolean getHideIndustry() {
		return hideIndustry;
	}

	public void setHideIndustry(Boolean hideIndustry) {
		this.hideIndustry = hideIndustry;
	}

	public Boolean getHideJurisdiction() {
		return hideJurisdiction;
	}

	public void setHideJurisdiction(Boolean hideJurisdiction) {
		this.hideJurisdiction = hideJurisdiction;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(Long classificationId) {
		this.classificationId = classificationId;
	}

	public Boolean getHideMedia() {
		return hideMedia;
	}

	public void setHideMedia(Boolean hideMedia) {
		this.hideMedia = hideMedia;
	}

}
