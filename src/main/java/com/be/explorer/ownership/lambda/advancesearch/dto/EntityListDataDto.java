package com.be.explorer.ownership.lambda.advancesearch.dto;

import java.io.Serializable;
import java.util.List;

public class EntityListDataDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<EntityReponseDataDto> entityReponseDataDtoList;

	public List<EntityReponseDataDto> getEntityReponseDataDtoList() {
		return entityReponseDataDtoList;
	}

	public void setEntityReponseDataDtoList(List<EntityReponseDataDto> entityReponseDataDtoList) {
		this.entityReponseDataDtoList = entityReponseDataDtoList;
	}

}
