package com.be.explorer.ownership.lambda.sourceCredibility.cloudDto;

public class SaveDataAttributeCloudDto {
	

	private Long attributeId;
	
	private String sourceAttributeName;

	private String sourceAttributeSchemaGroup;

	private String sourceAttributeSchema;
	
	private Long subClassifications;
	
	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public String getSourceAttributeName() {
		return sourceAttributeName;
	}

	public void setSourceAttributeName(String sourceAttributeName) {
		this.sourceAttributeName = sourceAttributeName;
	}

	public String getSourceAttributeSchemaGroup() {
		return sourceAttributeSchemaGroup;
	}

	public void setSourceAttributeSchemaGroup(String sourceAttributeSchemaGroup) {
		this.sourceAttributeSchemaGroup = sourceAttributeSchemaGroup;
	}

	public String getSourceAttributeSchema() {
		return sourceAttributeSchema;
	}

	public void setSourceAttributeSchema(String sourceAttributeSchema) {
		this.sourceAttributeSchema = sourceAttributeSchema;
	}

	public Long getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(Long subClassifications) {
		this.subClassifications = subClassifications;
	}
	
	
}
