package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author suresh
 *
 */
public class SourcesDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long sourceId;

	private String sourceName;

	private String sourceUrl;

	private String sourceDisplayName;

	private String entityId;

	private String sourceType;

	private Date sourceCreatedDate;

	private List<SourceDomainDto> sourceDomain;

	private List<SourceIndustryDto> sourceIndustry;

	private List<SourceJurisdictionDto> sourceJurisdiction;

	private List<ClassificationsDto> classifications;

	private List<SourceMediaDto> sourceMedia;

	private boolean deleteMedia;
	
	private String category;

	public SourcesDto() {
	}

	
	public SourcesDto(Long sourceId, String sourceName, String sourceUrl,String category) {
		super();
		this.sourceId = sourceId;
		this.sourceName = sourceName;
		this.sourceUrl = sourceUrl;
		this.category=category;
	}


	public Long getSourceId() {
		return sourceId;
	}

	
	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getSourceDisplayName() {
		return sourceDisplayName;
	}

	public void setSourceDisplayName(String sourceDisplayName) {
		this.sourceDisplayName = sourceDisplayName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public List<SourceDomainDto> getSourceDomain() {
		return sourceDomain;
	}

	public void setSourceDomain(List<SourceDomainDto> sourceDomain) {
		this.sourceDomain = sourceDomain;
	}

	public List<SourceIndustryDto> getSourceIndustry() {
		return sourceIndustry;
	}

	public void setSourceIndustry(List<SourceIndustryDto> sourceIndustry) {
		this.sourceIndustry = sourceIndustry;
	}

	public List<SourceJurisdictionDto> getSourceJurisdiction() {
		return sourceJurisdiction;
	}

	public void setSourceJurisdiction(List<SourceJurisdictionDto> sourceJurisdiction) {
		this.sourceJurisdiction = sourceJurisdiction;
	}

	public List<ClassificationsDto> getClassifications() {
		return classifications;
	}

	public void setClassifications(List<ClassificationsDto> classifications) {
		this.classifications = classifications;
	}

	public List<SourceMediaDto> getSourceMedia() {
		return sourceMedia;
	}

	public void setSourceMedia(List<SourceMediaDto> sourceMedia) {
		this.sourceMedia = sourceMedia;
	}

	public Date getSourceCreatedDate() {
		return sourceCreatedDate;
	}

	public void setSourceCreatedDate(Date sourceCreatedDate) {
		this.sourceCreatedDate = sourceCreatedDate;
	}

	public boolean isDeleteMedia() {
		return deleteMedia;
	}

	public void setDeleteMedia(boolean deleteMedia) {
		this.deleteMedia = deleteMedia;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}

}
