package com.be.explorer.ownership.lambda.advancesearch.serviceImpl;

import java.util.Comparator;

import org.json.JSONObject;

public class NewsComparator implements Comparator<JSONObject> {

	@Override
	public int compare(JSONObject firstJson, JSONObject secondJson) {
		Integer firstValue = 0;
		Integer secondValue = 0;
		if (firstJson.has("credibility"))
			firstValue = firstJson.getInt("credibility");
		if (firstJson.has("credibility"))
			secondValue = secondJson.getInt("credibility");
		return secondValue.compareTo(firstValue);
	}

}
