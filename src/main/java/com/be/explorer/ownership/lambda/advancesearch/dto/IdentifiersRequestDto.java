package com.be.explorer.ownership.lambda.advancesearch.dto;

public class IdentifiersRequestDto {

	private String identifier;
	
	private String keyword;
	
	private String response;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
