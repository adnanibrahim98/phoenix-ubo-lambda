package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceDomainDto;



public interface DomainCloudService {

	public List<SourceDomainDto> getSourceDomain(String type);
}
