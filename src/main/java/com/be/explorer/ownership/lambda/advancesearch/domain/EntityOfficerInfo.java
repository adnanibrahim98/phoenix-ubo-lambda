package com.be.explorer.ownership.lambda.advancesearch.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/* @author Prateek Maurya
 *
 */
@ApiModel("entity_officers")
@Entity
@Table(name = "entity_officers")
public class EntityOfficerInfo {

	@ApiModelProperty(value = "The ID of Entity Officier")
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ApiModelProperty(value = "main entity identifier")
	@Column(name = "entity_id")
	private String entityId;

	@ApiModelProperty(value = "officer name")
	@Column(name = "officer_name")
	private String officerName;

	@ApiModelProperty(value = "source or information provider")
	@Column(name = "source")
	private String source;

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}