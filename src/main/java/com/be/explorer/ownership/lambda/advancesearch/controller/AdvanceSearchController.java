package com.be.explorer.ownership.lambda.advancesearch.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.be.explorer.ownership.lambda.advancesearch.dto.HierarchyDto;
import com.be.explorer.ownership.lambda.advancesearch.service.AdvanceSearchService;
import com.be.explorer.ownership.lambda.dto.GreetingDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SourcesPaginationDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.ClassificationCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.SourceCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.ClassificationsDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourcesDto;




/**
 * 
 * @author Viswanath Reddy G
 *
 */
@RestController
//@Component
@RequestMapping("/api/advancesearch")
public class AdvanceSearchController  {

	@Autowired
	AdvanceSearchService advanceSearchService;

	@Autowired
	private SourceCloudService sourceCloudService;
	
	@Autowired
	private ClassificationCloudService classificationCloudService;
	
	@RequestMapping(path = "/newGreeting", method = RequestMethod.GET)
	public GreetingDto sayHello(@RequestParam String name) {
		String message = "Hello " + name;
		GreetingDto dto = new GreetingDto();
		dto.setMessage(message);
		return dto;
	}


	@PostMapping(value = "/ownershipStructure", consumes = { "application/json; charset=UTF-8" }, produces = {
	"application/json; charset=UTF-8" })
	public ResponseEntity<?> ownershipStructure(@RequestBody HierarchyDto jsonString,
			@RequestParam(required = false) Integer maxSubsidiarielevels,
			@RequestParam(required = false) Integer lowRange, @RequestParam(required = false) Integer highRange,
			@RequestParam("token")  String token, HttpServletRequest request, @RequestParam String identifier,
			@RequestParam(required = false) Integer noOfSubsidiaries, @RequestParam String organisationName,
			@RequestParam String juridiction,
			@RequestParam(value = "isSubsidiariesRequired", required = false) Boolean isSubsidiariesRequired,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "source", required = false) String source) throws Exception {
		
			maxSubsidiarielevels = maxSubsidiarielevels != null ? maxSubsidiarielevels : 5;
			lowRange = lowRange != null ? lowRange : 10;
			highRange = highRange != null ? highRange : 100;
			noOfSubsidiaries = noOfSubsidiaries != null ? noOfSubsidiaries : 5;
			Long classificationId = null;

			List<ClassificationsDto> classificationsDtos = classificationCloudService
					.getClassifications("classification");
			// List<ClassificationsDto> classificationsDtos =
			// classificationsService.getClassifications();

			for (ClassificationsDto classificationsDto : classificationsDtos) {
				if (classificationsDto.getClassifcationName().equalsIgnoreCase("GENERAL")) {
					classificationId = classificationsDto.getClassificationId();
				}
			}
			SourcesPaginationDto dto = sourceCloudService.getSources(0, 0, null, classificationId, null, null, null,
					null, false);

			List<SourcesDto> sourcesDtos = dto.getSourceList();

			String path = "";
			String requestId = UUID.randomUUID().toString();
			path = identifier + "-" + (maxSubsidiarielevels.toString()) + "-" + (lowRange.toString()) + "-"
					+ (highRange.toString()) + "-" + (noOfSubsidiaries.toString()) + "-" + requestId;

			advanceSearchService.ownershipStructure(identifier, jsonString, maxSubsidiarielevels, lowRange, highRange,
					path, noOfSubsidiaries, 30, organisationName, juridiction, 12345L, isSubsidiariesRequired,
					startDate, endDate, false, sourcesDtos, source);
			JSONObject json = new JSONObject();

			json.put("path", path);

			
			
			 return new ResponseEntity<>(json.toString(), HttpStatus.OK);
		//return new ResponseEntity<>(advanceSearchService.readCorporateStructure(json.getString("path")), HttpStatus.OK);
	}
	
	@GetMapping(value = "/getCorporateStructure", produces = { "application/json; charset=UTF-8" })
	public String getOwnershipPath(@RequestParam String path, @RequestParam("token") String token,
			HttpServletRequest request) throws Exception {

		return (advanceSearchService.readCorporateStructure(path));
	}

}
