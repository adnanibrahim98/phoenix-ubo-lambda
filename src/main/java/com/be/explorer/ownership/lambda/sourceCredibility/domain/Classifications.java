package com.be.explorer.ownership.lambda.sourceCredibility.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_classifcations")
public class Classifications implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long classificationId;

	@Column(name = " classifcationName")
	private String classifcationName;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "classifications")
	private List<SubClassifications> subClassifications;

	public Classifications() {
	}

	public Classifications(Long classificationId, String classifcationName) {
		super();
		this.classificationId = classificationId;
		this.classifcationName = classifcationName;
	}

	public Long getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(Long classificationId) {
		this.classificationId = classificationId;
	}

	public String getClassifcationName() {
		return classifcationName;
	}

	public void setClassifcationName(String classifcationName) {
		this.classifcationName = classifcationName;
	}

	public List<SubClassifications> getSubClassifications() {
		return subClassifications;
	}

	public void setSubClassifications(List<SubClassifications> subClassifications) {
		this.subClassifications = subClassifications;
	}

}
