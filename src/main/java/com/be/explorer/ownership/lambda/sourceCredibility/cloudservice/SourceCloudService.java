package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SourcesCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SourcesPaginationDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceFilterDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourcesDto;



public interface SourceCloudService {
	
	public SourcesPaginationDto getSources( Integer pageNumber, Integer recordsPerPage, Boolean visible,
			Long classificationId, String orderBy, String orderIn, List<SourceFilterDto> sourceFilterDto,
			Long subClassifcationId, Boolean isAllSourcesRequired);
	
	public Boolean saveSource(SourcesDto sourcesDto,Long userId);
	
	public Boolean updateSource(SourcesDto sourcesDto,Long userId);
	
	public SourcesCloudDto getSourceById(Long sourceId);
	
}
