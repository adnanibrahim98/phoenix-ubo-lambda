package com.be.explorer.ownership.lambda.sourceCredibility.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "sc_media")
public class SourceMedia implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long mediaId;

	@Column(name = "domainName")
	private String mediaName;

	public SourceMedia() {
	}

	public SourceMedia(String mediaName) {
		super();
		this.mediaName = mediaName;
	}

	public Long getMediaId() {
		return mediaId;
	}

	public void setMediaId(Long mediaId) {
		this.mediaId = mediaId;
	}

	public String getMediaName() {
		return mediaName;
	}

	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}

}
