package com.be.explorer.ownership.lambda.sourceCredibility.cloudDto;

import java.io.Serializable;

public class GenericResponseDto implements Serializable {
	
	private Long itemId;
	
	private String message;

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
