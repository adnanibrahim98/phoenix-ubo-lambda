package com.be.explorer.ownership.lambda.sourceCredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.be.explorer.ownership.lambda.rest.util.ServiceCallHelper;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.ClassificationsCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.CredibilityCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.DataAttributeForSourceByIdDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.DataAttributesCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SourcesCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SourcesPaginationDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SubClassificationsCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.CredibilityCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.DataAttributesCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.SourceCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.ClassificationsDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.DataAttributesDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceDomainDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceFilterDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceIndustryDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceJurisdictionDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceMediaDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourcesDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourcesHideStatusDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SubClassificationsDto;
import com.be.explorer.ownership.lambda.sourceCredibility.enumType.CredibilityEnums;


@Service
public class SourceCloudServiceImpl implements SourceCloudService {

	@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL;
	
	@Value("${client_id}")
	private String clientId;
	
	private static String genericEntity="sources";
	
	@Autowired
	private CredibilityCloudService credibilityCloudService;
	
	@Autowired
	private DataAttributesCloudService dataAttributesCloudService;
	
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	
	@Override
	public SourcesPaginationDto getSources(Integer pageNumber, Integer recordsPerPage, Boolean visible,
			Long classificationId, String orderBy, String orderIn, List<SourceFilterDto> sourceFilterDto,
			Long subClassifcationId, Boolean isAllSourcesRequired) {
		SourcesPaginationDto resultDto= new SourcesPaginationDto();
		SourceFilterDto sourceFilterDtoForClassification= new SourceFilterDto();
		sourceFilterDtoForClassification.setColumnName("classification_id");
		if(sourceFilterDto==null) {
			sourceFilterDto= new ArrayList<>();
		}
		if(classificationId!=null) {
			
			List<String> filters= new ArrayList<>();
			filters.add(classificationId.toString());
			sourceFilterDtoForClassification.setFilters(filters);
			sourceFilterDto.add(sourceFilterDtoForClassification);
			
		}
		List<SourcesDto> sourceDtoList = new ArrayList<>();
		List<DataAttributesCloudDto> allList = dataAttributesCloudService.getDatAttributes();
		String url = buildCloudUrl(isAllSourcesRequired,pageNumber,recordsPerPage);
		JSONArray credibility = new JSONArray();
		JSONObject mainJson = new JSONObject();
		try {
			JSONArray filterJson = new JSONArray();
			filterJson= new JSONArray(sourceFilterDto);
			String response = postCloudApiData(url,filterJson.toString());
			if (response != null) {
				mainJson = new JSONObject(response);
				if (mainJson.has("data_link") && mainJson.has("status") && mainJson.getBoolean("status") ) {
					String dataLink=mainJson.getString("data_link");
					byte[] bytes= ServiceCallHelper.downloadFileFromServerWithoutTimeout(dataLink);
					if(bytes!=null) {
						String linkData=new String(bytes);
						JSONObject json = new JSONObject(linkData);
					JSONArray data = json.getJSONArray("data");
					resultDto.setTotalRecord(json.getLong("total_records"));
					for (int i = 0; i < data.length(); i++) {
						List<SourceDomainDto> domainDtoList = new ArrayList<>();
						List<SourceJurisdictionDto> jurisdictionDto = new ArrayList<>();
						List<SourceMediaDto> mediaDtoList = new ArrayList<>();
						List<SourceIndustryDto> industryDtoList = new ArrayList<>();
						SourcesDto singleSource = new SourcesDto();
						JSONObject singleJson = new JSONObject();
						singleJson = data.getJSONObject(i);
						Long classification_Id = 0l;
						Long sourceId = 0l;
						if (singleJson.has("classification_id") && singleJson.get("classification_id") != null) {
							classification_Id = singleJson.getLong("classification_id");
						}
						if (singleJson.has("category") && singleJson.get("category") instanceof String
								&& singleJson.get("category") != null) {
							singleSource.setCategory(singleJson.getString("category"));
						}
						if (singleJson.has("entity_id") && singleJson.get("entity_id") != null) {
							singleSource.setEntityId(singleJson.getString("entity_id"));
						}
						if (singleJson.has("display_name") && singleJson.get("display_name")  instanceof String) {
							singleSource.setSourceDisplayName(singleJson.getString("display_name"));
						}
						if (singleJson.has("name") && singleJson.get("name") != null) {
							singleSource.setSourceName(singleJson.getString("name"));
						}
						if (singleJson.has("type") && singleJson.get("type") != null) {
							singleSource.setSourceType(singleJson.getString("type"));
						}
						if (singleJson.has("url") && singleJson.get("url") instanceof String) {
							singleSource.setSourceUrl(singleJson.getString("url"));
						}
						if (singleJson.has("source_id") && singleJson.get("source_id") != null) {
							singleSource.setSourceId(singleJson.getLong("source_id"));
							sourceId = singleJson.getLong("source_id");
						}
						
							if (singleJson.has("source_domain") && singleJson.get("source_domain") != null) {
								JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_domain");
								if (sourceDomainJsonArray.length() > 0) {
									for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
										JSONObject sourceDomainObj = new JSONObject();
										sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
										SourceDomainDto sourceDomainDto = new SourceDomainDto();
										if (sourceDomainObj.has("item_id"))
											sourceDomainDto.setDomainId(sourceDomainObj.getLong("item_id"));
										if (sourceDomainObj.has("name"))
											sourceDomainDto.setDomainName(sourceDomainObj.getString("name"));
										domainDtoList.add(sourceDomainDto);
										singleSource.setSourceDomain(domainDtoList);
									}
								} else {
									singleSource.setSourceDomain(new ArrayList<>());
								}
							}
							if (singleJson.has("source_industry") && singleJson.get("source_industry") != null) {
								JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_industry");
								if (sourceDomainJsonArray.length()>0) {
									for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
										JSONObject sourceDomainObj = new JSONObject();
										sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
										SourceIndustryDto sourceDomainDto = new SourceIndustryDto();
										if (sourceDomainObj.has("item_id"))
											sourceDomainDto.setIndustryId(sourceDomainObj.getLong("item_id"));
										if (sourceDomainObj.has("name"))
											sourceDomainDto.setIndustryName(sourceDomainObj.getString("name"));
										industryDtoList.add(sourceDomainDto);
										singleSource.setSourceIndustry(industryDtoList);
									}
								}else {
									singleSource.setSourceIndustry(new ArrayList<>());
								}
							}
							if (singleJson.has("source_jurisdiction")
									&& singleJson.get("source_jurisdiction") != null) {
								JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_jurisdiction");
								if (sourceDomainJsonArray.length() > 0) {
									for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
										JSONObject sourceDomainObj = new JSONObject();
										sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
										SourceJurisdictionDto sourceDomainDto = new SourceJurisdictionDto();
										if (sourceDomainObj.has("item_id"))
											sourceDomainDto.setJurisdictionId(sourceDomainObj.getLong("item_id"));
										if (sourceDomainObj.has("code"))
											sourceDomainDto.setJurisdictionName(sourceDomainObj.getString("code"));
										if (sourceDomainObj.has("name"))
											sourceDomainDto
													.setJurisdictionOriginalName(sourceDomainObj.getString("name"));
										jurisdictionDto.add(sourceDomainDto);
										singleSource.setSourceJurisdiction(jurisdictionDto);
									}
								} else {
									singleSource.setSourceJurisdiction(new ArrayList<>());
								}
							}
							if (singleJson.has("source_media") && singleJson.get("source_media") != null) {
								JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_media");
								if (sourceDomainJsonArray.length() > 0) {
									for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
										JSONObject sourceDomainObj = new JSONObject();
										sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
										SourceMediaDto sourceDomainDto = new SourceMediaDto();
										if (sourceDomainObj.has("item_id"))
											sourceDomainDto.setMediaId(sourceDomainObj.getLong("item_id"));
										if (sourceDomainObj.has("name"))
											sourceDomainDto.setMediaName(sourceDomainObj.getString("name"));
										mediaDtoList.add(sourceDomainDto);
										singleSource.setSourceMedia(mediaDtoList);
									}
								} else {
									singleSource.setSourceMedia(new ArrayList<>());
								}
							}

						if (singleJson.has("credibilities")) {
							credibility = singleJson.getJSONArray("credibilities");
						}

						List<ClassificationsDto> classificationsDtoList = new ArrayList<>();
						if (singleJson.has("classification")) {
							JSONObject classificationJson = singleJson.getJSONObject("classification");
							ClassificationsDto class_Dto = new ClassificationsDto();
							if (classificationJson.has("name"))
								class_Dto.setClassifcationName(classificationJson.getString("name"));
							if (classificationJson.has("item_id"))
								class_Dto.setClassificationId(classificationJson.getLong("item_id"));
							if (singleJson.has("is_hidden") && singleJson.getInt("is_hidden") == 0) {
								SourcesHideStatusDto hidestatusDto = new SourcesHideStatusDto();
								hidestatusDto.setVisible(true);
								hidestatusDto.setClassificationId(classification_Id);
								hidestatusDto.setHideCategory(false);
								hidestatusDto.setHideDomain(false);
								hidestatusDto.setHideIndustry(false);
								hidestatusDto.setHideJurisdiction(false);
								hidestatusDto.setHideLink(false);
								hidestatusDto.setHideMedia(false);
								hidestatusDto.setSourceId(singleJson.getLong("source_id"));
								hidestatusDto.setSourcesHideStatusId(singleJson.getLong("source_id"));
								class_Dto.setHideStatusDto(hidestatusDto);
							} else {
								SourcesHideStatusDto hidestatusDto = new SourcesHideStatusDto();
								hidestatusDto.setVisible(false);
								hidestatusDto.setClassificationId(classification_Id);
								hidestatusDto.setHideCategory(false);
								hidestatusDto.setHideDomain(false);
								hidestatusDto.setHideIndustry(false);
								hidestatusDto.setHideJurisdiction(false);
								hidestatusDto.setHideLink(false);
								hidestatusDto.setHideMedia(false);
								hidestatusDto.setSourcesHideStatusId(singleJson.getLong("source_id"));
								hidestatusDto.setSourceId(singleJson.getLong("source_id"));
								class_Dto.setHideStatusDto(hidestatusDto);
							}
							List<SubClassificationsDto> subClassificationsDtoList = new ArrayList<>();
							if (classificationJson.has("sub_items")) {
								JSONArray subClassJarray = classificationJson.getJSONArray("sub_items");
								for (int j = 0; j < subClassJarray.length(); j++) {
									SubClassificationsDto sub_class = new SubClassificationsDto();
									JSONObject subClassJson = new JSONObject();
									subClassJson = subClassJarray.getJSONObject(j);
									if (subClassJson.has("item_id"))
										sub_class.setSubClassificationId(subClassJson.getLong("item_id"));
									if (subClassJson.has("name"))
										sub_class.setSubClassifcationName(subClassJson.getString("name"));
									List<DataAttributesDto> attributeList = new ArrayList<>();
									for (DataAttributesCloudDto dto1 : allList) {
										DataAttributesDto dto = new DataAttributesDto();
										if (dto1.getSubClassifications().getSubClassificationId()
												.equals(subClassJson.getLong("item_id"))) {
											dto.setAttributeId(dto1.getAttributeId());
											dto.setSourceAttributeName(dto1.getSourceAttributeName());
											dto.setSourceAttributeSchema(dto1.getSourceAttributeSchema());
											dto.setSourceAttributeSchemaGroup(dto1.getSourceAttributeSchemaGroup());
											attributeList.add(dto);
										}
									}
									//setting credibility for subClassification and data Attributes
									 sub_class.setDataAttributes(attributeList); 
									if (singleJson.has("credibilities")) {
										for (int k = 0; k < credibility.length(); k++) {
											JSONObject credibilityJson = new JSONObject();
											credibilityJson = credibility.getJSONObject(k);
											if (credibilityJson.has("sub_classification")
													&& credibilityJson.getLong("sub_classification_id") == sub_class
															.getSubClassificationId()
													&& /*credibilityJson.get("data_attribute") == null*/
													!(credibilityJson.get("data_attribute") instanceof JSONObject)) {
												if (credibilityJson.has("credibility_code_id")) {
													int credibilityOfSubClass = credibilityJson
															.getInt("credibility_code_id");
													if (credibilityOfSubClass == 0)
														sub_class
																.setSubClassificationCredibility(CredibilityEnums.NONE);
													if (credibilityOfSubClass == 1)
														sub_class.setSubClassificationCredibility(CredibilityEnums.LOW);
													if (credibilityOfSubClass == 2)
														sub_class.setSubClassificationCredibility(
																CredibilityEnums.MEDIUM);
													if (credibilityOfSubClass == 3)
														sub_class
																.setSubClassificationCredibility(CredibilityEnums.HIGH);

												} //setting all credibility of data attribute as default
												for (DataAttributesDto pol : attributeList) {
													
														if (credibilityJson.has("credibility_code_id")) {
															int credibilityOfSubClass = credibilityJson
																	.getInt("credibility_code_id");
															if (credibilityOfSubClass == 0)
																pol.setCredibilityValue(CredibilityEnums.NONE);
															if (credibilityOfSubClass == 1)
																pol.setCredibilityValue(CredibilityEnums.LOW);
															if (credibilityOfSubClass == 2)
																pol.setCredibilityValue(CredibilityEnums.MEDIUM);
															if (credibilityOfSubClass == 3)
																pol.setCredibilityValue(CredibilityEnums.HIGH);
														}
													
											}
											}
										}
										for (int k = 0; k < credibility.length(); k++) {
											JSONObject credibilityJson = new JSONObject();
											credibilityJson = credibility.getJSONObject(k);
											if (credibilityJson.has("data_attribute")
													&& credibilityJson.get("data_attribute") instanceof JSONObject) {
											for (DataAttributesDto pol : attributeList) {//setting dataattribute if credibility is changed.
													if (credibilityJson.getJSONObject("data_attribute")
															.getLong("attribute_id") == pol.getAttributeId()) {
														if (credibilityJson.has("credibility_code_id")) {
															int credibilityOfSubClass = credibilityJson
																	.getInt("credibility_code_id");
															if (credibilityOfSubClass == 0)
																pol.setCredibilityValue(CredibilityEnums.NONE);
															if (credibilityOfSubClass == 1)
																pol.setCredibilityValue(CredibilityEnums.LOW);
															if (credibilityOfSubClass == 2)
																pol.setCredibilityValue(CredibilityEnums.MEDIUM);
															if (credibilityOfSubClass == 3)
																pol.setCredibilityValue(CredibilityEnums.HIGH);
															pol.setAttributeSaved(true);
														}
													}
											}
										}else {
											
										}
									}
									sub_class.setDataAttributes(attributeList);
									subClassificationsDtoList.add(sub_class);
								}
								class_Dto.setSubClassifications(subClassificationsDtoList);
							}
							classificationsDtoList.add(class_Dto);
							singleSource.setClassifications(classificationsDtoList);
						}
						sourceDtoList.add(singleSource);
						resultDto.setSourceList(sourceDtoList);
					}
				}
			}
		}}} catch (Exception e) {
			e.printStackTrace();
		}
		return resultDto;
	}
	

	@Override
	public Boolean saveSource(SourcesDto sourcesDto,Long userId) {
		Boolean savedSource=null;
		Boolean savedCredibility=null;
		JSONObject jsonTosent= new JSONObject(sourcesDto);
		jsonTosent.put("client_id", clientId);
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity;
		try {
			String response=postCloudApiData(url,jsonTosent.toString());
			if(response!=null) {
				JSONObject postResponse=new JSONObject(response);
				if(postResponse.has("source_id")) {
					Long sourceId= postResponse.getLong("source_id");
					sourcesDto.setSourceId(sourceId);
					for(ClassificationsDto classifications: sourcesDto.getClassifications()) {
						for(SubClassificationsDto subClassifications: classifications.getSubClassifications()) {
							CredibilityCloudDto credibilityDto= new CredibilityCloudDto();
							credibilityDto.setSourceId(sourceId);
							credibilityDto.setSubClassifications(subClassifications);
							credibilityDto.setCredibility(calculateCredibility(sourcesDto.getSourceName()));
							credibilityDto.setDataAttributes(new DataAttributesDto());
							savedCredibility=credibilityCloudService.saveCredibility(credibilityDto);
						}
					}
					savedSource=true;
					/*eventPublisher.publishEvent(new LoggingEvent(sourcesDto.getSourceId(), sourcesDto.getSourceName(),
							LoggingEventType.SOURCE_CREATED, userId, new Date()));*/
				}else if(postResponse.has("status")) {
					//throw new InsufficientDataException("source name already exists");
				}
				else {
					savedSource=false;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return savedSource;
	}
	
	@Override
	public Boolean updateSource(SourcesDto sourceDto,Long userId) {
		Boolean updateSource=null;
		//List<LoggingEvent> loggingEventList = new ArrayList<LoggingEvent>();
		SourcesCloudDto sources= getSourceById(sourceDto.getSourceId());
	
		try {
			if(sources!=null) {
					if(sourceDto.getSourceName() != null){
						if(sources.getSourceName() == null){
							sources.setSourceName("");
							/*loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceName(),
									sourceDto.getSourceName(), LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_NAME_CHANGE,
									userId, new Date()));*/
							sources.setSourceName(sourceDto.getSourceName());
						}else if (!sources.getSourceName().equals(sourceDto.getSourceName())) {
							/*loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceName(),
									sourceDto.getSourceName(), LoggingEventType.SOURCE_CLASSIFICATION_SOURCE_NAME_CHANGE,
									userId, new Date()));*/
							sources.setSourceName(sourceDto.getSourceName());
						}
					}
					if(sourceDto.getSourceDisplayName() != null){
						if(sources.getSourceDisplayName() == null){
							sources.setSourceDisplayName("");
							/*loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceDisplayName(),
									sourceDto.getSourceDisplayName(),
									LoggingEventType.SOURCE_CLASSIFICATION_DISPLAY_NAME_CHANGE, userId, new Date()));*/
							sources.setSourceDisplayName(sourceDto.getSourceDisplayName());
						}else if (!sources.getSourceDisplayName().equals(sourceDto.getSourceDisplayName())) {
							sources.setSourceDisplayName(sourceDto.getSourceDisplayName());
							/*loggingEventList.add(new LoggingEvent(sources.getSourceId(), sources.getSourceDisplayName(),
									sourceDto.getSourceDisplayName(),
									LoggingEventType.SOURCE_CLASSIFICATION_DISPLAY_NAME_CHANGE, userId, new Date()));*/
						}
					}
					if(sourceDto.getSourceUrl() != null){
						if(sources.getSourceUrl() == null){
							sources.setSourceUrl("");
							/*loggingEventList.add(
									new LoggingEvent(sources.getSourceId(), sources.getSourceUrl(), sourceDto.getSourceUrl(),
											LoggingEventType.SOURCE_CLASSIFICATION_LINK_CHANGE, userId, new Date()));*/
							sources.setSourceUrl(sourceDto.getSourceUrl());
						}
						else if(!sources.getSourceUrl().equals(sourceDto.getSourceUrl())){
							/*loggingEventList.add(
									new LoggingEvent(sources.getSourceId(), sources.getSourceUrl(), sourceDto.getSourceUrl(),
											LoggingEventType.SOURCE_CLASSIFICATION_LINK_CHANGE, userId, new Date()));*/
							sources.setSourceUrl(sourceDto.getSourceUrl());
						}
					}
					//Update Domain
					if (!( sourceDto.getSourceDomain().isEmpty())) {
					
						List<SourceDomainDto> updatedSourceDomainList= new ArrayList<>();
						for (SourceDomainDto sourceDomain : sources.getSourceDomain()) {
							if (!sourceDto.getSourceDomain().stream()
									.anyMatch(ti -> ti.getDomainId().equals(sourceDomain.getDomainId()))) {
							
								/*loggingEventList.add(new LoggingEvent(sourceDomain.getDomainId(),
										sourceDto.getSourceDisplayName(), sourceDomain.getDomainName(),
										LoggingEventType.DELETE_SOURCE_DOMAIN, userId, new Date()));*/
							}
						}
						
						
						for (SourceDomainDto sourceDomainDto : sourceDto.getSourceDomain()) {
							if (!sources.getSourceDomain().stream()
									.anyMatch(ti -> ti.getDomainId().equals(sourceDomainDto.getDomainId()))) {
							
								/*loggingEventList.add(new LoggingEvent(sourceDomainDto.getDomainId(),
										sourceDto.getSourceDisplayName(), sourceDomainDto.getDomainName(),
										LoggingEventType.SOURCE_DOMAIN_NEW_ASSIGNED, userId, new Date()));*/
							}
						}
						
						for(SourceDomainDto domainForUpdation :sourceDto.getSourceDomain()) {
							updatedSourceDomainList.add(domainForUpdation);
						}
						sources.setSourceDomain(new ArrayList<>());
						sources.setSourceDomain(updatedSourceDomainList);
					}else if(sources.getSourceDomain()==null) {
						sources.setSourceDomain(new ArrayList<>());
					}
					//update industry
					if(!(sourceDto.getSourceIndustry().isEmpty())) {
					
						for (SourceIndustryDto sourceIndustry : sources.getSourceIndustry()) {
							if (!sourceDto.getSourceIndustry().stream()
									.anyMatch(ti -> ti.getIndustryId().equals(sourceIndustry.getIndustryId()))) {
							
								/*loggingEventList.add(new LoggingEvent(sourceIndustry.getIndustryId(),
										sourceDto.getSourceDisplayName(), sourceIndustry.getIndustryName(),
										LoggingEventType.DELETE_SOURCE_INDUSTRY, userId, new Date()));*/
							}
						}
						
						
						for (SourceIndustryDto sourceIndustryDto : sourceDto.getSourceIndustry()) {
							if (!sources.getSourceIndustry().stream().anyMatch(ti -> ti.getIndustryId()
									.equals(sourceIndustryDto.getIndustryId()))) {
								/*loggingEventList.add(new LoggingEvent(sourceIndustryDto.getIndustryId(),
										sourceDto.getSourceDisplayName(),
										sourceIndustryDto.getIndustryName(),
										LoggingEventType.SOURCE_INDUSTRY_NEW_ASSIGNED, userId,
										new Date()));*/
							}
						}
						sources.setSourceIndustry(new ArrayList<>());
						sources.setSourceIndustry(sourceDto.getSourceIndustry());
					}else if(sources.getSourceIndustry()==null) {
						sources.setSourceIndustry(new ArrayList<>());
					}
					//update jurisdiction
					if(!(sourceDto.getSourceJurisdiction().isEmpty())) {
						
						for (SourceJurisdictionDto sourceJurisdiction : sources.getSourceJurisdiction()) {
							if (!sourceDto.getSourceJurisdiction().stream()
									.anyMatch(ti -> ti.getJurisdictionId().equals(sourceJurisdiction.getJurisdictionId()))) {
								
								/*loggingEventList.add(new LoggingEvent(sourceJurisdiction.getJurisdictionId(),
										sourceDto.getSourceDisplayName(), sourceJurisdiction.getJurisdictionName(),
										LoggingEventType.DELETE_SOURCE_JURISDICTION, userId, new Date()));*/
							}
						}
						
						
						for (SourceJurisdictionDto sourceJurisdictionDto : sourceDto.getSourceJurisdiction()) {
							if (!sources.getSourceJurisdiction().stream().anyMatch(ti -> ti.getJurisdictionId()
									.equals(sourceJurisdictionDto.getJurisdictionId()))) {
								/*loggingEventList.add(new LoggingEvent(sourceJurisdictionDto.getJurisdictionId(),
										sourceDto.getSourceDisplayName(),
										sourceJurisdictionDto.getJurisdictionName(),
										LoggingEventType.SOURCE_JURISDICTION_NEW_ASSIGNED, userId,
										new Date()));*/
							}
						}
						sources.setSourceJurisdiction(new ArrayList<>() );
						sources.setSourceJurisdiction(sourceDto.getSourceJurisdiction());
					}else if(sources.getSourceJurisdiction()==null) {
						sources.setSourceJurisdiction(new ArrayList<>());
					}
					//update Media
					if( !(sourceDto.getSourceMedia().isEmpty())) {
						for (SourceMediaDto sourceMedia : sources.getSourceMedia()) {
							if (!sourceDto.getSourceMedia().stream()
									.anyMatch(ti -> ti.getMediaId().equals(sourceMedia.getMediaId()))) {
								/*loggingEventList.add(new LoggingEvent(sourceMedia.getMediaId(),
										sourceDto.getSourceDisplayName(), sourceMedia.getMediaName(),
										LoggingEventType.DELETE_SOURCE_MEDIA, userId, new Date()));*/
							}
						}
						
						for (SourceMediaDto sourceMedia : sourceDto.getSourceMedia()) {
							if (!sources.getSourceMedia().stream()
									.anyMatch(ti -> ti.getMediaId().equals(sourceMedia.getMediaId()))) {
								/*loggingEventList.add(new LoggingEvent(sourceMedia.getMediaId(),
										sourceDto.getSourceDisplayName(), sourceMedia.getMediaName(),
										LoggingEventType.SOURCE_MEDIA_NEW_ASSIGNED, userId, new Date()));*/
							}
						}
						sources.setSourceMedia(new ArrayList<>());
						sources.setSourceMedia(sourceDto.getSourceMedia());
					}else if(sources.getSourceMedia()==null) {
						sources.setSourceMedia(new ArrayList<>());
					}
					if(sourceDto.getCategory()!=null || sourceDto.getCategory().isEmpty())
						sources.setCategory(sourceDto.getCategory());
					if(sourceDto.getEntityId()!=null)
						sources.setEntityId(sourceDto.getEntityId());
					if(sourceDto.getSourceCreatedDate()!=null)
						sources.setSourceCreatedDate(sourceDto.getSourceCreatedDate());
					if(sourceDto.getSourceType()!=null)
						sources.setSourceType(sourceDto.getSourceType());
					
					Map<Long,SubClassificationsCloudDto> subClassMap= new HashedMap();
					Map<Long,DataAttributeForSourceByIdDto> dataAttributeMap= new HashedMap();
				
					if(sources.getClassifications()!=null) {
						for(ClassificationsCloudDto classificationsDtoSave:sources.getClassifications()) {
							for(SubClassificationsCloudDto subClassificationsDtoSave :classificationsDtoSave.getSubClassifications()) {
								subClassMap.put(subClassificationsDtoSave.getSubClassificationId(),subClassificationsDtoSave);
								for(DataAttributeForSourceByIdDto dataAttribute:subClassificationsDtoSave.getDataAttributes()) {
									dataAttributeMap.put(dataAttribute.getAttributeId(),dataAttribute);
								}
							
							}
						}
					}
					Boolean hideSource=true;
					if (sourceDto.getClassifications() != null) {
						List<ClassificationsDto> classificationsListSave = sourceDto.getClassifications();
						for (ClassificationsDto classificationsDtoSave : classificationsListSave) {
							if(!classificationsDtoSave.getHideStatusDto().getVisible()) {
								hideSource=false;
							}
							List<SubClassificationsDto> subClassificationsDtoListSave = classificationsDtoSave
									.getSubClassifications();
							// updating sub-classification credibility.
							for (SubClassificationsDto subClassificationsDtoSave : subClassificationsDtoListSave) {
								SubClassificationsCloudDto subClassDto=subClassMap.get(subClassificationsDtoSave.getSubClassificationId());
								if(subClassMap.containsKey(subClassificationsDtoSave.getSubClassificationId())) {
									
								if (!(subClassDto.getSubClassificationCredibility() == subClassificationsDtoSave
										.getSubClassificationCredibility())) {

									CredibilityCloudDto updateSubClassifications = new CredibilityCloudDto();
									updateSubClassifications.setCredibility(
											subClassificationsDtoSave.getSubClassificationCredibility());
									updateSubClassifications
											.setSourceCredibilityId(subClassDto.getSourceCredibilityId());
									updateSubClassifications.setSourceId(sources.getSourceId());
									updateSubClassifications.setSubClassifications(subClassificationsDtoSave);
									updateSubClassifications.setDataAttributes(new DataAttributesDto());
									credibilityCloudService.updateCredibility(updateSubClassifications);
									/*loggingEventList.add(new LoggingEvent(subClassDto.getSourceCredibilityId(),
											subClassDto.getSubClassificationCredibility(),
											subClassificationsDtoSave.getSubClassificationCredibility(),
											LoggingEventType.SUB_CLASSIFICATION_CREDIBILITY_CHANGE, userId,
											new Date()));*/
								}
								// update data-attribute credibility.
								for (DataAttributesDto dataAttributeSave : subClassificationsDtoSave
										.getDataAttributes()) {
									DataAttributeForSourceByIdDto dataAttributeById = dataAttributeMap
											.get(dataAttributeSave.getAttributeId());
									if (dataAttributeMap.containsKey(dataAttributeSave.getAttributeId())) {
										if (dataAttributeById.isAttributeSaved()) { // True then update it.
											CredibilityCloudDto updateDataAttribute = new CredibilityCloudDto();
											updateDataAttribute.setCredibility(dataAttributeSave.getCredibilityValue());
											updateDataAttribute
													.setSourceCredibilityId(dataAttributeById.getSourceCredibilityId());
											updateDataAttribute.setSourceId(sources.getSourceId());
											updateDataAttribute.setSubClassifications(subClassificationsDtoSave);
											updateDataAttribute.setDataAttributes(dataAttributeSave);
											/*loggingEventList.add(new LoggingEvent(
													dataAttributeById.getSourceCredibilityId(),
													dataAttributeById.getCredibilityValue(),
													dataAttributeSave.getCredibilityValue(),
													LoggingEventType.ATTRIBUTE_CREDIBILITY_CHANGE, userId,
													new Date()));*/
											credibilityCloudService.updateCredibility(updateDataAttribute);
										} else {
											CredibilityCloudDto saveDataAttribute = new CredibilityCloudDto();
											saveDataAttribute.setCredibility(dataAttributeSave.getCredibilityValue());
											saveDataAttribute
													.setSourceCredibilityId(dataAttributeById.getSourceCredibilityId());
											saveDataAttribute.setSourceId(sources.getSourceId());
											saveDataAttribute.setSubClassifications(subClassificationsDtoSave);
											saveDataAttribute.setDataAttributes(dataAttributeSave);
											/*loggingEventList.add(new LoggingEvent(
													dataAttributeById.getSourceCredibilityId(),
													dataAttributeById.getCredibilityValue(),
													dataAttributeSave.getCredibilityValue(),
													LoggingEventType.ATTRIBUTE_CREDIBILITY_ADD_NEW, userId,
													new Date()));*/
											credibilityCloudService.saveCredibility(saveDataAttribute);
										}
									}
								}
								}
								} // end of for subclassification
							}// end of for classification
						}// end of if
					
					//Updating visibililty
					JSONObject jsonTosent= new JSONObject();
					jsonTosent=new JSONObject(sources);
					if (!hideSource) {
						jsonTosent.put("is_hidden", 1);
					}
					jsonTosent.put("client_id", clientId);
					String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?source_id="+sources.getSourceId().toString();
					String response=putCloudApiData(url,jsonTosent.toString());
					if(response!=null) {
						JSONObject responseJson= new JSONObject(response);
						if(responseJson.has("message") && responseJson.getString("message").equals("updated")) {
							updateSource=true;
							/*if (!loggingEventList.isEmpty())
								eventPublisher.publishEvent(new LoggingEvent(sources.getSourceId(), loggingEventList,
									LoggingEventType.UPDATE_SOURCE, userId, new Date()));*/
						}
						else
							updateSource=false;
					}
			}
			else {
				throw new Exception("Source Does not exist");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return updateSource;
	}
	
	@Override
	public SourcesCloudDto getSourceById(Long sourceId) {
		SourcesCloudDto singleSource= new SourcesCloudDto(); // 1. changed SourcesDto to SourcesCloudDto
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?source_id="+sourceId.toString();
		List<DataAttributesCloudDto> allList = dataAttributesCloudService.getDatAttributes();
		JSONArray credibility = new JSONArray();
		try {
			String response= getCloudApiData(url);
			if(response!=null) {
				JSONObject json= new JSONObject(response);
				List<SourceDomainDto> domainDtoList = new ArrayList<>();
				List<SourceJurisdictionDto> jurisdictionDto = new ArrayList<>();
				List<SourceMediaDto> mediaDtoList = new ArrayList<>();
				List<SourceIndustryDto> industryDtoList = new ArrayList<>();
				Long classification_Id = 0l;
				if(json.has("data")) {
					JSONObject singleJson= json.getJSONObject("data");
					if (singleJson.has("classification_id") && singleJson.get("classification_id") != null) {
						classification_Id = singleJson.getLong("classification_id");
					}
					if (singleJson.has("category") && singleJson.get("category") instanceof String
							&& singleJson.get("category") != null) {
						singleSource.setCategory(singleJson.getString("category"));
					}
					if (singleJson.has("entity_id") && singleJson.get("entity_id") != null) {
						singleSource.setEntityId(singleJson.getString("entity_id"));
					}
					if (singleJson.has("display_name") && singleJson.get("display_name") != null) {
						singleSource.setSourceDisplayName(singleJson.getString("display_name"));
					}
					if (singleJson.has("name") && singleJson.get("name") != null) {
						singleSource.setSourceName(singleJson.getString("name"));
					}
					if (singleJson.has("type") && singleJson.get("type") != null) {
						singleSource.setSourceType(singleJson.getString("type"));
					}
					if (singleJson.has("url") && singleJson.get("url") instanceof String) {
						singleSource.setSourceUrl(singleJson.getString("url"));
					}
					if (singleJson.has("source_id") && singleJson.get("source_id") != null) {
						singleSource.setSourceId(singleJson.getLong("source_id"));
						sourceId = singleJson.getLong("source_id");
					}
					
					if (singleJson.has("source_domain") && singleJson.get("source_domain") != null) {
						JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_domain");
						if (sourceDomainJsonArray.length() > 0) {
							for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
								JSONObject sourceDomainObj = new JSONObject();
								sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
								SourceDomainDto sourceDomainDto = new SourceDomainDto();
								if (sourceDomainObj.has("item_id"))
									sourceDomainDto.setDomainId(sourceDomainObj.getLong("item_id"));
								if (sourceDomainObj.has("name"))
									sourceDomainDto.setDomainName(sourceDomainObj.getString("name"));
								domainDtoList.add(sourceDomainDto);
								singleSource.setSourceDomain(domainDtoList);
							}
						} else {
							singleSource.setSourceDomain(domainDtoList);
						}
					}
					if (singleJson.has("source_industry") && singleJson.get("source_industry") != null) {
						JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_industry");
						if (sourceDomainJsonArray.length() > 0) {
							for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
								JSONObject sourceDomainObj = new JSONObject();
								sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
								SourceIndustryDto sourceDomainDto = new SourceIndustryDto();
								if (sourceDomainObj.has("item_id"))
									sourceDomainDto.setIndustryId(sourceDomainObj.getLong("item_id"));
								if (sourceDomainObj.has("name"))
									sourceDomainDto.setIndustryName(sourceDomainObj.getString("name"));
								industryDtoList.add(sourceDomainDto);
								singleSource.setSourceIndustry(industryDtoList);
							}
						} else {
							singleSource.setSourceIndustry(industryDtoList);
						}
					}
					if (singleJson.has("source_jurisdiction") && singleJson.get("source_jurisdiction") != null) {
						JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_jurisdiction");
						if (sourceDomainJsonArray.length() > 0) {
							for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
								JSONObject sourceDomainObj = new JSONObject();
								sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
								SourceJurisdictionDto sourceDomainDto = new SourceJurisdictionDto();
								if (sourceDomainObj.has("item_id"))
									sourceDomainDto.setJurisdictionId(sourceDomainObj.getLong("item_id"));
								if (sourceDomainObj.has("code"))
									sourceDomainDto.setJurisdictionName(sourceDomainObj.getString("code"));
								if (sourceDomainObj.has("name"))
									sourceDomainDto.setJurisdictionOriginalName(sourceDomainObj.getString("name"));
								jurisdictionDto.add(sourceDomainDto);
								singleSource.setSourceJurisdiction(jurisdictionDto);
							}
						} else {
							singleSource.setSourceJurisdiction(jurisdictionDto);
						}
					}
					if (singleJson.has("source_media") && singleJson.get("source_media") != null) {
						JSONArray sourceDomainJsonArray = singleJson.getJSONArray("source_media");
						if (sourceDomainJsonArray.length() > 0) {
							for (int q = 0; q < sourceDomainJsonArray.length(); q++) {
								JSONObject sourceDomainObj = new JSONObject();
								sourceDomainObj = sourceDomainJsonArray.getJSONObject(q);
								SourceMediaDto sourceDomainDto = new SourceMediaDto();
								if (sourceDomainObj.has("item_id"))
									sourceDomainDto.setMediaId(sourceDomainObj.getLong("item_id"));
								if (sourceDomainObj.has("name"))
									sourceDomainDto.setMediaName(sourceDomainObj.getString("name"));
								mediaDtoList.add(sourceDomainDto);
								singleSource.setSourceMedia(mediaDtoList);
							}
						} else {
							singleSource.setSourceMedia(mediaDtoList);
						}
					}
					if (singleJson.has("credibilities")) {
						credibility = singleJson.getJSONArray("credibilities");
					}
					List<ClassificationsCloudDto> classificationsDtoList = new ArrayList<>(); //2. change ClassificationsDto to ClassificationsCloudDto
					if (singleJson.has("classification")) {
						JSONObject classificationJson = singleJson.getJSONObject("classification");
						ClassificationsCloudDto class_Dto = new ClassificationsCloudDto();// 3. change ClassificationsDto to ClassificationsCloudDto
						if (classificationJson.has("name"))
							class_Dto.setClassifcationName(classificationJson.getString("name"));
						if (classificationJson.has("item_id"))
							class_Dto.setClassificationId(classificationJson.getLong("item_id"));
						if (singleJson.has("is_hidden") && singleJson.getInt("is_hidden") == 0) {
							SourcesHideStatusDto hidestatusDto = new SourcesHideStatusDto();
							hidestatusDto.setVisible(true);
							hidestatusDto.setClassificationId(classification_Id);
							hidestatusDto.setHideCategory(false);
							hidestatusDto.setHideDomain(false);
							hidestatusDto.setHideIndustry(false);
							hidestatusDto.setHideJurisdiction(false);
							hidestatusDto.setHideLink(false);
							hidestatusDto.setHideMedia(false);
							hidestatusDto.setSourceId(singleJson.getLong("source_id"));
							class_Dto.setHideStatusDto(hidestatusDto);
						} else {
							SourcesHideStatusDto hidestatusDto = new SourcesHideStatusDto();
							hidestatusDto.setVisible(false);
							hidestatusDto.setClassificationId(classification_Id);
							hidestatusDto.setHideCategory(false);
							hidestatusDto.setHideDomain(false);
							hidestatusDto.setHideIndustry(false);
							hidestatusDto.setHideJurisdiction(false);
							hidestatusDto.setHideLink(false);
							hidestatusDto.setHideMedia(false);
							hidestatusDto.setSourceId(singleJson.getLong("source_id"));
							class_Dto.setHideStatusDto(hidestatusDto);
						}
						List<SubClassificationsCloudDto> subClassificationsDtoList = new ArrayList<>();//4. change SubClassificationsDto to SubClassificationsCloudDto
						if (classificationJson.has("sub_items")) {
							JSONArray subClassJarray = classificationJson.getJSONArray("sub_items");
							for (int j = 0; j < subClassJarray.length(); j++) {
								SubClassificationsCloudDto sub_class = new SubClassificationsCloudDto();
								JSONObject subClassJson = new JSONObject();
								subClassJson = subClassJarray.getJSONObject(j);
								if (subClassJson.has("item_id"))
									sub_class.setSubClassificationId(subClassJson.getLong("item_id"));
								if (subClassJson.has("name"))
									sub_class.setSubClassifcationName(subClassJson.getString("name"));
								List<DataAttributeForSourceByIdDto> attributeList = new ArrayList<>(); // change DataAttributesDto to DataAttributesCloudDto
								//setting data attribute to subclassifications.
								for (DataAttributesCloudDto dto1 : allList) {
									DataAttributeForSourceByIdDto dto = new DataAttributeForSourceByIdDto();
									if (dto1.getSubClassifications().getSubClassificationId()
											.equals(subClassJson.getLong("item_id"))) { // commented the setters and adding to list 
										dto.setAttributeId(dto1.getAttributeId());
										dto.setSourceAttributeName(dto1.getSourceAttributeName());
										dto.setSourceAttributeSchema(dto1.getSourceAttributeSchema());
										dto.setSourceAttributeSchemaGroup(dto1.getSourceAttributeSchemaGroup());
										attributeList.add(dto);
									}
								}
								//setting credibility for subClassification and data Attributes
								 sub_class.setDataAttributes(attributeList); 
								if (singleJson.has("credibilities")) {
									for (int k = 0; k < credibility.length(); k++) {
										JSONObject credibilityJson = new JSONObject();
										credibilityJson = credibility.getJSONObject(k);
										if (credibilityJson.has("sub_classification")
												&& credibilityJson.getLong("sub_classification_id") == sub_class
														.getSubClassificationId()
												&& /*credibilityJson.get("data_attribute") == null*/
												!(credibilityJson.get("data_attribute") instanceof JSONObject)) {
											if (credibilityJson.has("credibility_code_id")) {
												int credibilityOfSubClass = credibilityJson
														.getInt("credibility_code_id");
												if (credibilityOfSubClass == 0)
													sub_class
															.setSubClassificationCredibility(CredibilityEnums.NONE);
												if (credibilityOfSubClass == 1)
													sub_class.setSubClassificationCredibility(CredibilityEnums.LOW);
												if (credibilityOfSubClass == 2)
													sub_class.setSubClassificationCredibility(
															CredibilityEnums.MEDIUM);
												if (credibilityOfSubClass == 3)
													sub_class
															.setSubClassificationCredibility(CredibilityEnums.HIGH);
												sub_class.setSourceCredibilityId(credibilityJson.getLong("credibility_id"));

											}
											for (DataAttributeForSourceByIdDto pol : attributeList) {
												
													if (credibilityJson.has("credibility_code_id")) {
														int credibilityOfSubClass = credibilityJson
																.getInt("credibility_code_id");
														if (credibilityOfSubClass == 0)
															pol.setCredibilityValue(CredibilityEnums.NONE);
														if (credibilityOfSubClass == 1)
															pol.setCredibilityValue(CredibilityEnums.LOW);
														if (credibilityOfSubClass == 2)
															pol.setCredibilityValue(CredibilityEnums.MEDIUM);
														if (credibilityOfSubClass == 3)
															pol.setCredibilityValue(CredibilityEnums.HIGH);
													}
													pol.setSourceCredibilityId(credibilityJson.getLong("credibility_id"));
												
										}
										}
									}
									for (int k = 0; k < credibility.length(); k++) {
										JSONObject credibilityJson = new JSONObject();
										credibilityJson = credibility.getJSONObject(k);
										if (credibilityJson.has("data_attribute")
												&& credibilityJson.get("data_attribute") instanceof JSONObject) {
										for (DataAttributeForSourceByIdDto pol : attributeList) {
												if (credibilityJson.getJSONObject("data_attribute")
														.getLong("attribute_id") == pol.getAttributeId()) {
													if (credibilityJson.has("credibility_code_id")) {
														int credibilityOfSubClass = credibilityJson
																.getInt("credibility_code_id");
														if (credibilityOfSubClass == 0)
															pol.setCredibilityValue(CredibilityEnums.NONE);
														if (credibilityOfSubClass == 1)
															pol.setCredibilityValue(CredibilityEnums.LOW);
														if (credibilityOfSubClass == 2)
															pol.setCredibilityValue(CredibilityEnums.MEDIUM);
														if (credibilityOfSubClass == 3)
															pol.setCredibilityValue(CredibilityEnums.HIGH);
														pol.setSourceCredibilityId(credibilityJson.getLong("credibility_id"));
														pol.setAttributeSaved(true);
													}
												}
										}
									}
								}
								sub_class.setDataAttributes(attributeList);
								subClassificationsDtoList.add(sub_class);
							}
							class_Dto.setSubClassifications(subClassificationsDtoList);
						}
						classificationsDtoList.add(class_Dto);
						singleSource.setClassifications(classificationsDtoList);
					}
				}
			}
		}} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return singleSource;
	}
	public String putCloudApiData(String url, String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] =ServiceCallHelper.putStringDataToServerWithoutTimeout(url, jsonToSent);
		
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data_attribute_id")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
		
	public static String[] postStringDataToServer(String url, String jsonToBeSent) {
		String[] serverResponse = new String[2];
		StringBuilder responseStrBuilder = new StringBuilder();
		RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10 * 1000)
				.setConnectTimeout(10 * 1000).setConnectionRequestTimeout(10 * 1000).build();

		try (CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();) {
			HttpResponse response = null;
			HttpPost httpPost = new HttpPost(url);
			StringEntity stringEntity = new StringEntity(jsonToBeSent, "UTF-8");
			httpPost.setEntity(stringEntity);
			httpPost.setHeader("Content-type", "application/json");
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			serverResponse[0] = String.valueOf(status);
			HttpEntity entity = response.getEntity();
			if (status == 200) {
				responseStrBuilder.append(EntityUtils.toString(entity, "UTF-8"));
				serverResponse[1] = responseStrBuilder.toString();
			} else {
				serverResponse[1] = null;
			}
			EntityUtils.consume(entity);
		} catch (Exception e) {
			serverResponse[0] = String.valueOf(0);
		}
		return serverResponse;
	}
	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("data")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}
	
	public String postCloudApiData(String url,String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.postStringDataToServerWithoutTimeout(url, jsonToSent);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("item_id")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}
	
	public String buildCloudUrl(Boolean isAllRequired, Integer pageNumber, Integer recordsPerPage) {
		StringBuilder url= new StringBuilder();
	//	if(isAllRequired)
			url.append(SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"/get");
		 if(pageNumber!=null && recordsPerPage!=null &&pageNumber>0 && recordsPerPage>0)
			url.append( "?recordsPerPage="
					+ recordsPerPage.toString() + "&pageNumber=" + pageNumber.toString());
		 
		return url.toString();
	}
	
	CredibilityEnums calculateCredibility(String sourceName) {
		if (sourceName.contains(".gov"))
			return CredibilityEnums.HIGH;
		else if (sourceName.contains(".edu") || sourceName.contains(".ac") || sourceName.contains(".org"))
			return CredibilityEnums.MEDIUM;
		else if (sourceName.contains(".com.co"))
			return CredibilityEnums.LOW;
		else
			return CredibilityEnums.NONE;
	}
}
