package com.be.explorer.ownership.lambda.sourceCredibility.enumType;

/**
 * 
 * @author Suresh
 *
 */
public enum CredibilityEnums {

	NONE, LOW, MEDIUM, HIGH

}
