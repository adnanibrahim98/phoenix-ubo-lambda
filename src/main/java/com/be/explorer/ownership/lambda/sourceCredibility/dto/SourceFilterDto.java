package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author suresh
 *
 */
public class SourceFilterDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String columnName;
	private String id;
	private List<String> filters;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getFilters() {
		return filters;
	}

	public void setFilters(List<String> filters) {
		this.filters = filters;
	}

}
