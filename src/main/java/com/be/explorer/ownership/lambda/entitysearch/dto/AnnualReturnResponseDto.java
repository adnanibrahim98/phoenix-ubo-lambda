package com.be.explorer.ownership.lambda.entitysearch.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author suresh
 *
 */
public class AnnualReturnResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Double totalNumberOfShares;
	private List<ShareholdingDto> shareholdingDto;

	public Double getTotalNumberOfShares() {
		return totalNumberOfShares;
	}

	public void setTotalNumberOfShares(Double totalNumberOfShares) {
		this.totalNumberOfShares = totalNumberOfShares;
	}

	public List<ShareholdingDto> getShareholdingDto() {
		return shareholdingDto;
	}

	public void setShareholdingDto(List<ShareholdingDto> shareholdingDto) {
		this.shareholdingDto = shareholdingDto;
	}

}
