package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.GenericEntityDto;

public interface GenericCloudService {
	
	public GenericEntityDto saveGenericEntity(GenericEntityDto genericEntityDto);

	public GenericEntityDto getGenericItemById(Long id);
	
	public List<GenericEntityDto> getAllGenericItem();
	
	public List<GenericEntityDto> getGenericItemByType(String type);
	
	public Boolean deleteGenericEntityByid(Long id);
	
	public GenericEntityDto updateGenericEntity(GenericEntityDto genericEntityDto);
}
