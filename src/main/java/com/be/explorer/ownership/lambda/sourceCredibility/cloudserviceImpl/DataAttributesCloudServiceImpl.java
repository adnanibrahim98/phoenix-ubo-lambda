package com.be.explorer.ownership.lambda.sourceCredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.be.explorer.ownership.lambda.rest.util.ServiceCallHelper;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.DataAttributesCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SaveDataAttributeCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.DataAttributesCloudService;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SubClassificationsDto;


@Service
public class DataAttributesCloudServiceImpl implements DataAttributesCloudService {
	
	@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL;
	
	private static String genericEntity="dataattributes";
	
	@Value("${client_id}")
	private String clientId;

	@Override
	public List<DataAttributesCloudDto> getDatAttributes() {
		List<DataAttributesCloudDto> dataAttributeDtolist = new ArrayList<>();
		String url = buildCloudUrl(null);
		JSONObject json = new JSONObject();
		try {
			String response = getCloudApiData(url);
			if (response != null) {
				json = new JSONObject(response);
			}
			if (json.has("data")) {
				JSONArray data = json.getJSONArray("data");

				for (int i = 0; i < data.length(); i++) {
					DataAttributesCloudDto singleDto = new DataAttributesCloudDto();
					JSONObject singleJson = new JSONObject();
					singleJson = data.getJSONObject(i);
					if (singleJson.has("attribute_id"))
						singleDto.setAttributeId(singleJson.getLong("attribute_id"));
					if (singleJson.has("name"))
						singleDto.setSourceAttributeName(singleJson.getString("name"));
					if (singleJson.has("schema"))
						singleDto.setSourceAttributeSchema(singleJson.getString("schema"));
					if (singleJson.has("schema_group"))
						singleDto.setSourceAttributeSchemaGroup(singleJson.getString("schema_group"));
					if (singleJson.has("sub_classification")) {
						SubClassificationsDto subClassDto = new SubClassificationsDto();
						if (singleJson.getJSONObject("sub_classification").has("name"))
							subClassDto.setSubClassifcationName(
									singleJson.getJSONObject("sub_classification").getString("name"));
						if (singleJson.getJSONObject("sub_classification").has("item_id"))
							subClassDto.setSubClassificationId(
									singleJson.getJSONObject("sub_classification").getLong("item_id"));
						singleDto.setSubClassifications(subClassDto);
					}

					singleDto.setAttributeSaved(false);
					dataAttributeDtolist.add(singleDto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataAttributeDtolist;
	}
	
	@Override
	public Boolean deleteDataAttributesById(Long attrId) {

		String url = buildCloudUrl(attrId);
		Boolean deleteResult = null;
		try {
			String getresponse = deleteCloudApiData(url);
			if (getresponse != null) {
				JSONObject getResponseJson = new JSONObject(getresponse);
				if (getResponseJson.has("message") && getResponseJson.getString("message").equals("deleted")) {
					deleteResult = true;
					return deleteResult;
				} else
					deleteResult = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return deleteResult;
	}
	
	@Override
	public Boolean saveDataAttributes(SaveDataAttributeCloudDto saveCloudDto) {
		JSONObject jsonToSent = new JSONObject();
		jsonToSent.put("name",saveCloudDto.getSourceAttributeName());
		jsonToSent.put("schema",saveCloudDto.getSourceAttributeSchema() );
		jsonToSent.put("schema_group", saveCloudDto.getSourceAttributeSchemaGroup());
		jsonToSent.put("sub_classification_id", saveCloudDto.getSubClassifications());
		jsonToSent.put("client_id", clientId);
		Boolean saved = null;
		String url = buildCloudUrl(null);
		try {
			String response = postCloudApiData(url, jsonToSent.toString());
			if (response != null) {
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("data_attribute_id") && responseJson.has("message")
						&& responseJson.getString("message").equals("saved")) {
					saved = true;
					return saved;
				} else {
					saved = false;
					return saved;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saved;
	}
	@Override
	public Boolean updateDataAttributes(SaveDataAttributeCloudDto dataAttributeDto) {
		JSONObject jsonToSent = new JSONObject();
		jsonToSent.put("name",dataAttributeDto.getSourceAttributeName());
		jsonToSent.put("schema",dataAttributeDto.getSourceAttributeSchema() );
		jsonToSent.put("schema_group", dataAttributeDto.getSourceAttributeSchemaGroup());
		jsonToSent.put("sub_classification_id", dataAttributeDto.getSubClassifications());
		jsonToSent.put("client_id", clientId);
		jsonToSent.put("data_attribute_id", dataAttributeDto.getAttributeId());
		Boolean saved = null;
		String url = buildCloudUrl(null);
		try {
			String response = putCloudApiData(url, jsonToSent.toString());
			if (response != null) {
				JSONObject responseJson = new JSONObject(response);
				if (responseJson.has("data_attribute_id") && responseJson.has("message")
						&& responseJson.getString("message").equals("updated")) {
					saved = true;
					return saved;
				} else {
					saved = false;
					return saved;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return saved;
	}


	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}

	public String deleteCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.deleteDataInServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("message"))
					return response;
			} else {
				return response;
			}
			return response;
		}
		return response;
	}

	
	public String putCloudApiData(String url, String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] =ServiceCallHelper.putStringDataToServerWithoutTimeout(url, jsonToSent);
		
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data_attribute_id")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
	public String postCloudApiData(String url, String jsonToSent) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.postStringDataToServer(url, jsonToSent);
		
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("data_attribute_id")) {
					return response;
				}
			} else {
				return response;
			}
			return response;
		}
		return response;
	}
	
	
	public String buildCloudUrl(Long item_type) {
		if (item_type != null) {
			String url = SOURCE_MANAGEMENT_CLOUD_URL + genericEntity + "?data_attribute_id=" + item_type;
			return url;
		} else {
			String url = SOURCE_MANAGEMENT_CLOUD_URL + genericEntity;
			return url;
		}
	}
}
