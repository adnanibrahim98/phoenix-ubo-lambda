package com.be.explorer.ownership.lambda.advancesearch.serviceImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.be.explorer.ownership.lambda.advancesearch.dto.HierarchyDto;
import com.be.explorer.ownership.lambda.advancesearch.service.AdvanceSearchService;
import com.be.explorer.ownership.lambda.entitysearch.dto.AnnualReturnResponseDto;
import com.be.explorer.ownership.lambda.entitysearch.dto.ShareholdingDto;
import com.be.explorer.ownership.lambda.entitysearch.service.EntitySearchService;
import com.be.explorer.ownership.lambda.rest.util.ServiceCallHelper;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.ClassificationsDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourcesDto;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SubClassificationsDto;




/**
 * 
 * @author Viswanath Reddy G
 *
 */

@Service("advanceSearchService")
@Transactional("transactionManager")
@PropertySource(value = "classpath:application.properties")
public class AdvanceSearchServiceImpl implements AdvanceSearchService {
	@Autowired
	Environment env;

	@Value("${corporate_structure_location}")
	//private String CORPORATE_STRUCTURE_LOCATION=env.getProperty("corporate_structure_location");
	private String CORPORATE_STRUCTURE_LOCATION;
	
	@Value("${bigdata_s3_doc_url}")
	//private String BIG_DATA_S3_DOC_URL=env.getProperty("bigdata_s3_doc_url");
	private String BIG_DATA_S3_DOC_URL;
	
	@Value("${bigdata_multisource_url}")
	private String BIGDATA_MULTISOURCE_URL;

//	@Value("${be_entity_screenshot_url}")
	//private String SCREEN_SHOT_URL=env.getProperty("be_entity_screenshot_url");

	//@Value("${bigdata_org_url}")
	//private String BIG_DATA__ORG_URL=env.getProperty("bigdata_org_url");

	//@Value("${screening_url}")
	//private String SCREENING_URL=env.getProperty("screening_url");
	
	@Value("${client_id}")
	//private String clientId=env.getProperty("client_id");
	private String clientId;
	@Lazy
	@Autowired
	private EntitySearchService entitySearchService;

	//@Autowired
	//AdverseNewsService adverseNewsService;

	//@Autowired
	//TunaService tunaService;
	
	//@Autowired
	//private EntityAttributeService entityAttributeService;
	
	//@Autowired
	//private SourceAddToPageService sourceAddToPageService;

	//@Autowired
	//private SignificantWatchListService significantWatchListService;
	
	//@Autowired
	//private AuditLogService auditLogService;

	@Value("${client_id}")
	private String CLIENT_ID;


	@Autowired
	private ThreadPoolExecutor executor;

	@Value("${be_entity_api_key}")
	private String BE_ENTITY_API_KEY;

	@Override
	@Async("taskExecutor")
	public Boolean ownershipStructure(String inputIdentifier, HierarchyDto jsonString, Integer maxSubsidiarielevel,
			Integer lowRange, Integer highRange, String path, Integer noOfSubsidiaries, Integer maxShareholderLevel,
			String organisationName, String juridiction,Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate, boolean isRollBack,List<SourcesDto> sourcesDtos, String source)
					throws JSONException, Exception {
		boolean isScreeningRequired=false;
		boolean isBstSource=false;
		String documentUrl=null;
		boolean isOtherSource=false;
		String annualReportLink=null;
		JSONObject sourceInfo=new JSONObject();
		JSONArray sources=new JSONArray();
		String sourceValue=null;
		/*if(sourceInfo!=null && sourceInfo.names()!=null){
		for (int i = 0; i < sourceInfo.names().length(); i++) {
			if(sourceInfo.names().getString(i).equalsIgnoreCase(source))
				sources.put(0, sourceInfo.names().getString(i));
		}
		}*/
		try{
			sourceValue=getSource(jsonString, inputIdentifier, sourcesDtos, sourceInfo);
			//sources=sourceInfo.names();
			if (sourceValue == null) {
				sourceValue = CLIENT_ID;
			}

			if (sourceInfo.names() != null) {
				sources = sourceInfo.names();
			} else {
				sources.put(CLIENT_ID);
			}
			if ((source != null && "companyhouse.co.uk".equalsIgnoreCase(source))) {
				annualReportLink = buildMultiSourceUrl(inputIdentifier, "documents", null, null, null, CLIENT_ID);
				documentUrl = getAnnualReturnsLink(annualReportLink);
			}else{
				isBstSource=true;
			}
			if (!isRollBack) {
				if (source == null) {
					source = sourceValue;//getSource(jsonString, inputIdentifier, sourcesDtos, sourceInfo);
					sources=sourceInfo.names();
					if (source != null && "BST".equalsIgnoreCase(source)
							&& (jsonString.getUrl() != "" && jsonString.getUrl() != null)) {
						isBstSource = true;
					}
					if ((source != null && "companyhouse.co.uk".equalsIgnoreCase(source)) || !isBstSource) {
						annualReportLink = buildMultiSourceUrl(inputIdentifier, "documents", null, null, null, CLIENT_ID);
						documentUrl = getAnnualReturnsLink(annualReportLink);
						if (documentUrl != null) {
							isBstSource = false;
						} else if (jsonString.getUrl() != "" && jsonString.getUrl() != null) {
							isBstSource = true;
							JSONArray names = sourceInfo.names();
							if (names != null && names.length() > 0) {
								int credibility = 0;
								for (int j = 0; j < names.length(); j++) {
									if (j == 0 && !"companyhouse.co.uk".equalsIgnoreCase(names.getString(j))) {
										credibility = sourceInfo.getInt(names.getString(j));
										source = names.getString(j);
									} else if (credibility < sourceInfo.getInt(names.getString(j))
											&& !"companyhouse.co.uk".equalsIgnoreCase(names.getString(j))) {
										credibility = sourceInfo.getInt(names.getString(j));
										source = names.getString(j);
									}
								}
							}
						}
					}
				}
			}

			System.out.println("CORPORATE_STRUCTURE_LOCATION >>>>"+CORPORATE_STRUCTURE_LOCATION);
			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			file.createNewFile();
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File jsonfile = new File(classLoader.getResource("entityType.json").getFile());
			Object jsonfiledata = parser.parse(new FileReader(jsonfile));
			org.json.simple.JSONArray personTypes = (org.json.simple.JSONArray) jsonfiledata;
			File pepKeyFile = new File(classLoader.getResource("pepKey.json").getFile());
			Object pepKeyJson = parser.parse(new FileReader(pepKeyFile));
			org.json.simple.JSONObject pepTypes = (org.json.simple.JSONObject) pepKeyJson;

			File subsourcesFile = new File(classLoader.getResource("subsources.json").getFile());
			Object subSourcesData = parser.parse(new FileReader(subsourcesFile));
			org.json.simple.JSONObject subSources = (org.json.simple.JSONObject) subSourcesData;
			JSONArray array = new JSONArray();
			JSONObject isStatusErrorTrue = new JSONObject();
			int tempCount = 0;
			JSONObject links = null;
			List<List<JSONObject>> list = new ArrayList<>();
			List<List<JSONObject>> allShareholders = new ArrayList<>();
			JSONObject persons = new JSONObject();
			JSONObject holders = new JSONObject();
			 String clientIdentifier=new String();
			JSONObject map = new JSONObject();
			//JSONObject allShareHoldersInfo = new JSONObject();
			List<JSONObject> holdingPreviousLeveltData=new ArrayList<JSONObject>();
			if ((jsonString.getUrl() != "" && jsonString.getUrl() != null) && isBstSource
					&& (source != null && !source.equalsIgnoreCase("companyhouse.co.uk"))) {
				boolean flag = true;
				String url = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, null,CLIENT_ID);
				while (flag) {
					String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
					if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
						String response = serverResponse[1];
						if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
							JSONObject jsonResponse = new JSONObject(response);
							if (jsonResponse.has("_links")) {
								links = jsonResponse.getJSONObject("_links");
								if (links.has("graph:shareholders")) {
									flag = false;
									JSONArray graphShareholders = links.getJSONArray("graph:shareholders");
									if (graphShareholders!=null && graphShareholders.length() >= maxShareholderLevel) {
										try {
											// get data
											for (int i = 0; i < maxShareholderLevel; i++) {
												list = getLevelwiseData(list, i, url, array,
														maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
														inputIdentifier, maxSubsidiarielevel, array.length(),
														organisationName, juridiction, annualReportLink, personTypes,
														pepTypes, executor, persons, allShareholders, map, userId,
														isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier);
												
												
												/*if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}*/
												
												// check if the source status has error true
												if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
													break;
												}else if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}

											}
										} catch (Exception e) {
											e.printStackTrace();
											if (array.length() > 0) {
												array.getJSONObject(array.length()-1).put("status", "completed");
											}
										}

									} else if (links.has("next")) {
										try {
											int counter = 0;
											for (int i = 0; i < graphShareholders.length(); i++) {
												counter = i;
												// get data
												list = getLevelwiseData(list, counter, url, array,
														maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
														inputIdentifier, maxSubsidiarielevel, array.length(),
														organisationName, juridiction, annualReportLink, personTypes,
														pepTypes, executor, persons, allShareholders, map, userId,
														isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier);
												/*if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}*/
												
												if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
													break;
												}else if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}
											}
											while (counter < maxShareholderLevel) {
												boolean nextLevel = getNextLevel(jsonString.getUrl(), counter);
												if (nextLevel) {
													// get data
													list = getLevelwiseData(list, counter, url, array,
															maxShareholderLevel, lowRange, highRange, noOfSubsidiaries,
															file, inputIdentifier, maxSubsidiarielevel, array.length(),
															organisationName, juridiction, annualReportLink, personTypes,
															pepTypes, executor, persons, allShareholders, map, userId,
															isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier);
													/*if (array.length() > 0 && "completed".equalsIgnoreCase(
															array.getJSONObject(array.length() - 1).getString("status"))) {
														break;
													}*/
													
													if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
														break;
													}else if (array.length() > 0 && "completed".equalsIgnoreCase(
															array.getJSONObject(array.length() - 1).getString("status"))) {
														break;
													}
													
													counter++;
												} else {
													counter++;
													break;
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
											if (array.length() > 0) {
												array.getJSONObject(array.length()-1).put("status", "completed");
											}
										}
									} else {
										try {
											for (int i = 0; i < graphShareholders.length(); i++) {
												// get data
												list = getLevelwiseData(list, i, url, array,
														maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
														inputIdentifier, maxSubsidiarielevel, array.length(),
														organisationName, juridiction, annualReportLink, personTypes,
														pepTypes, executor, persons, allShareholders, map, userId,
														isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier);
												/*if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}*/
												
												if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
													break;
												}else if (array.length() > 0 && "completed".equalsIgnoreCase(
														array.getJSONObject(array.length() - 1).getString("status"))) {
													break;
												}
												
												if (i == list.size())
													break;
											}
										} catch (Exception e) {
											e.printStackTrace();
											if (array.length() > 0) {
												array.getJSONObject(array.length()-1).put("status", "completed");
											}
										}
									}
								}
							}
						}
					}
					Thread.sleep(10000);
				}
			} else {
				try {
					if(source.equalsIgnoreCase("companyhouse.co.uk")) {
						String url = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, null,CLIENT_ID);
						isStatusErrorTrue.put("source", "true");
						for (int i = 0; i < maxShareholderLevel; i++) {
							list = getLevelwiseData(list, i, url, array,
									maxShareholderLevel, lowRange, highRange, noOfSubsidiaries, file,
									inputIdentifier, maxSubsidiarielevel, array.length(),
									organisationName, juridiction, annualReportLink, personTypes,
									pepTypes, executor, persons, allShareholders, map, userId,
									isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier);
							
							
							/*if (array.length() > 0 && "completed".equalsIgnoreCase(
									array.getJSONObject(array.length() - 1).getString("status"))) {
								break;
							}*/
							
							// check if the source status has error true
							if(isStatusErrorTrue.has("flag") && isStatusErrorTrue.getString("flag").equalsIgnoreCase("true")){
								break;
							}else if (array.length() > 0 && "completed".equalsIgnoreCase(
									array.getJSONObject(array.length() - 1).getString("status"))) {
								break;
							}

						}
						
					}else {
					getLevelwiseData(list, null, jsonString.getUrl(), array, maxShareholderLevel, lowRange, highRange, noOfSubsidiaries,
							file, inputIdentifier, maxSubsidiarielevel, tempCount, organisationName, juridiction,
							annualReportLink, personTypes, pepTypes, executor, persons, allShareholders, map, userId,
							isSubsidiariesRequired, startDate, endDate,documentUrl,isOtherSource,sourceInfo,path,isScreeningRequired,holders,source,sources,subSources,holdingPreviousLeveltData, isStatusErrorTrue,clientIdentifier);
					}
				
				} catch (Exception e) {
					e.printStackTrace();
					if (array.length() > 0) {
						array.getJSONObject(array.length()-1).put("status", "completed");
					}
				}
			}
			return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	private String getSource(HierarchyDto jsonString,String inputIdentifier,List<SourcesDto> sourcesDtos, JSONObject sourceInfo) throws UnsupportedEncodingException, InterruptedException {
		//JSONObject shareHolderLinks =new JSONObject();
		//JSONObject object = new JSONObject();
		int count=0;
		JSONArray sources=new JSONArray();
		if ((jsonString.getUrl() != "" && jsonString.getUrl() != null)) {
			boolean flag = true;
			String url = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, null,CLIENT_ID);
			while (flag) {
				String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
				if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
					String response = serverResponse[1];
					if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
						JSONObject jsonResponse = new JSONObject(response);
						if (jsonResponse != null && jsonResponse.has("statuses")) {
							flag = false;
							JSONObject statuses = jsonResponse.getJSONObject("statuses");
							if (statuses != null) {
								sources = statuses.names();
							}
						} else {
							if (count == 5) {
								flag = false;
								sources.put(CLIENT_ID);
							}
						}
						/*if (jsonResponse.has("_links")) {
							shareHolderLinks = jsonResponse.getJSONObject("_links");
							if (shareHolderLinks.has("graph:shareholders")) {
								flag = false;
								JSONArray graphShareholders = shareHolderLinks.getJSONArray("graph:shareholders");
								String href = buildMultiSourceUrl(inputIdentifier, "graph:shareholders", "graph", null, 0,CLIENT_ID);
								byte[] bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,BE_ENTITY_API_KEY);
								String json = null;
								if (bytes != null)
									json = new String(bytes);
								if (json != null && json.startsWith("{"))
									object = new JSONObject(json);
							}
						}*/
						count++;
					}
				}
			}
			Thread.sleep(5000);
		}
		if(sources!=null && sources.length()==1 && sources.get(0).toString().equalsIgnoreCase(CLIENT_ID)) {
			return sources.getString(0);
		}
		//JSONArray sources=object.names();
		if (sources != null) {
			sources.put("companyhouse.co.uk");
			if (sources != null && sources.length() > 0) {
				JSONObject otherSourceInfo = new JSONObject();
				for (int i = 0; i < sources.length(); i++) {
					for (SourcesDto sourcesDto : sourcesDtos) {
						if (sources.getString(i).equalsIgnoreCase(sourcesDto.getSourceName())) {
							List<ClassificationsDto> classifications = sourcesDto.getClassifications();
							for (ClassificationsDto classificationsDto : classifications) {
								List<SubClassificationsDto> subClassifications = classificationsDto
										.getSubClassifications();
								for (SubClassificationsDto subClassificationsDto : subClassifications) {
									if (subClassificationsDto.getSubClassifcationName()
											.equalsIgnoreCase("Corporate Structure")) {
										sourceInfo.put(sources.getString(i),
												subClassificationsDto.getSubClassificationCredibility().ordinal());
										if (!sourcesDto.getSourceDisplayName().equalsIgnoreCase("bst") && !sourcesDto
												.getSourceDisplayName().equalsIgnoreCase("company house")) {
											otherSourceInfo.put(sources.getString(i),
													subClassificationsDto.getSubClassificationCredibility().ordinal());
										}
									}
								}
							}
						}
					}
				}
			}
		}

		String source=null;
		int credibility=0;
		JSONArray names= new JSONArray();
		if (sourceInfo.names() != null) {
			names = sourceInfo.names();
		} else {
			names.put(CLIENT_ID);
		}
		if(names!=null && names.length()>0){
			for (int j = 0; j < names.length(); j++) {
				if(j==0){
					credibility=sourceInfo.getInt(names.getString(j));
					source=names.getString(j);
				}else if(credibility<sourceInfo.getInt(names.getString(j))){
					credibility=sourceInfo.getInt(names.getString(j));
					source=names.getString(j);
				}
			}
		}
		return source;
	}

	@Override
	public String readCorporateStructure(String path) throws JSONException, Exception {
		JSONObject response = new JSONObject();
		try {
			JSONParser parser = new JSONParser();
			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			if (file.exists() && file.length() > 0) {
				FileReader reader = new FileReader(CORPORATE_STRUCTURE_LOCATION + path + ".json");
				Object obj = parser.parse(reader);
				org.json.simple.JSONArray mainResponse = (org.json.simple.JSONArray) obj;
				if(!mainResponse.isEmpty()){
					org.json.simple.JSONObject firstJson = (org.json.simple.JSONObject) mainResponse.get(0);
					//org.json.JSONObject firstJson = (org.json.JSONObject) mainResponse.get(0);
					//if(firstJson.containsKey("status") && "completed".equalsIgnoreCase(firstJson.get("status").toString())){
						if(firstJson.containsKey("status") && "completed".equalsIgnoreCase(firstJson.get("status").toString())){
							// Prateek
//						Set<String> keys = firstJson.keySet();
//						if(keys.size() == 2){
//							response.put("status", "completed");
//							response.put("data", mainResponse);
//							return response.toString();
//							
//						}
							/// new
							if(firstJson.containsKey("error")) {
								response.put("status", "completed");
								response.put("data", mainResponse);
								return response.toString();
							}
							///
						
					}
					//return response.toString();
				}
				org.json.simple.JSONObject mainEntity = (org.json.simple.JSONObject) mainResponse.get(0);
				String mainEntityId = null;
				String mainEntityName = null;
				if (mainEntity.containsKey("identifier") && mainEntity.get("identifier") != null) {
					mainEntityId = (String) mainEntity.get("identifier").toString();
				}
				if (mainEntity.containsKey("name") && mainEntity.get("name") != null) {
					mainEntityName = (String) mainEntity.get("name").toString();
				}

				// fetching list of watch lists
				/*List<SignificantWatchList> significantWatchLists = significantWatchListService
						.fetchWtachListByEntityId(mainEntityId);
				for (int i = 0; i < mainResponse.size(); i++) {
					org.json.simple.JSONObject json = (org.json.simple.JSONObject) mainResponse.get(i);
					String identifier = null;
					String name = null;
					if (json.containsKey("identifier")) {
						if (json.get("identifier") != null)
							identifier = json.get("identifier").toString();
					}
					if (json.containsKey("name")) {
						if (json.get("name") != null)
							name = json.get("name").toString();
					}
					getSignificantPep(json, mainEntityId, mainEntityName, identifier, name, false,
							significantWatchLists);
					if (i == 0) {
						if (json.containsKey("officership")) {
							org.json.simple.JSONArray officership = (org.json.simple.JSONArray) json.get("officership");
							if (officership != null) {
								for (int j = 0; j < officership.size(); j++) {
									org.json.simple.JSONObject officer = (org.json.simple.JSONObject) officership
											.get(j);
									String officerName = null;
									if (officer.containsKey("name")) {
										if (officer.get("name") != null)
											officerName = officer.get("name").toString();
									}
									getSignificantPep(officer, mainEntityId, mainEntityName, officerName, officerName,
											true, significantWatchLists);
								}
							}
						}
					}
					if (json.containsKey("subsidiaries")) {
						org.json.simple.JSONArray subsidiaries = (org.json.simple.JSONArray) json.get("subsidiaries");
						if (subsidiaries != null) {
							for (int j = 0; j < subsidiaries.size(); j++) {
								org.json.simple.JSONObject subsidiarie = (org.json.simple.JSONObject) subsidiaries
										.get(j);
								String subsidiarieId = null;
								String subsidiarieName = null;
								if (subsidiarie.containsKey("identifier")) {
									if (subsidiarie.get("identifier") != null)
										subsidiarieId = subsidiarie.get("identifier").toString();
								}
								if (subsidiarie.containsKey("name")) {
									if (subsidiarie.get("name") != null)
										subsidiarieName = subsidiarie.get("name").toString();
								}
								getSignificantPep(subsidiarie, mainEntityId, mainEntityName, subsidiarieId,
										subsidiarieName, false, significantWatchLists);
							}
						}
					}
				}*/
				if (mainResponse.size() > 0) {
					org.json.simple.JSONObject lastObject = (org.json.simple.JSONObject) mainResponse
							.get(mainResponse.size() - 1);
					String status = (String) lastObject.get("status");
					if (status == null) {
						response.put("status", "inprogress");
					} else {
						response.put("status", status);
					}
					response.put("data", mainResponse);
					return response.toString();
				}
			} else {
				response.put("status", "inprogress");
				response.put("data", new JSONArray());
				return response.toString();

			}
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "inprogress");
			response.put("data", new JSONArray());
			return response.toString();

		}
		return response.toString();

	}

	public boolean getNextLevel(String url, int currentLevel) throws InterruptedException {
		boolean isReady = false;
		boolean flag = true;
		String levelVal1 = null;
		JSONObject links = null;
		while (flag) {
			String serverResponse1[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
			if (serverResponse1[0] != null && Integer.parseInt(serverResponse1[0]) == 200) {
				String response1 = serverResponse1[1];
				if (!StringUtils.isEmpty(response1) && serverResponse1[1].trim().length() > 0) {
					JSONObject jsonResponse1 = new JSONObject(response1);
					if(jsonResponse1.has("_links"))
						links = jsonResponse1.getJSONObject("_links");
					if (links.has("next")) {
						Map<String, String> query_pairs = new LinkedHashMap<String, String>();
						String nextLink = links.getString("next");
						String[] pairs = nextLink.split("&");
						for (String pair : pairs) {
							int idx = pair.indexOf("=");
							query_pairs.put((pair.substring(0, idx)), (pair.substring(idx + 1)));
						}
						levelVal1 = query_pairs.get("level");
						if (Integer.parseInt(levelVal1) > (currentLevel + 1)) {
							isReady = true;
							flag = false;
						}
					} else {
						if(links.has("graph:shareholders")){
							JSONArray shareholders = links.getJSONArray("graph:shareholders");
							if (shareholders.length() < (currentLevel + 1)) {
								isReady = false;
								flag = false;
							}
						}
					}
					Thread.sleep(10000);
				}
			}
		}
		return isReady;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<List<JSONObject>> getLevelwiseData(List<List<JSONObject>> list, Integer level, String url,
			JSONArray array, Integer maxShareholderLevel, Integer lowRange, Integer highRange, Integer noOfSubsidiaries,
			File file, String identifier, Integer maxSubsidiarielevel, Integer tempCount, String requestOrganisation,
			String juridiction, String annualReportLink, org.json.simple.JSONArray personTypes,
			org.json.simple.JSONObject pepTypes, ThreadPoolExecutor executor, JSONObject persons,
			List<List<JSONObject>> allShareholders, JSONObject map, Long userId, Boolean isSubsidiariesRequired,
			String startDate, String endDate, String documentUrl, boolean isOtherSource, JSONObject sourceInfo,
			String path, boolean isScreeningRequired, JSONObject holders, String source, JSONArray sources, org.json.simple.JSONObject subSources,  List<JSONObject> holdingPreviousLeveltData, JSONObject isStatusErrorTrue,String clientIdentifier) throws Exception {
		boolean isBstDataThere = false;
		String errorMsg=null;
		JSONObject holdingDataForNextLevel = new JSONObject();
		if (level != null) {
			JSONObject links = new JSONObject();
			JSONArray shareholders = new JSONArray();
			String serverResponse1[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url,
					BE_ENTITY_API_KEY);
			if (serverResponse1[0] != null && Integer.parseInt(serverResponse1[0]) == 200) {
				String response1 = serverResponse1[1];
				if (!StringUtils.isEmpty(response1) && serverResponse1[1].trim().length() > 0) {
					JSONObject jsonResponse1 = new JSONObject(response1);
					if (jsonResponse1.has("_links"))
						links = jsonResponse1.getJSONObject("_links");
					if (links.has("graph:shareholders"))
						shareholders = links.getJSONArray("graph:shareholders");
				}
			}
			List<JSONObject> refListWithInRange = new ArrayList<>();
			List<JSONObject> refListWithOutRange = new ArrayList<>();
			String href = buildMultiSourceUrl(identifier, "graph:shareholders", "graph", null, level, CLIENT_ID);
			byte[] bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, BE_ENTITY_API_KEY);
			String json = null;
			/*if (bytes != null)
				json = new String(bytes);
			if (json == null || !json.startsWith("{")) {
				int coutRepeat = 0;
				while (json == null || !json.startsWith("{")) {
					bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, BE_ENTITY_API_KEY);
					if (bytes != null)
						json = new String(bytes);
					coutRepeat = coutRepeat + 1;
					if (coutRepeat == 5)
						break;
					Thread.sleep(5000);
				}
			}*/
			
			//old code with the whole status check
			if (bytes != null) {
				json = new String(bytes);
				JSONObject data = new JSONObject(json);
				int check =0;
				while(data!=null && data.has("is-completed") && ! data.getBoolean("is-completed")) {
						bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,
								BE_ENTITY_API_KEY);
						if (bytes != null) {
							json = new String(bytes);
							data = new JSONObject(new String(bytes));
							check++;
						}
						Thread.sleep(3000);
						if(check==10)
							break;
					}
					
			}
			 
			// new code with the individual source status check
			if (bytes != null) {
				json = new String(bytes);
				JSONObject data = new JSONObject(json);
				// while(data!=null && data.has("is-completed") && !
				// data.getBoolean("is-completed")) {

				if (data.has("statuses")) {
					JSONObject statuses = data.getJSONObject("statuses");
					if (statuses != null) {
						sources = statuses.names();
						sources.put("companyhouse.co.uk");
					}
					String customKey=identifier+"_"+CLIENT_ID;
					//String clientIdentifier=null;
					if(data.has(CLIENT_ID)) {
						JSONObject clientID= data.getJSONObject(CLIENT_ID);
						JSONArray idsClient = clientID.names();
						//customkey
					
						if (level == 0 && !isStatusErrorTrue.has("clientIdentifier")) {
							for (int i = 0; i < idsClient.length(); i++) {
								if(idsClient.getString(i).equals(customKey))
									isStatusErrorTrue.put("clientIdentifier", customKey);
									//clientIdentifier=customKey;
							}
						}
					}
					for (int i = 0; i < sources.length(); i++) {
						if (statuses.has(source) && source.equalsIgnoreCase(sources.get(i).toString())) {
							JSONObject statucSource = statuses.getJSONObject(sources.get(i).toString());
							if (statucSource.has("level") && statucSource.has("graph")) {
								JSONObject levelObj = statucSource.getJSONObject("level");
								JSONObject graphObj = statucSource.getJSONObject("graph");
								if (levelObj.has("is-completed") && graphObj.has("is-completed")) {
									
									if (levelObj.getBoolean("is-error") || graphObj.getBoolean("is-error")) {
										if(isStatusErrorTrue.has("clientIdentifier") &&isStatusErrorTrue.getString("clientIdentifier").equals(customKey))
											isStatusErrorTrue.put("flag", "false");
										else
											isStatusErrorTrue.put("flag", "true");
										
										errorMsg="Failed to get ownership structure from the source";
//										array.put(new JSONObject().put("country", "Germany"));
//										//array.getJSONObject(0).put("country","Germany");
//										array.getJSONObject(0).put("entity_id","orgChartmainEntity");
//										array.getJSONObject(0).put("entity_type","organization");
//										array.getJSONObject(0).put("id","p00");
//										array.getJSONObject(0).put("identifier",identifier);
//										array.getJSONObject(0).put("indirectPercentage","100");
//										array.getJSONObject(0).put("jurisdiction",juridiction);
//										array.getJSONObject(0).put("parents",new JSONArray());
//										array.getJSONObject(0).put("source_evidence",source);
//										array.getJSONObject(0).put("name",requestOrganisation);
//										array.getJSONObject(0).put("title",requestOrganisation);
//										array.getJSONObject(0).put("totalPercentage",100);
//										array.getJSONObject(0).put("screeningFlag",true);
//										if(graphObj.getString("error-msg").length()>0)
//											array.getJSONObject(0).put("error",graphObj.getString("error-msg"));
//										else
//											array.getJSONObject(0).put("error","Source not present");
//										array.getJSONObject(0).put("sources", sources);
//
//										
//										array.getJSONObject(array.length() - 1).put("status", "completed");
//
//									
//										
//										writeFile(file,array);
										//return list;
									}else{
										while (levelObj.getBoolean("is-completed") != true
												&& graphObj.getBoolean("is-completed") != true) {


											bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,
													BE_ENTITY_API_KEY);
											if (bytes != null) {
												json = new String(bytes);
												data = new JSONObject(new String(bytes));
											}
											Thread.sleep(5000);
										}
									}
								}

							}
						}
					
					}
				}

				/*
				 * bytes = ServiceCallHelper.
				 * downloadFileFromServerWithoutTimeoutWithApiKey(href,
				 * BE_ENTITY_API_KEY); if (bytes != null) { json = new
				 * String(bytes); data = new JSONObject(new String(bytes)); }
				 * Thread.sleep(5000);
				 */
				// }
			}
			
			JSONObject object = new JSONObject();
			if (json != null && json.startsWith("{"))
				object = new JSONObject(json);
			if (list.size() > 0) {
				for (JSONObject prevJson : list.get(list.size() - 1)) {
					if (prevJson.has("isCustom")) {
						if (!prevJson.getBoolean("isCustom")) {
							isBstDataThere = true;
							break;
						}
					}
				}
			}
			JSONObject bstData = new JSONObject();
			JSONObject clientData = new JSONObject();
			/*if (source != null && !"BST".equalsIgnoreCase(source)) {
				int count=0;
				while (!object.has(source)) {
					bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href, BE_ENTITY_API_KEY);
					if (bytes != null)
						json = new String(bytes);
					if (json == null || !json.startsWith("{")) {
						int coutRepeat = 0;
						while (json == null || !json.startsWith("{")) {
							bytes = ServiceCallHelper.downloadFileFromServerWithoutTimeoutWithApiKey(href,
									BE_ENTITY_API_KEY);
							if (bytes != null)
								json = new String(bytes);
							coutRepeat = coutRepeat + 1;
							if (coutRepeat == 5)
								break;
							Thread.sleep(5000);
						}
					}
					if (json != null && json.startsWith("{"))
						object = new JSONObject(json);
					count++;
					if (count == 5)
						break;
					Thread.sleep(2000);
				}
			}*/
			boolean isDataFromBvd = false;
			if (object.has(source) && (isBstDataThere || level == 0)) {
				bstData = object.getJSONObject(source);
			}/*else if(level==0){
				while(!object.has(source)){
					switchToOtherSource(sourceInfo,source);
				}
				bstData = object.getJSONObject(source);
			}*/
			if (object.has(CLIENT_ID)) {
				clientData = object.getJSONObject(CLIENT_ID);
			}
			JSONArray ids = new JSONArray();
			JSONArray idsFromClient = clientData.names();
			ids = bstData.names();
			/*if (idsFromClient != null) {
				for (int i = 0; i < idsFromClient.length(); i++) {
					if (ids == null) {
						ids = new JSONArray();
					}
					//
					if (level==0 && ids != null && ids.length() > 0) {
							if (!ids.getString(0).equals(idsFromClient.getString(i)))
								ids.put(ids.getString(0));
							else
								ids.put(idsFromClient.getString(i));
					}
					else {
						ids.put(idsFromClient.getString(i));
					}
					//
				}
			}*/
			String customKey=identifier+"_"+CLIENT_ID;
			if (level == 0) {
				if (idsFromClient != null) {
					for (int i = 0; i < idsFromClient.length(); i++) {
						if (ids != null && ids.length() > 0) {
							if (!ids.getString(0).equals(idsFromClient.getString(i)))
								ids.put(ids.getString(0));
						}
					}
				}
			} else {
				if (idsFromClient != null) {
					for (int i = 0; i < idsFromClient.length(); i++) {
						if (ids == null)
							ids = new JSONArray();
						ids.put(idsFromClient.getString(i));
					}
				}
			}
			if (level == 0 && ids == null) {
				ids = new JSONArray();
				if (idsFromClient != null && idsFromClient.length() > 0) {
					for (int i = 0; i < idsFromClient.length(); i++) {
						if (idsFromClient.getString(i).equals(customKey))
							ids.put(customKey);
					}
				}
			}
			JSONObject prevoiuseShareHolders=new JSONObject();
			if(holdingPreviousLeveltData!=null && holdingPreviousLeveltData.size()>0)
				prevoiuseShareHolders=holdingPreviousLeveltData.get(holdingPreviousLeveltData.size()-1);
			if (ids == null)
				ids = new JSONArray();
			org.json.simple.JSONArray presentLevelArray = new org.json.simple.JSONArray();
			if ((ids != null && ids.length() > 0) || level != 0) {
				List<JSONObject> previousLists = new ArrayList<>();
				if (allShareholders.size() > 0 && (list.size() == level))
					previousLists = allShareholders.get(allShareholders.size() - 1);
				else if (list.size() == level) {
					for (int i = 0; i < ids.length(); i++) {
						if(!presentLevelArray.contains(ids.get(i).toString()) && !"status".equalsIgnoreCase(ids.get(i).toString()))
							presentLevelArray.add(ids.get(i).toString());
					}
				}
				JSONArray presentIds = new JSONArray();
				for (JSONObject jsonObject : previousLists) {
					if(jsonObject.has("parentIds"))
						presentIds = jsonObject.getJSONArray("parentIds");
					for (int i = 0; i < presentIds.length(); i++) {
						String parentId = presentIds.getJSONObject(i).getString("parentId");
						for (int j = 0; j < ids.length(); j++) {
							if (parentId.equals(ids.getString(j)) && !presentLevelArray.contains(ids.getString(j)) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								presentLevelArray.add(ids.getString(j));
								break;
							} else if (persons.has(parentId) && !presentLevelArray.contains(parentId) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								presentLevelArray.add(parentId);
								break;
							} else if (presentIds.getJSONObject(i).has("basic")
									&& !presentLevelArray.contains(parentId) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								presentLevelArray.add(parentId);
								break;
							}else if (isBstDataThere && prevoiuseShareHolders.has(parentId)
									&& !presentLevelArray.contains(parentId) &&
									!"status".equalsIgnoreCase(ids.get(j).toString())) {
								if (jsonObject.has("indirectPercentage")) {
									if ((jsonObject.getDouble("indirectPercentage") >= lowRange
											&& jsonObject.getDouble("indirectPercentage") <= highRange)) {
										presentLevelArray.add(parentId);
										break;
									}
								}
							}
						}
					}
				}
				if(presentLevelArray!=null && presentLevelArray.size()>0){
					for (int j = 0; j < presentLevelArray.size(); j++) {
						JSONObject mapJson = new JSONObject();
						JSONObject tempJson = new JSONObject();
						JSONObject presentJson = new JSONObject();
						JSONObject clientJson = new JSONObject();
						JSONObject basicJson = new JSONObject();
						JSONObject person = new JSONObject();
						String user = null;
						boolean isCustom = false;
						String organisationName = null;
						String hasURL = null;
						String shareHolderJuridiction = null;
						String country = null;
						String entityType = null;
						double indirectPercentage = 0;
						double totalPercentage = 0;
						String innerSource=null;
						String from=null;
						boolean needParents=true;
						String dateOfBirth=null;
						String role=null;
						String classification=null;
						if (holders.has(presentLevelArray.get(j).toString())) {
							tempJson = holders.getJSONObject(presentLevelArray.get(j).toString());
						}
						for (JSONObject jsonObject : previousLists) {
							if (jsonObject.has("parentIds") && jsonObject.get("parentIds") instanceof JSONArray)
								presentIds = jsonObject.getJSONArray("parentIds");
							if (presentIds != null && presentIds.length() > 0) {
								for (int i = 0; i < presentIds.length(); i++) {
									String parentId = presentIds.getJSONObject(i).getString("parentId");
									if (presentLevelArray.get(j).toString().equalsIgnoreCase(parentId)) {
										String subSource = null;
										if (presentIds.getJSONObject(i).has("basic")) {
											basicJson = presentIds.getJSONObject(i).getJSONObject("basic");
										}
										if (presentIds.getJSONObject(i).has("user")) {
											user = presentIds.getJSONObject(i).getString("user");
										}
										if (presentIds.getJSONObject(i).has("isCustom")) {
											isCustom = presentIds.getJSONObject(i).getBoolean("isCustom");
										}
										///
										if (presentIds.getJSONObject(i).has("dateOfBirth")) {
											dateOfBirth = presentIds.getJSONObject(i).getString("dateOfBirth");
										}
										if (presentIds.getJSONObject(i).has("role")) {
											role = presentIds.getJSONObject(i).getString("role");
										}

										if (presentIds.getJSONObject(i).has("classification")) {
											classification = presentIds.getJSONObject(i).getString("classification");
										}
										///
										if (presentIds.getJSONObject(i).has("source")) {
											subSource = presentIds.getJSONObject(i).getString("source");
											if (subSource != null) {
												if (subSources.get(subSource) != null) {
													innerSource = subSources.get(subSource).toString();
												}
											}

										}
										if (presentIds.getJSONObject(i).has("from")) {
											from = presentIds.getJSONObject(i).getString("from");
										}
										if (presentIds.getJSONObject(i).has("source")) {
											subSource = presentIds.getJSONObject(i).getString("source");
											if (subSource != null) {
												if (subSources.get(subSource) != null) {
													innerSource = subSources.get(subSource).toString();
												}
											}

										}
										if (presentIds.getJSONObject(i).has("from")) {
											from = presentIds.getJSONObject(i).getString("from");
										}
									}
								}
							}
						}
						if (clientData.has(presentLevelArray.get(j).toString())) {
							clientJson = clientData.getJSONObject(presentLevelArray.get(j).toString());
						}
						if (basicJson != null && !basicJson.has("vcard:organization-name")
								&& bstData.has(presentLevelArray.get(j).toString())) {
							presentJson = bstData.getJSONObject(presentLevelArray.get(j).toString());
							
							if(presentJson.has("basic"))
								basicJson = presentJson.getJSONObject("basic");
						} else if (!(bstData.has(presentLevelArray.get(j).toString()))
								&& (clientData.has(presentLevelArray.get(j).toString()))) {
							presentJson = clientData.getJSONObject(presentLevelArray.get(j).toString());
							basicJson = presentJson.getJSONObject("basic");
						} else if (persons.has(presentLevelArray.get(j).toString())) {
							mapJson = persons.getJSONObject(presentLevelArray.get(j).toString());
						}else if (prevoiuseShareHolders.has(presentLevelArray.get(j).toString())) {
							presentJson = prevoiuseShareHolders.getJSONObject(presentLevelArray.get(j).toString());
							if (presentJson.has("basic"))
								basicJson = presentJson.getJSONObject("basic");
							needParents = false;
						}
						if(presentJson!=null && presentJson.has("basic")){
							holdingDataForNextLevel.put(presentLevelArray.get(j).toString(),presentJson);
						}
						if (!mapJson.has("id")) {
							
							if (presentJson.has("entity_type")) {
								if ((presentJson.get("entity_type") instanceof String)) {
									entityType = presentJson.getString("entity_type");
								} else {
									entityType = "C";
								}
								if (entityType != null) {
									if ("S".equalsIgnoreCase(entityType)) {
										mapJson.put("news", new JSONArray());
										mapJson.put("screeningFlag", true);
									}
									if (personTypes.contains(entityType))
										entityType = "person";
									else
										entityType = "organization";
								}
							} else if (basicJson.has("entity_type")) {
								entityType = basicJson.getString("entity_type");
								if (personTypes.contains(entityType)) {
									entityType = "person";
								} else {
									entityType = "organization";
								}
								
							} else {
								entityType = "organization";
							}
							////
							if (presentJson.has("dateOfBirth")) {
								dateOfBirth = presentJson.getString("dateOfBirth");
								mapJson.put("dateOfBirth", presentJson.getString("dateOfBirth"));
							}
							
							if (presentJson.has("role")) {
								role = presentJson.getString("role");
								mapJson.put("role", presentJson.getString("role"));
							}
							
							if (presentJson.has("classification")) {
								classification = presentJson.getString("classification");
								mapJson.put("classification", presentJson.getString("classification"));
							}
							
							///
							if (basicJson.has("vcard:organization-name")) {
								organisationName = basicJson.getString("vcard:organization-name");
							} else if (basicJson.has("vcard:hasName")) {
								organisationName = basicJson.getString("vcard:hasName");
							}
							if (basicJson.has("isDomiciledIn"))
								shareHolderJuridiction = basicJson.getString("isDomiciledIn");
							if (basicJson.has("hasURL"))
								hasURL = basicJson.getString("hasURL");
							if (basicJson.has("mdaas:RegisteredAddress")) {
								JSONObject registeredAddress = basicJson.getJSONObject("mdaas:RegisteredAddress");
								if (registeredAddress.has("country")) {
									country = registeredAddress.getString("country");
								}
							}
							if (country != null)
								mapJson.put("country", country);
							else
								mapJson.put("country", "");
							if (level!=null && level == 0) {
								if (requestOrganisation.equalsIgnoreCase("undefined"))
									mapJson.put("title", organisationName);
								else
									mapJson.put("title", requestOrganisation);
								mapJson.put("jurisdiction", juridiction);
								
								mapJson.put("identifier", identifier);
								if(errorMsg!=null || isStatusErrorTrue.has("source")) {
									mapJson.put("is-error", true);
									mapJson.put("error", errorMsg);
								}else
									mapJson.put("is-error", false);
									
								mapJson.put("entity_id", "orgChartmainEntity");
								JSONArray sourceToShow=new JSONArray();
								if (sources != null && sources.length()>0) {
									for (int i = 0; i < sources.length(); i++) {
										if (!"companyhouse.co.uk".equalsIgnoreCase(sources.getString(i))) {
											sourceToShow.put(sources.getString(i));
										}
									}
								}
								if(juridiction!=null && "GB".equalsIgnoreCase(juridiction))
									mapJson.put("sources", sources);
								else
									mapJson.put("sources", sourceToShow);

								indirectPercentage = 100;
								totalPercentage = 100;
							} else {
								mapJson.put("title", organisationName);
								if (shareHolderJuridiction != null)
									mapJson.put("jurisdiction", shareHolderJuridiction);
								else
									mapJson.put("jurisdiction", juridiction);
								
								
								mapJson.put("identifier", presentLevelArray.get(j).toString());
								
								mapJson.put("entity_id", "orgChartParentEntity");
								mapJson.put("classification", classification);
								mapJson.put("role", role);
								mapJson.put("dateOfBirth", dateOfBirth);
							}
							mapJson.put("bvdId", presentLevelArray.get(j).toString());
							if (user == null)
								mapJson.put("source_evidence", source);
							else
								mapJson.put("source_evidence", user);
							if (isCustom)
								mapJson.put("isCustom", isCustom);
							else
								mapJson.put("isCustom", false);
							mapJson.put("entity_type", entityType);
							mapJson.put("hasURL", hasURL);
							mapJson.put("basic", basicJson);
							mapJson.put("level", level);
							if(innerSource!=null)
								mapJson.put("innerSource", innerSource);
							else if(!isCustom)
								mapJson.put("innerSource", "BST");
							mapJson.put("from", from);
							if (level != 0) {
								mapJson.put("childLevel", level - 1);
							}
							if(basicJson.has("sourceUrl"))
								mapJson.put("sourceUrl", basicJson.getString("sourceUrl"));
							if(basicJson.has("dateOfBirth"))
								mapJson.put("dateOfBirth", basicJson.getString("dateOfBirth"));
							if (level!=0) {
								mapJson.put("childLevel", level-1);
							}
							mapJson.put("parents", new JSONArray());
							mapJson.put("name", mapJson.getString("title"));
							mapJson.put("isSubsidiarie", false);
						}
						if (!"person".equalsIgnoreCase(mapJson.getString("entity_type"))) {
							if (tempJson.has("id")) {
								mapJson.put("id", tempJson.getString("id"));
							} else {
								mapJson.put("id", "p" + level + j);
							}

						}
						double prevIndirectPercentage=0.0;
						double prevTotalPercentage=0.0;
						double presentTotalPercentage=0.0;
						JSONArray parentIds =null;
						for (JSONObject prevJsonObject : previousLists) {
							if (prevJsonObject.has("parentIds") && prevJsonObject.get("parentIds") instanceof JSONArray)
								parentIds = prevJsonObject.getJSONArray("parentIds");
							if (parentIds != null && parentIds.length() > 0) {
								for (int k = 0; k < parentIds.length(); k++) {
									String parentId = parentIds.getJSONObject(k).getString("parentId");
									if (parentId.equals(presentLevelArray.get(j).toString())) {
										if (prevJsonObject.has("indirectPercentage") && prevJsonObject.get("indirectPercentage") instanceof Double)
											prevIndirectPercentage = prevJsonObject.getDouble("indirectPercentage");
										if (prevJsonObject.has("indirectPercentage") && prevJsonObject.get("indirectPercentage") instanceof String)
											prevIndirectPercentage = Double.parseDouble(prevJsonObject.getString("indirectPercentage"));
										if (prevJsonObject.has("indirectPercentage") && prevJsonObject.get("indirectPercentage") instanceof Integer)
											prevIndirectPercentage = prevJsonObject.getInt("indirectPercentage");
										if (prevJsonObject.has("totalPercentage"))
											//////////////////////////////////////////////////////////
											prevTotalPercentage = prevJsonObject.getDouble("totalPercentage");
										if (parentIds.getJSONObject(k).has("totalPercentage"))
											totalPercentage = parentIds.getJSONObject(k).getDouble("totalPercentage");
										indirectPercentage = (totalPercentage * prevIndirectPercentage) / 100;
										double presentIndirectPercentage = (totalPercentage * prevIndirectPercentage)
												/ 100;
										if (parentIds.getJSONObject(k).has("totalPercentage"))
											presentTotalPercentage = parentIds.getJSONObject(k)
													.getDouble("totalPercentage");
										if (tempJson.has("indirectPercentage") && tempJson.has("isCustom")) {
											if (prevIndirectPercentage >= lowRange
													&& prevIndirectPercentage <= highRange) {
												totalPercentage = tempJson.getDouble("totalPercentage")
														+ totalPercentage;
												indirectPercentage = tempJson.getDouble("indirectPercentage")
														+ indirectPercentage;
												tempJson.put("indirectPercentage", indirectPercentage);
												tempJson.put("totalPercentage", totalPercentage);
											}
											mapJson.put("totalPercentage",
													tempJson.getDouble("totalPercentage") + totalPercentage);
											mapJson.put("indirectPercentage",
													tempJson.getDouble("indirectPercentage") + indirectPercentage);
										} else if (mapJson.has("indirectPercentage")
												&& "organization".equalsIgnoreCase(mapJson.getString("entity_type"))) {
											if (prevIndirectPercentage >= lowRange
													&& prevIndirectPercentage <= highRange) {
												indirectPercentage = mapJson.getDouble("indirectPercentage")
														+ indirectPercentage;
												totalPercentage = mapJson.getDouble("totalPercentage")
														+ totalPercentage;
											}
											mapJson.put("indirectPercentage", indirectPercentage);
											mapJson.put("totalPercentage", totalPercentage);
										} else {
											if (prevIndirectPercentage >= lowRange
													&& prevIndirectPercentage <= highRange) {
												mapJson.put("indirectPercentage", indirectPercentage);
												mapJson.put("totalPercentage", totalPercentage);
											}
										}
										if (mapJson.has("entity_type")) {
											String tempEntityType = mapJson.getString("entity_type");
											if ("person".equalsIgnoreCase(tempEntityType)
													&& !persons.has(presentLevelArray.get(j).toString())) {
												mapJson.put("id", "p" + level + j);
												mapJson.put("indirectPercentage", indirectPercentage);
												mapJson.put("totalPercentage", totalPercentage);
												mapJson.put("indirectChilds", new JSONArray());
												JSONArray parentArray = new JSONArray();
												getParents(presentJson, clientJson, indirectPercentage, parentArray);
												mapJson.put("parentIds", parentArray);
												checkEntityType(mapJson, organisationName, entityType);
												JSONObject indirectObject = new JSONObject();
												if (map.has(prevJsonObject.getString("id"))) {
													JSONArray indirectChilds = new JSONArray();
													indirectObject.put("id", prevJsonObject.getString("id"));
													indirectObject.put("totalPercentage", totalPercentage);
													indirectObject.put("indirectPercentage", indirectPercentage);
													indirectObject.put("level", prevJsonObject.getInt("level"));
													indirectChilds.put(indirectObject);
													mapJson.put("indirectChilds", indirectChilds);
												} else {
													JSONArray indirectChilds = new JSONArray();
													String childJson = prevJsonObject.getString("child");
													while (!map.has(childJson)) {
														for (List<JSONObject> listJsonObject : allShareholders) {
															for (JSONObject jsonObject : listJsonObject) {
																if (childJson.equals(jsonObject.getString("id"))) {
																	childJson = jsonObject.getString("child");
																	indirectObject.put("level",
																			jsonObject.getInt("level"));
																}
															}
														}
													}
													indirectObject.put("id", childJson);
													indirectObject.put("totalPercentage", totalPercentage);
													indirectObject.put("indirectPercentage", indirectPercentage);
													indirectChilds.put(indirectObject);
													mapJson.put("indirectChilds", indirectChilds);
												}
												persons.put(presentLevelArray.get(j).toString(), mapJson);
											} else if ("person".equalsIgnoreCase(mapJson.getString("entity_type"))
													&& persons.has(presentLevelArray.get(j).toString())) {
												JSONObject prevPerson = persons
														.getJSONObject(presentLevelArray.get(j).toString());
												double presentIndirect = indirectPercentage;
												mapJson.put("totalPercentage", totalPercentage);
												mapJson.put("id", prevPerson.getString("id"));
												indirectPercentage = indirectPercentage
														+ prevPerson.getDouble("indirectPercentage");
												prevPerson.put("indirectPercentage", indirectPercentage);
												JSONArray indirectChilds = prevPerson.getJSONArray("indirectChilds");
												JSONObject indirectObject = new JSONObject();
												if (map.has(prevJsonObject.getString("id"))) {
													boolean isAlreadChild = false;
													for (int i = 0; i < indirectChilds.length(); i++) {
														if (prevJsonObject.getString("id").equalsIgnoreCase(
																indirectChilds.getJSONObject(i).getString("id"))) {
															indirectChilds.getJSONObject(i)
																	.put("indirectPercentage",
																			indirectChilds.getJSONObject(i)
																					.getDouble("indirectPercentage")
																					+ presentIndirect);
															isAlreadChild = true;
														}
													}
													if (!isAlreadChild) {

														indirectObject.put("id", prevJsonObject.getString("id"));
														indirectObject.put("totalPercentage", totalPercentage);
														if (!isAlreadChild)
															indirectObject.put("indirectPercentage", presentIndirect);
														indirectObject.put("level", prevJsonObject.getInt("level"));
														indirectChilds.put(indirectObject);
													}
													prevPerson.put("indirectChilds", indirectChilds);
													mapJson.put("indirectChilds", indirectChilds);
												} else {
													String childJson = prevJsonObject.getString("child");
													while (!map.has(childJson)) {
														for (List<JSONObject> listJsonObject : allShareholders) {
															for (JSONObject jsonObject : listJsonObject) {
																if (childJson.equals(jsonObject.getString("id"))) {
																	childJson = jsonObject.getString("child");
																}
															}
														}
													}
													boolean isAlreadChild = false;
													for (int i = 0; i < indirectChilds.length(); i++) {
														if (childJson.equalsIgnoreCase(
																indirectChilds.getJSONObject(i).getString("id"))) {
															if (indirectChilds.getJSONObject(i)
																	.has("indirectPercentage")) {
																indirectChilds.getJSONObject(i).put(
																		"indirectPercentage",
																		indirectChilds.getJSONObject(i)
																				.getDouble("indirectPercentage")
																				+ presentIndirect);
															}
															isAlreadChild = true;
														}
													}
													if (!isAlreadChild) {
														indirectObject.put("level",
																map.getJSONObject(childJson).getInt("level"));
														indirectObject.put("id",
																map.getJSONObject(childJson).getString("id"));
														indirectObject.put("totalPercentage", totalPercentage);
														if (isAlreadChild)
															indirectObject.put("indirectPercentage", presentIndirect);
														indirectChilds.put(indirectObject);
													}
													prevPerson.put("indirectChilds", indirectChilds);
													mapJson.put("indirectChilds", indirectChilds);
												}
											}
										}
										if ((indirectPercentage >= lowRange && indirectPercentage <= highRange)
												|| (tempJson.has("indirectPercentage")
														&& tempJson.getDouble("indirectPercentage") >= lowRange
														&& tempJson.getDouble("indirectPercentage") <= highRange)) {
											checkParent(map, prevJsonObject, mapJson, level, j, allShareholders,
													persons, tempJson, holders, presentTotalPercentage,
													presentIndirectPercentage);
										} else if ("organization".equalsIgnoreCase(mapJson.getString("entity_type"))) {
											checkChild(map, prevJsonObject, mapJson, level, j, allShareholders);
										}
									}
								}
							}
						}
						if ("organization".equalsIgnoreCase(entityType)) {
							JSONArray parentArray = new JSONArray();
							if(needParents)
								getParents(presentJson, clientJson, indirectPercentage, parentArray);
							mapJson.put("parentIds", parentArray);
							mapJson.put("totalPercentage", totalPercentage);
							mapJson.put("indirectPercentage", indirectPercentage);
							// checking and adding entity type
							checkEntityType(mapJson, organisationName, entityType);
						}
						refListWithOutRange.add(mapJson);
						if ((indirectPercentage >= lowRange && indirectPercentage <= highRange) || 
								(tempJson.has("indirectPercentage") && tempJson.getDouble("indirectPercentage")>= lowRange
								&& tempJson.getDouble("indirectPercentage")<= highRange)) {
							if (!map.has(mapJson.getString("id"))) {
								map.put(mapJson.getString("id"), mapJson);
								holders.put(mapJson.getString("identifier"), mapJson);
								refListWithInRange.add(mapJson);
								array.put(mapJson);
								//finalJson.add(mapJson);
							} else if (map.has(mapJson.getString("id")) && "organization".equalsIgnoreCase(entityType)) {
								refListWithInRange.add(mapJson);
							}
						}
					}
				}else if(level==0){
					JSONObject mapJson = new JSONObject();
					mapJson.put("id", "p00");
					mapJson.put("identifier", identifier);
					mapJson.put("name", requestOrganisation);
					mapJson.put("title", requestOrganisation);
					mapJson.put("jurisdiction", juridiction);
					mapJson.put("level", 0);
					mapJson.put("parentIds", new JSONArray());
					mapJson.put("parents", new JSONArray());
					mapJson.put("entity_type", "organization");
					mapJson.put("entity_id", "orgChartmainEntity");
					mapJson.put("isSubsidiarie", false);
					mapJson.put("source_evidence", source);
					mapJson.put("screeningFlag", true);
					mapJson.put("subsidiaries", new JSONArray());
					mapJson.put("officership", new JSONArray());
					if (ids != null && ids.length() > 0 && "status".equalsIgnoreCase(ids.getString(0)))
						mapJson.put("isDataNotFound", true);
					refListWithInRange.add(mapJson);
					array.put(mapJson);
				}
				allShareholders.add(refListWithOutRange);
				holdingPreviousLeveltData.add(holdingDataForNextLevel);
				writeFile(file, array);
				if (refListWithInRange.size() > 0) {
					double conflictPercentage=0;
					list.add(refListWithInRange);
					/*if (isScreeningRequired) {
						executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, list, level, true,
								new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes,
								startDate, endDate, BE_ENTITY_API_KEY));
					} else {
						for (JSONObject jsonObject : list.get(level)) {
							jsonObject.put("screeningFlag", true);
						}
					}*/
					array.getJSONObject(array.length() - 1).put("status", "inprogress");
					while (array.length() > tempCount) {
						String companyName = array.getJSONObject(tempCount).getString("title");
						String shareHolderJuridiction = array.getJSONObject(tempCount).getString("jurisdiction");
						String country = array.getJSONObject(tempCount).getString("country");
						String website = null;
						if (array.getJSONObject(tempCount).has("hasURL"))
							website = array.getJSONObject(tempCount).getString("hasURL");
						/*if (isSubsidiariesRequired) {
							executor.submit(new MultiSourceSubsidiaries(identifier, array, noOfSubsidiaries,
									maxSubsidiarielevel, tempCount, companyName, shareHolderJuridiction, website,
									new AdvanceSearchServiceImpl(), BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor,
									personTypes, pepTypes, startDate, endDate, CLIENT_ID, BE_ENTITY_API_KEY));
						} else {
							array.getJSONObject(tempCount).put("subsidiaries", new JSONArray());
						}*/
						/*if (tempCount == 0) {
							String highCredibilitySource=getKeyManagementSource(keys);
							List<EntityOfficerInfoDto> listOfOfficer= entityOfficerInfoService.getEntityOfficerList(identifier);
							 
							executor.submit(new OfficershipScreening(array, identifier, juridiction, SCREENING_URL,
									BIGDATA_MULTISOURCE_URL, new AdvanceSearchServiceImpl(), country, file, personTypes,
									pepTypes, startDate, endDate, isScreeningRequired, BE_ENTITY_API_KEY, CLIENT_ID,highCredibilitySource,listOfOfficer));
						}*/
						tempCount = tempCount + 1;
					}
					try {
						if (((maxShareholderLevel - 1) == level)
								|| ((shareholders.length() - 1) == level && !(links.has("next")))
								|| (shareholders.length() == 0)) {
							for (int i = 0; i < array.length(); i++) {
								/*while (/*
										 * (!array.getJSONObject(i).has("subsidiaries")) ||
										  (array.getJSONObject(i).has("subsidiaries")
												&& array.getJSONObject(i).getJSONArray("subsidiaries").length() > 0
												&& array.getJSONObject(i).getJSONArray("subsidiaries")
												.getJSONObject(array.getJSONObject(i)
														.getJSONArray("subsidiaries").length() - 1)
												.getString("status").equals("pending"))
										/*|| (!array.getJSONObject(0).has("officership"))
										|| (array.getJSONObject(0).has("officership")
												&& array.getJSONObject(0).getJSONArray("officership").length() > 0
												&& array.getJSONObject(0).getJSONArray("officership").getJSONObject(
														array.getJSONObject(0).getJSONArray("officership").length() - 1)
												.getString("status").equals("pending"))
										|| (!array.getJSONObject(i).has("screeningFlag"))) {*/
									array.getJSONObject(array.length() - 1).put("status", "inprogress");
									Thread.sleep(10000);
									writeFile(file, array);
								//}
							}
							//array.getJSONObject(array.length() - 1).put("status", "completed");
							
							if(array != null && array.length() > 0){
								array.getJSONObject(array.length() - 1).put("status", "completed");
							}
							if(array.length() <= 0){
								JSONObject jsonObject = new JSONObject();
								array.put(jsonObject.put("status", "completed"));
							}
							
							getCustomShareholdersFromLatLevel(list, array,lowRange,highRange,holders,map);
							getFactualControl(array, persons);
							getConflicts(array,map,lowRange,highRange);
						} else {
							array.getJSONObject(array.length() - 1).put("status", "inprogress");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					writeFile(file, array);
				} else {
					try {
						for (int i = 0; i < array.length(); i++) {
							/* while (/*(!array.getJSONObject(i).has("subsidiaries"))
									|| (array.getJSONObject(i).has("subsidiaries")
											&& array.getJSONObject(i).getJSONArray("subsidiaries").length() > 0
											&& array.getJSONObject(i).getJSONArray("subsidiaries").getJSONObject(
													array.getJSONObject(i).getJSONArray("subsidiaries").length() - 1)
											.getString("status").equals("pending"))
									//|| (!array.getJSONObject(0).has("officership"))
									|| (array.getJSONObject(0).has("officership")
											&& array.getJSONObject(0).getJSONArray("officership").length() > 0
											&& array.getJSONObject(0).getJSONArray("officership").getJSONObject(
													array.getJSONObject(0).getJSONArray("officership").length() - 1)
											.getString("status").equals("pending"))
									|| (!array.getJSONObject(i).has("screeningFlag"))) {*/
								array.getJSONObject(array.length() - 1).put("status", "inprogress");
								Thread.sleep(10000);
								writeFile(file, array);
							//}
						}
						
						if(array != null && array.length() > 0){
							array.getJSONObject(array.length() - 1).put("status", "completed");
						}
						if(array.length() <= 0){
							JSONObject jsonObject = new JSONObject();
							array.put(jsonObject.put("status", "completed"));
						}
						//array.getJSONObject(array.length() - 1).put("status", "completed");
						getCustomShareholdersFromLatLevel(list, array,lowRange,highRange,holders,map);
						getFactualControl(array, persons);
						getConflicts(array,map,lowRange,highRange);
						writeFile(file, array);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				whenShareholderDataEmpty(identifier, requestOrganisation, juridiction, lowRange, highRange,
						noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, executor,
						userId, isSubsidiariesRequired, startDate, endDate, sourceInfo, url, path, isScreeningRequired,true,source,array,sources,errorMsg);
			}
		} else if(documentUrl!=null){
			whenShareholderDataEmpty(identifier, requestOrganisation, juridiction, lowRange, highRange,
					noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, executor,
					userId, isSubsidiariesRequired, startDate, endDate, sourceInfo, url, path, isScreeningRequired,true,source,array,sources,errorMsg);
		}else {
			whenSourcesNotFound(identifier, requestOrganisation, juridiction, lowRange, highRange,
					noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, 
					userId, isSubsidiariesRequired, startDate, endDate, path, isScreeningRequired,array);
		} 
		return list;
	}

	private void whenSourcesNotFound(String identifier, String requestOrganisation, String juridiction, Integer lowRange,
			Integer highRange, Integer noOfSubsidiaries, Integer maxSubsidiarielevel, File file,
			String annualReportLink, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,
			 Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate,
			String path, boolean isScreeningRequired, JSONArray array)
			throws FileNotFoundException, IOException, ParseException, InterruptedException {
		List<List<JSONObject>> refList = new ArrayList<List<JSONObject>>();
		JSONObject finalJson = new JSONObject();
		JSONObject sourceData = null;
		String overviewData = null;
		JSONObject result = null;
		JSONObject overviewDataJson = new JSONObject();
		finalJson.put("name", requestOrganisation);
		finalJson.put("title", requestOrganisation);
		try {
			String url = buildMultiSourceUrl(identifier, "overview", null, null, null, CLIENT_ID);
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(url);
			overviewData = getHierarchyData(hierarchyDto, null, BE_ENTITY_API_KEY);
			if (overviewData != null)
				overviewDataJson = new JSONObject(overviewData);
			if (overviewDataJson.has("is-completed")) {
				int counter = 0;
				while (overviewDataJson.has("is-completed") && !overviewDataJson.has("is-completed")) {
					overviewData = getHierarchyData(hierarchyDto, null, BE_ENTITY_API_KEY);
					if (overviewData != null)
						overviewDataJson = new JSONObject(overviewData);
					counter = counter + 1;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
		}
		String country = null;
		if (!overviewDataJson.has("results") || overviewDataJson.getJSONArray("results").length() == 0) {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
			Object fileJsonData = parser.parse(new FileReader(fileJSon));
			org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
			for (int k = 0; k < countryJson.size(); k++) {
				org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
				if (countryObj.get("countryCode").toString().equalsIgnoreCase(juridiction)) {
					country = countryObj.get("country").toString();
				}
			}
			finalJson.put("country", country);
			finalJson.put("name", requestOrganisation);
			finalJson.put("title", requestOrganisation);
		}
		if (overviewDataJson.has("results")) {
			JSONArray results = overviewDataJson.getJSONArray("results");
			if (results.length() > 0) {
				result = results.getJSONObject(0);
				if (result.has("overview")) {
					JSONArray addressArray = null;
					JSONObject address = null;
					JSONObject overview = result.getJSONObject("overview");
					JSONArray sources = overview.names();
					if (sources != null && sources.length() > 0) {
						sourceData = overview.getJSONObject(sources.getString(0));
						if (sourceData.has("vcard:organization-name")) {
							finalJson.put("name", sourceData.getString("vcard:organization-name"));
							finalJson.put("title", sourceData.getString("vcard:organization-name"));
						}
						if (sourceData.has("mdaas:RegisteredAddress")) {
							if (sourceData.get("mdaas:RegisteredAddress") instanceof JSONObject)
								address = sourceData.getJSONObject("mdaas:RegisteredAddress");
							else
								addressArray = sourceData.getJSONArray("mdaas:RegisteredAddress");
							if (addressArray != null && addressArray.length() > 0) {
								address = addressArray.getJSONObject(0);
							}
							if (address != null && address.has("country")) {
								country = address.getString("country");
							}
							finalJson.put("country", country);
						}
					}
				}
			}
		}
		finalJson.put("id", "p00");
		finalJson.put("totalPercentage", 100);
		finalJson.put("indirectPercentage", 100);
		finalJson.put("identifier", identifier);
		finalJson.put("sources", new JSONArray());
		finalJson.put("entity_id", "orgChartmainEntity");
		finalJson.put("entity_type", "organization");
		if (sourceData != null)
			finalJson.put("basic", sourceData);
		finalJson.put("jurisdiction", juridiction);
		finalJson.put("parents", new JSONArray());
		finalJson.put("status", "inprogress");
		array.put(finalJson);
		writeFile(file, array);
		List<JSONObject> lewiseData = new ArrayList<>();
		lewiseData.add(finalJson);
		refList.add(lewiseData);
		/*if (isScreeningRequired) {
			executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 0, true,
					new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes, startDate,
					endDate, BE_ENTITY_API_KEY));
		} else {
			array.getJSONObject(0).put("screeningFlag", true);
		}*/
		/*if (isSubsidiariesRequired) {
			executor.submit(new MultiSourceSubsidiaries(identifier, array, noOfSubsidiaries, maxSubsidiarielevel, 0,
					finalJson.getString("name"), juridiction, null, new AdvanceSearchServiceImpl(),
					BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes, startDate, endDate,
					CLIENT_ID, BE_ENTITY_API_KEY));
		} else {
			array.getJSONObject(0).put("subsidiaries", new JSONArray());
		}*/
		/*executor.submit(new OfficershipScreening(array, identifier, juridiction, SCREENING_URL, BIGDATA_MULTISOURCE_URL,
				new AdvanceSearchServiceImpl(), country, file, personTypes, pepTypes, startDate, endDate,
				isScreeningRequired, BE_ENTITY_API_KEY, CLIENT_ID));*/
		if (array != null) {
			for (int j = 0; j < array.length(); j++) {
				while ((!array.getJSONObject(j).has("subsidiaries"))
						|| (array.getJSONObject(j).has("subsidiaries")
								&& array.getJSONObject(j).getJSONArray("subsidiaries").length() > 0
								&& array.getJSONObject(j).getJSONArray("subsidiaries")
										.getJSONObject(array.getJSONObject(j).getJSONArray("subsidiaries").length() - 1)
										.getString("status").equals("pending"))
						/*|| (!array.getJSONObject(0).has("officership"))*/
						|| (array.getJSONObject(0).has("officership")
								&& array.getJSONObject(0).getJSONArray("officership").length() > 0
								&& array.getJSONObject(0).getJSONArray("officership")
										.getJSONObject(array.getJSONObject(0).getJSONArray("officership").length() - 1)
										.getString("status").equals("pending"))
						|| (!array.getJSONObject(j).has("screeningFlag"))) {
					array.getJSONObject(array.length() - 1).put("status", "inprogress");
					Thread.sleep(10000);
					writeFile(file, array);
				}
			}
		}
		array.getJSONObject(array.length() - 1).put("status", "completed");
		writeFile(file, array);
	}

	@SuppressWarnings("unused")
	private void getConflicts(JSONArray array) {
		Map<Integer,Double> map=new HashMap<Integer,Double>();
		for (int i = 0; i < array.length(); i++) {
			if(map.containsKey(array.getJSONObject(i).getInt("level"))){
				double value=0;
				value=map.get(array.getJSONObject(i).getInt("level"))+array.getJSONObject(i).getDouble("totalPercentage");
				map.put(array.getJSONObject(i).getInt("level"), value);
			}else{
				map.put(array.getJSONObject(i).getInt("level"), array.getJSONObject(i).getDouble("totalPercentage"));
			}
		}
		for (int i = 0; i < array.length(); i++) {
			double percentage=map.get(array.getJSONObject(i).getInt("level"));
			if(percentage>100)
				array.getJSONObject(i).put("isConflict",true);
			else
				array.getJSONObject(i).put("isConflict",false);
		}
	}

	@SuppressWarnings("unused")
	private void getDataFromOtherSources(String identifier, String requestOrganisation, String juridiction,
			Integer lowRange, Integer highRange, Integer noOfSubsidiaries, Integer maxSubsidiarielevel, File file,
			String annualReportLink, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,
			ThreadPoolExecutor executor, Long userId, Boolean isSubsidiariesRequired, String startDate,
			String endDate, String otherSource,boolean isScreeningRequired) throws Exception {
		JSONArray finalArray = new JSONArray();
		List<List<JSONObject>> refList = new ArrayList<List<JSONObject>>();
		JSONObject finalJson = new JSONObject();
		JSONObject sourceData = null;
		String overviewData = null;
		JSONObject result = null;
		JSONObject overviewDataJson = new JSONObject();
		try {
			String url = buildMultiSourceUrl(identifier, "overview", null, null, null,null);
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(url);
			overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
			if (overviewData != null)
				overviewDataJson = new JSONObject(overviewData);
			if (overviewDataJson.has("is-completed")) {
				int counter = 0;
				while (overviewDataJson.has("is-completed") && !overviewDataJson.has("is-completed")) {
					overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
					if (overviewData != null)
						overviewDataJson = new JSONObject(overviewData);
					counter = counter + 1;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
		}
		String country = null;
		if (!overviewDataJson.has("results") || overviewDataJson.getJSONArray("results").length() == 0) {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
			Object fileJsonData = parser.parse(new FileReader(fileJSon));
			org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
			for (int k = 0; k < countryJson.size(); k++) {
				org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
				if (countryObj.get("countryCode").toString().equalsIgnoreCase(juridiction)) {
					country = countryObj.get("country").toString();
				}
			}
			finalJson.put("country", country);
			finalJson.put("name", requestOrganisation);
			finalJson.put("title", requestOrganisation);

		}
		if (overviewDataJson.has("results")) {
			JSONArray results = overviewDataJson.getJSONArray("results");
			if (results.length() > 0) {
				result = results.getJSONObject(0);
				if (result.has("overview")) {
					JSONArray addressArray = null;
					JSONObject address = null;
					JSONObject overview = result.getJSONObject("overview");
					JSONArray sources = overview.names();
					if (sources != null && sources.length() > 0) {
						sourceData = overview.getJSONObject(sources.getString(0));
						if (sourceData.has("vcard:organization-name")) {
							finalJson.put("name", sourceData.getString("vcard:organization-name"));
							finalJson.put("title", sourceData.getString("vcard:organization-name"));
						}
						if (sourceData.has("mdaas:RegisteredAddress")) {
							if (sourceData.get("mdaas:RegisteredAddress") instanceof JSONObject)
								address = sourceData.getJSONObject("mdaas:RegisteredAddress");
							else
								addressArray = sourceData.getJSONArray("mdaas:RegisteredAddress");
							if (addressArray != null && addressArray.length() > 0) {
								address = addressArray.getJSONObject(0);
							}
							if (address != null && address.has("country")) {
								country = address.getString("country");
							}
							finalJson.put("country", country);
						}
					}
				}
			}
		}
		finalJson.put("id", "p00");
		finalJson.put("totalPercentage", 100);
		finalJson.put("indirectPercentage", 100);
		finalJson.put("identifier", identifier);
		finalJson.put("entity_id", "orgChartmainEntity");
		//finalJson.put("source_evidence", "Annual Report");
		finalJson.put("entity_type", "organization");
		if (sourceData != null)
			finalJson.put("basic", sourceData);
		finalJson.put("jurisdiction", juridiction);
		finalJson.put("parents", new JSONArray());
		finalJson.put("status", "inprogress");
		finalArray.put(finalJson);
		writeFile(file, finalArray);
		new ArrayList<>().add(finalJson);
		List<JSONObject> lewiseData = new ArrayList<>();
		lewiseData.add(finalJson);
		refList.add(lewiseData);
		/*if(isScreeningRequired){
			executor.submit(
					new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 0, true, new AdvanceSearchServiceImpl(),
							CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes, startDate, endDate,BE_ENTITY_API_KEY));
		}else{
			finalArray.getJSONObject(0).put("screeningFlag", true);
		}*/
		/*if (isSubsidiariesRequired) {
			executor.submit(new MultiSourceSubsidiaries(identifier, finalArray, noOfSubsidiaries, maxSubsidiarielevel,
					0, finalJson.getString("name"), juridiction, null, new AdvanceSearchServiceImpl(),
					BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes, startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
		} else {
			finalArray.getJSONObject(0).put("subsidiaries", new JSONArray());
		}*/
		/*executor.submit(
				new OfficershipScreening(finalArray, identifier, juridiction, SCREENING_URL, BIGDATA_MULTISOURCE_URL,
						new AdvanceSearchServiceImpl(), country, file, personTypes, pepTypes, startDate, endDate,isScreeningRequired,BE_ENTITY_API_KEY,CLIENT_ID));
*/
		String url=buildMultiSourceUrl(identifier, "shareholders", null, userId, null,null);
		String response=getDataInAsynMode(url);
		if (response != null) {
			List<JSONObject> levelData = new ArrayList<>();
			JSONObject json = new JSONObject(response);
			if (json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				if (results != null && results.length() > 0) {
					JSONObject shareHolderResult = results.getJSONObject(0);
					if (shareHolderResult.has("shareholders")) {
						JSONObject shareholders = shareHolderResult.getJSONObject("shareholders");
						JSONArray sources = shareholders.names();
						if (sources != null && sources.length() > 0) {
							JSONArray shareholderArray = new JSONArray();
							if (otherSource == null)
								shareholderArray = shareholders.getJSONArray(sources.getString(0));
							else
								shareholderArray = shareholders.getJSONArray(otherSource);
							if (shareholderArray.length() == 0)
								shareholderArray = shareholders.getJSONArray(sources.getString(0));
							int count = 0;
							for (int i = 0; i < shareholderArray.length(); i++) {
								String entity_type = null;
								String entityType = null;
								String organizationName = null;
								String shareHolderJurisdiction = null;
								JSONObject finalShareHolder = new JSONObject();
								JSONObject shareHolder = shareholderArray.getJSONObject(i);
								JSONObject basicJson = new JSONObject();
								if (shareHolder != null && shareHolder.has("basic")) {
									basicJson = shareHolder.getJSONObject("basic");
									if (basicJson.has("vcard:organization-name")) {
										organizationName = basicJson.getString("vcard:organization-name");
										finalShareHolder.put("name", organizationName);
										finalShareHolder.put("title", organizationName);
									}
									if (basicJson.has("isDomiciledIn")) {
										shareHolderJurisdiction = basicJson.getString("isDomiciledIn");
										finalShareHolder.put("jurisdiction", shareHolderJurisdiction);
									} else {
										finalShareHolder.put("jurisdiction", juridiction);
									}
									finalShareHolder.put("basic", basicJson);
								}
								Double totalPercentage = 0.0;
								Double indirectPercentage = 0.0;
								JSONObject relation = new JSONObject();
								if (shareHolder != null && shareHolder.has("relation")) {
									relation = shareHolder.getJSONObject("relation");
								} else if (basicJson != null && basicJson.has("relation")) {
									relation = basicJson.getJSONObject("relation");
								}
								if (relation.has("totalPercentage")) {
									String percentageInString = null;
									if (relation.get("totalPercentage") instanceof Double) {
										totalPercentage = relation.getDouble("totalPercentage");
									} else {
										percentageInString = relation.getString("totalPercentage");
										if (percentageInString != null) {
											String[] percentageArray = percentageInString.split(",");
											if (percentageArray != null && percentageArray.length > 0) {
												String percentage = percentageArray[0].trim();
												if (percentage.matches("[0-9]+")) {
													totalPercentage = Double.valueOf(percentage);
												}

											}
										}
									}
									indirectPercentage = totalPercentage;
								}
								// }
								if (indirectPercentage > lowRange && indirectPercentage <= highRange) {
									count = count + 1;

									if (shareHolder != null && shareHolder.has("entity_type")) {
										entity_type = shareHolder.getString("entity_type");
										if (entity_type != null && personTypes.contains(entity_type))
											entityType = "person";
										else
											entityType = "organization";
									}
									finalShareHolder.put("entity_type", entityType);
									String shareHolderIdentifier = null;
									if (shareHolder.has("@sourceReferenceID")) {
										shareHolderIdentifier = shareHolder.getString("@sourceReferenceID");
									} else {
										shareHolderIdentifier = "";
									}
									finalShareHolder.put("identifier", shareHolderIdentifier);
									finalShareHolder.put("totalPercentage", totalPercentage);
									finalShareHolder.put("indirectPercentage", totalPercentage);
									finalShareHolder.put("source_evidence_url", url);
									finalShareHolder.put("source_evidence", sources.getString(0));
									finalShareHolder.put("id", "p1" + count);
									finalShareHolder.put("level", 1);
									finalShareHolder.put("entity_id", "orgChartParentEntity");
									finalShareHolder.put("parents", new JSONArray());
									finalShareHolder.put("entity_id", "orgChartParentEntity");
									finalArray.put(finalShareHolder);
									levelData.add(finalShareHolder);
									finalArray.getJSONObject(0).getJSONArray("parents").put("p1" + count);
									/*if (isSubsidiariesRequired) {
										executor.submit(new MultiSourceSubsidiaries(identifier, finalArray,
												noOfSubsidiaries, maxSubsidiarielevel, count, organizationName,
												shareHolderJurisdiction, null, new AdvanceSearchServiceImpl(),
												BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes,
												startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
									} else {
										finalArray.getJSONObject(count).put("subsidiaries", new JSONArray());
									}*/
								}
							}
							/*refList.add(levelData);
							if (levelData.size() > 0 && refList.size() == 2 && isScreeningRequired) {
								executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 1, true,
										new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes,
										pepTypes, startDate, endDate,BE_ENTITY_API_KEY));
							}else{
								if (refList.size() == 2) {
									for (JSONObject jsonObject : refList.get(1)) {
										jsonObject.put("screeningFlag", true);
									}
								}
							}*/
						}
					}
				}
			}
		}
		for (int j = 0; j < finalArray.length(); j++) {
			while ((!finalArray.getJSONObject(j).has("subsidiaries"))
					|| (finalArray.getJSONObject(j).has("subsidiaries")
							&& finalArray.getJSONObject(j).getJSONArray("subsidiaries").length() > 0
							&& finalArray.getJSONObject(j).getJSONArray("subsidiaries")
							.getJSONObject(
									finalArray.getJSONObject(j).getJSONArray("subsidiaries").length() - 1)
							.getString("status").equals("pending"))
					|| (!finalArray.getJSONObject(0).has("officership"))
					|| (finalArray.getJSONObject(0).has("officership")
							&& finalArray.getJSONObject(0).getJSONArray("officership").length() > 0
							&& finalArray.getJSONObject(0).getJSONArray("officership")
							.getJSONObject(
									finalArray.getJSONObject(0).getJSONArray("officership").length() - 1)
							.getString("status").equals("pending"))
					|| (!finalArray.getJSONObject(j).has("screeningFlag"))) {
				finalArray.getJSONObject(finalArray.length() - 1).put("status", "inprogress");
				Thread.sleep(10000);
				writeFile(file, finalArray);
			}
		}
		finalArray.getJSONObject(finalArray.length() - 1).put("status", "completed");
		writeFile(file, finalArray);
	}

	private void checkChild(JSONObject map, JSONObject prevJsonObject, JSONObject mapJson, Integer level, int j,
			List<List<JSONObject>> allShareholders) {
		if (map.has(prevJsonObject.getString("id"))) {
			mapJson.put("child", prevJsonObject.getString("id"));
		} else {
			String child = prevJsonObject.getString("child");
			for (List<JSONObject> list : allShareholders) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject json = list.get(i);
					if (json.getString("id").equalsIgnoreCase(child))
						checkChild(map, json, mapJson, level - 1, j, allShareholders);
				}
			}
		}

	}

	private void getFactualControl(JSONArray array, JSONObject persons) {
		for (int i = 0; i < array.length(); i++) {
			if (persons.has(array.getJSONObject(i).getString("identifier"))) {
				boolean isUbo = false;
				double indirectPercentage = array.getJSONObject(i).getDouble("indirectPercentage");
				if (indirectPercentage < 25 && array.getJSONObject(i).getDouble("totalPercentage") > 50) {
					String child = array.getJSONObject(i).getString("child");
					for (int j = 0; j < array.length(); j++) {
						if (array.getJSONObject(j).getString("id").equalsIgnoreCase(child)) {
							int presentLevel = array.getJSONObject(j).getInt("level");
							if (presentLevel != 0) {
								double totalPercentage = array.getJSONObject(j).getDouble("totalPercentage");
								child = array.getJSONObject(j).getString("child");
								while (totalPercentage >= 50 && presentLevel > 0) {
									for (int k = 0; k < array.length(); k++) {
										if (array.getJSONObject(k).getString("id").equalsIgnoreCase(child)) {
											totalPercentage = array.getJSONObject(k).getDouble("totalPercentage");
											child = array.getJSONObject(k).getString("child");
											presentLevel = array.getJSONObject(k).getInt("level");
											if (presentLevel == 0)
												isUbo = true;
										}
									}
								}
							} else {
								isUbo = true;
							}
						}
					}
				} else if (indirectPercentage >= 25) {
					isUbo = true;
				}
				array.getJSONObject(i).put("isUbo", isUbo);
			}
		}

	}

	/*@Override
	public boolean deleteFiles(String path) throws IOException {
		boolean flag = true;
		if (path == null) {
			FileUtils.cleanDirectory(new File(CORPORATE_STRUCTURE_LOCATION));
		} else {
			File file = new File(CORPORATE_STRUCTURE_LOCATION + path + ".json");
			flag = file.delete();
		}
		return flag;
	}*/

	
	/*@Override
	public String getScreeningStatus() throws Exception {
		JSONObject json=new JSONObject();
		String url = BIG_DATA_S3_DOC_URL;
		try{
			url = url + "/sources?";
			url=url+"&client_id="+CLIENT_ID;

			String serverResponse[]=ServiceCallHelper.getDataFromServer(url);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				String response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					JSONObject jsonResponse = new JSONObject(response);
					if (jsonResponse != null && (((jsonResponse.has("screening") &&
							!jsonResponse.getString("screening").equalsIgnoreCase("none"))
							||jsonResponse.has("message")))) {
						json.put("isScreeningEnabled", true);//If Sources configurations not found or screening is not none then enabled
					}else {
						json.put("isScreeningEnabled", false);
					}
				}else {
					json.put("isScreeningEnabled", false);
				}
			}else {
				json.put("isScreeningEnabled", false);
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}
	*/
	/*@Override
	public String getAnnualReturnsLink(String documentsLink) throws Exception {
		String finalUrl = null;
		List<JSONObject> mergedData = new ArrayList<JSONObject>();
		if (documentsLink != null) {
			String documentsString = getDataInAsynMode(documentsLink);
			if (documentsString != null) {
				JSONObject documentsJson = new JSONObject(documentsString);
				if (documentsJson.has("results")) {
					JSONArray results = documentsJson.getJSONArray("results");
					if (results.length() > 0) {
						JSONObject resultsJson = results.getJSONObject(0);
						if (resultsJson.has("documents")) {
							JSONObject documents = resultsJson.getJSONObject("documents");
							JSONArray sources = documents.names();
							if (sources != null && sources.length() > 0) {
								for (int i = 0; i < sources.length(); i++) {
									JSONArray documentsArray = new JSONArray();
									if (documents.get(sources.getString(i)) instanceof JSONArray)
										documentsArray = documents.getJSONArray(sources.getString(i));
									for (int j = 0; j < documentsArray.length(); j++) {
										JSONObject document =new JSONObject();
										if(documentsArray.get(j) instanceof JSONObject)
											document = documentsArray.getJSONObject(j);
										if (document.has("description")) {
											String description = document.getString("description");
											if (description.startsWith("Annual return"))
												mergedData.add(documentsArray.getJSONObject(j));
										}
									}
								}
							}
							Collections.sort(mergedData, new DateComparator());
						}
					}
				}
			}
		}
		JSONObject finalJson = null;
		if (mergedData.size() > 0)
			finalJson = mergedData.get(0);
		if (finalJson != null) {
			if (finalJson.has("url"))
				finalUrl = finalJson.getString("url");
			if (finalJson.has("Primary_source_url"))
				finalUrl = finalJson.getString("Primary_source_url");
		}
		return finalUrl;
	}*/

	public void whenShareholderDataEmpty(String identifier, String requestOrganisation, String juridiction,
			Integer lowRange, Integer highRange, Integer noOfSubsidiaries, Integer maxSubsidiarielevel, File file,
			String annualReportLink, org.json.simple.JSONArray personTypes, org.json.simple.JSONObject pepTypes,
			ThreadPoolExecutor executor, Long userId, Boolean isSubsidiariesRequired, String startDate, String endDate, JSONObject sourceInfo, String bstUrl, String path, boolean isScreeningRequired, boolean isNoData, String source, JSONArray array, JSONArray sources2, String errorMsg)
					throws Exception {
		boolean isRollback=false;
		//JSONArray finalArray = new JSONArray();
		List<List<JSONObject>> refList = new ArrayList<List<JSONObject>>();
		JSONObject finalJson = new JSONObject();
		JSONObject sourceData = null;
		String overviewData = null;
		JSONObject result = null;
		JSONObject overviewDataJson = new JSONObject();
		try {
			String url = buildMultiSourceUrl(identifier, "overview", null, null, null,null);
			HierarchyDto hierarchyDto = new HierarchyDto();
			hierarchyDto.setUrl(url);
			overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
			if (overviewData != null)
				overviewDataJson = new JSONObject(overviewData);
			if (overviewDataJson.has("is-completed")) {
				int counter = 0;
				while (overviewDataJson.has("is-completed") && !overviewDataJson.has("is-completed")) {
					overviewData = getHierarchyData(hierarchyDto, null,BE_ENTITY_API_KEY);
					if (overviewData != null)
						overviewDataJson = new JSONObject(overviewData);
					counter = counter + 1;
					if (counter == 6)
						break;
					Thread.sleep(5000);
				}
			}
		} catch (Exception e) {
		}
		String country = null;
		if (!overviewDataJson.has("results") || overviewDataJson.getJSONArray("results").length() == 0) {
			JSONParser parser = new JSONParser();
			ClassLoader classLoader = getClass().getClassLoader();
			File fileJSon = new File(classLoader.getResource("BVD_countries.json").getFile());
			Object fileJsonData = parser.parse(new FileReader(fileJSon));
			org.json.simple.JSONArray countryJson = (org.json.simple.JSONArray) fileJsonData;
			for (int k = 0; k < countryJson.size(); k++) {
				org.json.simple.JSONObject countryObj = (org.json.simple.JSONObject) countryJson.get(k);
				if (countryObj.get("countryCode").toString().equalsIgnoreCase(juridiction)) {
					country = countryObj.get("country").toString();
				}
			}
			finalJson.put("country", country);
			finalJson.put("name", requestOrganisation);
			finalJson.put("title", requestOrganisation);

		}
		if (overviewDataJson.has("results")) {
			JSONArray results = overviewDataJson.getJSONArray("results");
			if (results.length() > 0) {
				result = results.getJSONObject(0);
				if (result.has("overview")) {
					JSONArray addressArray = null;
					JSONObject address = null;
					JSONObject overview = result.getJSONObject("overview");
					JSONArray sources = overview.names();
					if (sources != null && sources.length() > 0) {
						sourceData = overview.getJSONObject(sources.getString(0));
						if (sourceData.has("vcard:organization-name")) {
							finalJson.put("name", sourceData.getString("vcard:organization-name"));
							finalJson.put("title", sourceData.getString("vcard:organization-name"));
						}
						if (sourceData.has("mdaas:RegisteredAddress")) {
							if (sourceData.get("mdaas:RegisteredAddress") instanceof JSONObject)
								address = sourceData.getJSONObject("mdaas:RegisteredAddress");
							else
								addressArray = sourceData.getJSONArray("mdaas:RegisteredAddress");
							if (addressArray != null && addressArray.length() > 0) {
								address = addressArray.getJSONObject(0);
							}
							if (address != null && address.has("country")) {
								country = address.getString("country");
							}
							finalJson.put("country", country);
						}
					}
				}
			}
		}
		finalJson.put("id", "p00");
		finalJson.put("totalPercentage", 100);
		finalJson.put("indirectPercentage", 100);
		finalJson.put("identifier", identifier);
		finalJson.put("sources", sources2);
		finalJson.put("entity_id", "orgChartmainEntity");
		if(errorMsg!=null) {
			finalJson.put("is-error", true);
			finalJson.put("error", errorMsg);
		}
		else {
			finalJson.put("is-error", false);
		}
		if(source!=null && source.equalsIgnoreCase("companyhouse.co.uk")) {
			finalJson.put("source_evidence", "Annual Report");
			//sources2.put("companyhouse.co.uk");
			//finalJson.put("source_evidence", "companyhouse.co.uk");
		}
		else if(source!=null )
			finalJson.put("source_evidence", source);
		finalJson.put("entity_type", "organization");
		if (sourceData != null)
			finalJson.put("basic", sourceData);
		finalJson.put("jurisdiction", juridiction);
		finalJson.put("parents", new JSONArray());
		finalJson.put("status", "inprogress");
		array.put(finalJson);
		writeFile(file, array);
		new ArrayList<>().add(finalJson);
		List<JSONObject> lewiseData = new ArrayList<>();
		lewiseData.add(finalJson);
		refList.add(lewiseData);
		/*if(isScreeningRequired){
			executor.submit(
					new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, 0, true, new AdvanceSearchServiceImpl(),
							CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes, startDate, endDate,BE_ENTITY_API_KEY));
		}else{
			array.getJSONObject(0).put("screeningFlag", true);
		}*/
		/*if (isSubsidiariesRequired) {
			executor.submit(new MultiSourceSubsidiaries(identifier, array, noOfSubsidiaries, maxSubsidiarielevel,
					0, finalJson.getString("name"), juridiction, null, new AdvanceSearchServiceImpl(),
					BIGDATA_MULTISOURCE_URL, SCREENING_URL, executor, personTypes, pepTypes, startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
		} else {
			array.getJSONObject(0).put("subsidiaries", new JSONArray());
		}*/
		/*String 
String highCredibilitySource=getKeyManagementSource(keys);
		List<EntityOfficerInfoDto> listOfOfficer= entityOfficerInfoService.getEntityOfficerList(identifier);
		executor.submit(
				new OfficershipScreening(array, identifier, juridiction, SCREENING_URL, BIGDATA_MULTISOURCE_URL,
						new AdvanceSearchServiceImpl(), country, file, personTypes, pepTypes, startDate, endDate,isScreeningRequired,BE_ENTITY_API_KEY,CLIENT_ID,highCredibilitySource,listOfOfficer));
		*/int tempCount = 1;
		boolean flag = true;
		while ((flag && array.length() > 0) || (refList.get(refList.size() - 1).size() > 0 && !isRollback)) {
			List<JSONObject> levelwiseData = new ArrayList<>();
			List<JSONObject> previousShareholders = refList.get(refList.size() - 1);
			int count = 0;
			for (int j = 0; j < previousShareholders.size(); j++) {
				JSONObject previousShareholder = previousShareholders.get(j);
				String previousShareHolderName =null;
				if(previousShareholder.has("title"))
				 previousShareHolderName = previousShareholder.getString("title");
				String tempMultiSourceData = null;
				String presentDocumentLink = null;
				String selectEntity = null;
				if (array.length() == 1) {
					presentDocumentLink = annualReportLink;
				} else {
					try {
						if(previousShareHolderName!=null)
						tempMultiSourceData = getMultisourceData(previousShareHolderName, null, null, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (tempMultiSourceData != null) {
						JSONObject tempMultiSourceDataJson = new JSONObject(tempMultiSourceData);
						if (tempMultiSourceDataJson.has("results")) {
							JSONArray results = tempMultiSourceDataJson.getJSONArray("results");
							if (results.length() > 0) {
								JSONObject resultsJson = results.getJSONObject(0);
								if (resultsJson.has("links")) {
									JSONObject multiSourceLinks = resultsJson.getJSONObject("links");
									if (multiSourceLinks.has("documents")) {
										presentDocumentLink = multiSourceLinks.getString("documents");
									} else if (multiSourceLinks.has("select-entity")) {
										selectEntity = multiSourceLinks.getString("select-entity");
										String selectEntityResponse = getDataInAsynMode(selectEntity);
										if (selectEntityResponse != null) {
											JSONObject selectEntityJson = new JSONObject(selectEntityResponse);
											if (selectEntityJson.has("results")) {
												JSONArray entityArray = selectEntityJson.getJSONArray("results");
												if (entityArray.length() > 0) {
													JSONObject entityJson = entityArray.getJSONObject(0);
													if (entityJson.has("links")) {
														JSONObject entityLinks = entityJson.getJSONObject("links");
														if (entityLinks.has("documents")) {
															presentDocumentLink = entityLinks.getString("documents");
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if (presentDocumentLink != null) {
					String annualreturnsLink = getAnnualReturnsLink(presentDocumentLink);
					if (annualreturnsLink != null) {
						AnnualReturnResponseDto annualReturnResponseDto = entitySearchService
								.getAnnualReports(annualreturnsLink, identifier, requestOrganisation, userId);
						previousShareholder.put("source_evidence_url", annualreturnsLink);
						if (array.length() > 0 && array.length() == 1)
							array.getJSONObject(0).put("numberOfShares",
									annualReturnResponseDto.getTotalNumberOfShares());
						List<ShareholdingDto> shareholdingDto = annualReturnResponseDto.getShareholdingDto();
						if (shareholdingDto != null && shareholdingDto.size()>0) {
							JSONArray parents = new JSONArray();
							for (ShareholdingDto tempShareholdingDto : shareholdingDto) {
								JSONObject tempJson = new JSONObject();
								tempJson.put("id", "p" + refList.size() + count);
								tempJson.put("level", refList.size());
								tempJson.put("identifier", tempShareholdingDto.getIdentifier());
								tempJson.put("name", tempShareholdingDto.getShareHolderName());
								tempJson.put("title", tempShareholdingDto.getShareHolderName());
								tempJson.put("entity_id", "orgChartParentEntity");
								tempJson.put("totalPercentage", tempShareholdingDto.getTotalShareValuePer());
								tempJson.put("parents", new JSONArray());
								tempJson.put("jurisdiction", juridiction);
								double indirectPercentage = (previousShareholder.getDouble("indirectPercentage")
										* tempShareholdingDto.getTotalShareValuePer() / 100);
								if (indirectPercentage > lowRange && indirectPercentage <= highRange) {
									parents.put(tempJson.getString("id"));
									tempJson.put("source_evidence", "Annual Report");
									tempJson.put("entity_type", "organization");
									tempJson.put("numberOfShares", tempShareholdingDto.getNoOfShares());
									tempJson.put("indirectPercentage", indirectPercentage);
									tempJson.put("status", "inprogress");
									levelwiseData.add(tempJson);
									array.put(tempJson);
									previousShareholder.put("parents", parents);
									array.getJSONObject(array.length() - 1).put("status", "inprogress");
									writeFile(file, array);
									/*if (isSubsidiariesRequired) {
										executor.submit(new MultiSourceSubsidiaries(tempShareholdingDto.getIdentifier(),
												array, noOfSubsidiaries, maxSubsidiarielevel, tempCount,
												tempJson.getString("name"), juridiction, null,
												new AdvanceSearchServiceImpl(), BIGDATA_MULTISOURCE_URL, SCREENING_URL,
												executor, personTypes, pepTypes, startDate, endDate,CLIENT_ID,BE_ENTITY_API_KEY));
									} else {
										array.getJSONObject(tempCount).put("subsidiaries", new JSONArray());
									}*/
									tempCount = tempCount + 1;
									count = count + 1;
								}
							}
						} 
					}
				}
			}
			flag = false;
			refList.add(levelwiseData);
			/*if (levelwiseData.size() > 0 && isScreeningRequired) {
				executor.submit(new MultiSourceScreeningNew(identifier, SCREENING_URL, refList, refList.size() - 1,
						true, new AdvanceSearchServiceImpl(), CORPORATE_STRUCTURE_LOCATION, personTypes, pepTypes,
						startDate, endDate,BE_ENTITY_API_KEY));
			}else{
				for (JSONObject jsonObject : refList.get(refList.size()-1)) {
					jsonObject.put("screeningFlag", true);
				}
			}*/
		}
		if(array.length()==1 && !isNoData){
			int bstCredibility = 0;
			boolean isOtherSource = false;
			int otherSourceCredibility = 0;
			String otherSource = null;
			if (sourceInfo.has("BST") || sourceInfo.has("bst")) {
				bstCredibility = sourceInfo.getInt("BST");
				JSONArray sourcesArray = sourceInfo.names();
				if (sourcesArray != null) {
					for (int i = 0; i < sourcesArray.length(); i++) {
						if (!"BST".equalsIgnoreCase(sourcesArray.getString(i)) &&
								!"companyhouse.co.uk".equalsIgnoreCase(sourcesArray.getString(i))) {
							if (sourceInfo.getInt(sourcesArray.getString(i)) >= bstCredibility) {
								isOtherSource = true;
								otherSourceCredibility = sourceInfo.getInt(sourcesArray.getString(i));
								otherSource = sourcesArray.getString(i);
								break;
							}
						}
					}
				}
			}

			if ((bstUrl != null && !"".equalsIgnoreCase(bstUrl)) && !isOtherSource) {
				isRollback=true;
				HierarchyDto jsonString=new HierarchyDto();
				jsonString.setUrl(bstUrl);
				ownershipStructure(identifier, jsonString, maxSubsidiarielevel, lowRange, highRange, path, noOfSubsidiaries, maxSubsidiarielevel, requestOrganisation, juridiction, userId, isSubsidiariesRequired, startDate, endDate,true,null,null);
			}/*else{
					getDataFromOtherSources(identifier, requestOrganisation, juridiction, lowRange, highRange, noOfSubsidiaries, maxSubsidiarielevel, file, annualReportLink, personTypes, pepTypes, executor, userId, isSubsidiariesRequired, startDate, endDate,otherSource,isScreeningRequired);
					isRollback=true;
				}*/
		}
		if(!isRollback){
			if (refList.get(refList.size() - 1).size() == 0) {
				for (int j = 0; j < array.length(); j++) {
					/*while ((!array.getJSONObject(j).has("subsidiaries"))
							|| (array.getJSONObject(j).has("subsidiaries")
									&& array.getJSONObject(j).getJSONArray("subsidiaries").length() > 0
									&& array.getJSONObject(j).getJSONArray("subsidiaries")
									.getJSONObject(
											array.getJSONObject(j).getJSONArray("subsidiaries").length() - 1)
									.getString("status").equals("pending"))
							/*|| (!array.getJSONObject(0).has("officership"))
							|| (array.getJSONObject(0).has("officership")
									&& array.getJSONObject(0).getJSONArray("officership").length() > 0
									&& array.getJSONObject(0).getJSONArray("officership")
									.getJSONObject(
											array.getJSONObject(0).getJSONArray("officership").length() - 1)
									.getString("status").equals("pending"))
							|| (!array.getJSONObject(j).has("screeningFlag"))) {*/
						array.getJSONObject(array.length() - 1).put("status", "inprogress");
						Thread.sleep(10000);
						writeFile(file, array);
					//}
				}
				array.getJSONObject(array.length() - 1).put("status", "completed");

			} else {
				array.getJSONObject(array.length() - 1).put("status", "inprogress");
			}
			writeFile(file, array);
		}
	}

	@Override
	public String getMultisourceData(String query, String jurisdiction, String website, Long userId) throws Exception {
		String url = BIGDATA_MULTISOURCE_URL + "?name=" + URLEncoder.encode(query, "UTF-8").replaceAll("\\+", "%20");
		if (jurisdiction != null)
			url = url + "&jurisdiction=" + URLEncoder.encode(jurisdiction, "UTF-8").replaceAll("\\+", "%20");
		if (website != null)
			url = url + "&website=" + URLEncoder.encode(website, "UTF-8").replaceAll("\\+", "%20");
		url=url+"&client_id="+CLIENT_ID;
		JSONObject json = new JSONObject();
		String response = getDataInAsynMode(url);
		if (response != null) {
			json = new JSONObject(response);
		}
		if (userId == null) {
			return response;
		} else {
			if (json.has("results")) {
				JSONArray results = json.getJSONArray("results");
				for (int i = 0; i < results.length(); i++) {
					JSONObject result = results.getJSONObject(i);
					String identifier = null;
					if (result.has("identifier")) {
						identifier = result.getString("identifier");
					}
					if (result.has("overview")) {
						JSONObject overview = result.getJSONObject("overview");
						JSONArray sources = overview.names();
						for (int j = 0; j < sources.length(); j++) {
							JSONObject overviewJson = overview.getJSONObject(sources.getString(j));
							if (overviewJson.has("vcard:organization-name")) {
								String organizationName = overviewJson.getString("vcard:organization-name");
								/*AdvanceNewsFavourite advanceNewsFavourite = advanceNewsFavouriteDao
										.fetchadvanceNewsFavourite(identifier, organizationName, userId);
								if (advanceNewsFavourite != null) {
									overviewJson.accumulate("entitySiginificance", true);
								}*/
							}
							/*try{
								String category = sourcesService.getSourceCategoryBySourceName(sources.getString(j));
								if(category!=null){
									overviewJson.put("sourceCategory", category);
								}
							}catch (Exception e) {
								//e.printStackTrace();
							}*/
							
						}
					}
				}

			}
			return json.toString();
		}
	}

	
	JSONArray keys=new JSONArray();

	public String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level)
			throws UnsupportedEncodingException {

		String url = BIGDATA_MULTISOURCE_URL;
		String finalFields = null;
		if ("finance_info".equals(fields)) {
			finalFields = "overview";
		} else {
			finalFields = fields;
		}
		if (identifier != null)
			url = url + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		;
		if (graph != null)
			url = url + "/" + graph;
		if (level != null)
			url = url + "/" + level;
		if (finalFields != null)
			url = url + "?fields=" + finalFields;
		if (clientId != null)
			url = url + "&client_id=" + clientId;
		return url;

	}
	

	@Override
	public String buildMultiSourceUrl(String identifier, String fields, String graph, Long userId, Integer level,String clientId)
			throws UnsupportedEncodingException {

		String url = BIGDATA_MULTISOURCE_URL;
		String finalFields = null;
		if ("finance_info".equals(fields)) {
			finalFields = "overview";
		} else {
			finalFields = fields;
		}
		if (identifier != null)
			url = url + URLEncoder.encode(identifier, "UTF-8").replaceAll("\\+", "%20");
		if (graph != null)
			url = url + "/" + graph;
		if (level != null)
			url = url + "/" + level;
		if (finalFields != null)
			url = url + "?fields=" + finalFields;
		if (clientId != null)
			url = url + "&client_id=" + clientId;
		return url;

	}

	



	public static synchronized void writeFile(File file, JSONArray array) throws IOException {
		FileWriter fileWriter = new FileWriter(file);
		fileWriter.write(array.toString());
		fileWriter.close();
	}

	public void checkParent(JSONObject map, JSONObject prevJsonObject, JSONObject mapJson, Integer level, int j,
			List<List<JSONObject>> allShareholders, JSONObject persons, JSONObject tempJson, JSONObject holders, double totalPercentage, double indirectPercentage) {
		if (persons.has(mapJson.getString("identifier"))) {
			JSONObject person = persons.getJSONObject(mapJson.getString("identifier"));
			JSONArray indirectChilds = person.getJSONArray("indirectChilds");
			JSONArray parents = new JSONArray();
			if (map.has(mapJson.getString("id"))) {
				JSONObject prevPerson = map.getJSONObject(mapJson.getString("id"));
				JSONObject childObj = map.getJSONObject(prevPerson.getString("child"));
				JSONArray childParents = childObj.getJSONArray("parents");
				for (int i = 0; i < childParents.length(); i++) {
					if (!mapJson.getString("id").equalsIgnoreCase(childParents.getString(i))) {
						parents.put(childParents.getString(i));
					}
				}
				map.getJSONObject(prevPerson.getString("child")).put("parents", parents);
				/*
				 * boolean isIndirect = false; for (int i = 0; i <
				 * indirectChilds.length(); i++) {
				 * if(indirectChilds.getJSONObject(i).getString("id").
				 * equalsIgnoreCase(childObj.getString("id"))){ isIndirect =
				 * true; break; } if
				 * (childObj.getString("id").equalsIgnoreCase(indirectChilds.
				 * getString(i))) { isIndirect = true; break; } }
				 */
				/*
				 * if (!isIndirect){ JSONObject indirectObject=new JSONObject();
				 * indirectObject.put("id", childObj.getString("id"));
				 * indirectObject.put("totalPercentage",
				 * mapJson.getDouble("previousTotalPercentage"));
				 * indirectObject.put("indirectPercentage",
				 * mapJson.getDouble("previousIndirectPercentage"));
				 * indirectObject.put("level", childObj.getInt("level"));
				 * indirectChilds.put(indirectObject);
				 * //indirectChilds.put(childObj.getString("id")); }
				 */
			}
			// person.put("indirectChilds", indirectChilds);
			// mapJson.put("indirectChilds", indirectChilds);
		}
		if (map.has(prevJsonObject.getString("id"))) {
			boolean isParent = false;
			if (persons.has(mapJson.getString("identifier"))) {
				JSONObject person = persons.getJSONObject(mapJson.getString("identifier"));
				JSONArray indirectChilds = person.getJSONArray("indirectChilds");
				// JSONArray newIndirectChilds=new JSONArray();
				for (int i = 0; i < indirectChilds.length(); i++) {
					if (map.has(indirectChilds.getJSONObject(i).getString("id"))) {
						if (prevJsonObject.getInt("level") < (indirectChilds.getJSONObject(i).getInt("level"))) {
							map.getJSONObject(indirectChilds.getJSONObject(i).getString("id")).getJSONArray("parents")
							.put(mapJson.getString("id"));
							mapJson.put("child", indirectChilds.getJSONObject(i).getString("id"));
							isParent = true;
							for (int k = 0; k < indirectChilds.length(); k++) {
								if (indirectChilds.getJSONObject(k).getString("id")
										.equalsIgnoreCase(indirectChilds.getJSONObject(i).getString("id")))
									indirectChilds.getJSONObject(k).put("isDirect", true);
								else
									indirectChilds.getJSONObject(k).put("isDirect", false);
								/// newIndirectChilds.put(indirectChilds.getJSONObject(k));
							}
							// mapJson.put("indirectChilds", newIndirectChilds);
							break;
						}
					}
				}
				if (!isParent) {
					if(holders!=null){
						if (holders.has(prevJsonObject.getString("identifier"))) {
							holders.getJSONObject(prevJsonObject.getString("identifier")).getJSONArray("parents")
							.put(mapJson.getString("id"));
						}
					}else{
						prevJsonObject.getJSONArray("parents").put(mapJson.getString("id"));
					}
					if(map.has(mapJson.getString("id"))){
						map.getJSONObject(mapJson.getString("id")).put("level", prevJsonObject.getInt("level")+1);
						map.getJSONObject(mapJson.getString("id")).put("child",prevJsonObject.getString("id"));
						map.getJSONObject(mapJson.getString("id")).put("childIdentifier",prevJsonObject.getString("bvdId"));
						map.getJSONObject(mapJson.getString("id")).put("childLevel",prevJsonObject.getInt("level"));

					}
					mapJson.put("child", prevJsonObject.getString("id"));
					mapJson.put("childIdentifier", prevJsonObject.getString("bvdId"));
					for (int k = 0; k < indirectChilds.length(); k++) {
						if (indirectChilds.getJSONObject(k).getString("id")
								.equalsIgnoreCase(prevJsonObject.getString("id")))
							indirectChilds.getJSONObject(k).put("isDirect", true);
						else
							indirectChilds.getJSONObject(k).put("isDirect", false);
						/// newIndirectChilds.put(indirectChilds.getJSONObject(k));
					}
				}
			} else {
				JSONObject indirectChild=new JSONObject();
				indirectChild.put("id", prevJsonObject.getString("id"));
				//indirectChild.put("totalPercentage", mapJson.getDouble("totalPercentage"));
				//indirectChild.put("indirectPercentage", mapJson.getDouble("indirectPercentage"));
				indirectChild.put("totalPercentage",totalPercentage);
				indirectChild.put("indirectPercentage",indirectPercentage);
				if(tempJson.has("indirectChilds")){
					tempJson.getJSONArray("indirectChilds").put(indirectChild);
					tempJson.put("child", prevJsonObject.getString("id"));
					tempJson.put("childIdentifier", prevJsonObject.getString("bvdId"));
					tempJson.put("childLevel", prevJsonObject.getInt("level"));
					tempJson.put("level", prevJsonObject.getInt("level")+1);
				}else if(mapJson.has("indirectChilds")){
					mapJson.getJSONArray("indirectChilds").put(indirectChild);
				}else{
					mapJson.put("indirectChilds",new JSONArray().put(indirectChild));
				}
				if(holders!=null){
					if (holders.has(prevJsonObject.getString("identifier"))) {
						holders.getJSONObject(prevJsonObject.getString("identifier")).getJSONArray("parents")
						.put(mapJson.getString("id"));
					}
				}else{
					prevJsonObject.getJSONArray("parents").put(mapJson.getString("id"));
				}
				//prevJsonObject.getJSONArray("parents").put(mapJson.getString("id"));
				mapJson.put("child", prevJsonObject.getString("id"));
				mapJson.put("childIdentifier", prevJsonObject.getString("bvdId"));
			}
		} else {
			String child = prevJsonObject.getString("child");
			for (List<JSONObject> list : allShareholders) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject json = list.get(i);
					if (json.getString("id").equalsIgnoreCase(child))
						checkParent(map, json, mapJson, level - 1, j, allShareholders, persons,tempJson,holders,json.getDouble("totalPercentage"),json.getDouble("indirectPercentage"));
				}
			}
		}
	}

	
	

	

	public String getDataInAsynMode(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if (json.has("is-completed")) {
					int counter = 0;
					while (json.has("is-completed") && !json.getBoolean("is-completed")) {
						serverResponse = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, BE_ENTITY_API_KEY);
						if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
							response = serverResponse[1];
							if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
								json = new JSONObject(response);
							}
						}
						counter++;
						if (counter == 6)
							break;
						Thread.sleep(5000);
					}
				}
			} else {
				return null;
			}
		} else {
			return response;
		}
		return response;
	}

	
	
	

	public void getParents(JSONObject presentJson, JSONObject clientJson, double indirectPercentage, JSONArray parentArray) {
		if (presentJson.has("shareholders")) {
			JSONObject shareholderobj = presentJson.getJSONObject("shareholders");
			JSONArray parentIds = shareholderobj.names();
			if(parentIds!=null && parentIds.length()>0) {
			for (int k = 0; k < parentIds.length(); k++) {
				JSONObject parentObj = new JSONObject();
				JSONObject parentJson = shareholderobj.getJSONObject(parentIds.getString(k));
				parentObj.put("parentId", parentIds.getString(k));
					if (parentJson.has("totalPercentage")) {
						if (parentJson.get("totalPercentage") instanceof Double)
							parentObj.put("totalPercentage", parentJson.getDouble("totalPercentage"));
						if (parentJson.get("totalPercentage") instanceof String
								&& !StringUtils.isEmpty(parentJson.getString(("totalPercentage"))))
							parentObj.put("totalPercentage",
									Double.parseDouble(parentJson.getString("totalPercentage")));
						if (parentJson.get("totalPercentage") instanceof Integer)
							parentObj.put("totalPercentage", parentJson.getInt("totalPercentage"));
					} else
						parentObj.put("totalPercentage", 0.0);
				////
				if(parentJson.has("classification"))
					parentObj.put("classification", parentJson.getString("classification"));
				if (parentJson.has("dateOfBirth"))
					parentObj.put("dateOfBirth", parentJson.getString("dateOfBirth"));
				if (parentJson.has("role"))
					parentObj.put("role", parentJson.getString("role"));
				////
				if (parentJson.has("from") && parentJson.get("from") instanceof String)
					parentObj.put("from", parentJson.getString("from"));
				if (parentJson.has("source") && parentJson.get("source") instanceof String)
					parentObj.put("source", parentJson.getString("source"));
				double nextIndirectPercentage=0.0;
					if (parentObj.has("totalPercentage")) {
						nextIndirectPercentage = (parentObj.getDouble("totalPercentage") * indirectPercentage) / 100;
					}
				parentObj.put("indircetPercentage", nextIndirectPercentage);
				//if (nextIndirectPercentage >= 10 && nextIndirectPercentage <= 100)
				parentArray.put(parentObj);
			}
		}
		}
		if (clientJson.has("shareholders")) {
			JSONObject shareholderobj = clientJson.getJSONObject("shareholders");
			JSONArray parentIds = shareholderobj.names();
			for (int k = 0; k < parentIds.length(); k++) {
				for (int i = 0; i < parentArray.length(); i++) {
					String id=parentArray.getJSONObject(i).getString("parentId");
					if(parentIds.getString(k).equalsIgnoreCase(id+"_"+CLIENT_ID) || parentIds.getString(k).equalsIgnoreCase(id)){
						parentArray.remove(i);
					}
				}
				JSONObject parentObj = new JSONObject();
				JSONObject parentJson = shareholderobj.getJSONObject(parentIds.getString(k));
				String parentId=null;
				if(parentIds.getString(k).contains(CLIENT_ID)){
					String [] str=parentIds.getString(k).split("_");
					parentId=str[0];
				}else{
					parentId=parentIds.getString(k);
				}
				parentObj.put("parentId", parentId);
				if(parentJson.has("basic")){
					parentObj.put("basic", parentJson.getJSONObject("basic"));
				}
				if (parentJson.has("totalPercentage"))
					parentObj.put("totalPercentage", parentJson.getDouble("totalPercentage"));
				else
					parentObj.put("totalPercentage", 0.0);
				if(parentJson.has("user")){
					parentObj.put("user", parentJson.getString("user"));
				}
				if (parentJson.has("from") && parentJson.get("from") instanceof String)
					parentObj.put("from", parentJson.getString("from"));
				if (parentJson.has("classification") && parentJson.get("classification") instanceof String)
					parentObj.put("classification", parentJson.getString("classification"));
				if (parentJson.has("role") && parentJson.get("role") instanceof String)
					parentObj.put("role", parentJson.getString("role"));
				if (parentJson.has("dateOfBirth") && parentJson.get("dateOfBirth") instanceof String)
					parentObj.put("dateOfBirth", parentJson.getString("dateOfBirth"));
				if (parentJson.has("source") && parentJson.get("source") instanceof String)
					parentObj.put("source", parentJson.getString("source"));
				parentObj.put("isCustom", true);
				double nextIndirectPercentage = (parentObj.getDouble("totalPercentage") * indirectPercentage) / 100;
				parentObj.put("indircetPercentage", nextIndirectPercentage);
				parentArray.put(parentObj);
			}
		}
	}


	
	public void getCustomShareholdersFromLatLevel(List<List<JSONObject>> list,JSONArray array, Integer lowRange, Integer highRange,JSONObject holders, JSONObject map){
		JSONParser parser = new JSONParser();
		ClassLoader classLoader = getClass().getClassLoader();
		File jsonfile = new File(classLoader.getResource("entityType.json").getFile());
		Object jsonfiledata = new Object();
		try {
			jsonfiledata = parser.parse(new FileReader(jsonfile));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		org.json.simple.JSONArray personTypes = (org.json.simple.JSONArray) jsonfiledata;
		for (JSONObject jsonObject :list.get(list.size()-1)) {
			
			if(jsonObject.has("parentIds")){
				for (int i = 0; i < jsonObject.getJSONArray("parentIds").length(); i++) {
					String entityType=null;
					JSONObject parentJson=jsonObject.getJSONArray("parentIds").getJSONObject(i);
					if(!holders.has(jsonObject.getJSONArray("parentIds")
							.getJSONObject(i).getString("parentId"))){
						if(jsonObject.getJSONArray("parentIds").getJSONObject(i).has("basic")){
							JSONObject jsonlast=new JSONObject();
							JSONObject basic=jsonObject.getJSONArray("parentIds").getJSONObject(i).getJSONObject("basic");
							jsonlast.put("totalPercentage", jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getDouble("totalPercentage"));
							jsonlast.put("indirectPercentage",
									(jsonObject.getDouble("indirectPercentage")
											* jsonlast.getDouble("totalPercentage"))/100);
							if ((jsonlast.getDouble("indirectPercentage") >= lowRange && jsonlast.getDouble("indirectPercentage") <= highRange)) {
								if(basic.has("vcard:organization-name")) {
									String organisationName = basic.getString("vcard:organization-name");
									jsonlast.put("name", organisationName);
									jsonlast.put("title", organisationName);
								}
								if (basic.has("isDomiciledIn")) {
									String jurisdiction = basic.getString("isDomiciledIn");
									jsonlast.put("jurisdiction", jurisdiction);
								}else{
									jsonlast.put("jurisdiction", jsonObject.getString("jurisdiction"));
								}
								jsonlast.put("level", jsonObject.getInt("level") + 1);
								jsonlast.put("entity_id", "orgChartParentEntity");
								
								jsonlast.put("bvdId", (jsonObject.getJSONArray("parentIds").getJSONObject(i)
										.getString("parentId")));
								jsonlast.put("identifier", (jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("parentId")));
								jsonlast.put("screeningFlag", true);
								jsonlast.put("subsidiaries", new JSONArray());
								jsonlast.put("childIdentifier",jsonObject.getString("identifier"));	
								jsonlast.put("childLevel",jsonObject.getInt("level"));
								/////
								 if (basic.has("entity_type")) {
										entityType = basic.getString("entity_type");
										if (personTypes.contains(entityType)) {
											entityType = "person";
										} else {
											entityType = "organization";
										}
										
									}
								 
								
								 /////
								jsonlast.put("entity_type", entityType);
								jsonlast.put("isCustom", true);
								
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("dateOfBirth"))
									jsonlast.put("dateOfBirth", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("dateOfBirth"));
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("role"))
									jsonlast.put("role", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("role"));
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("classification"))
									jsonlast.put("classification", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("classification"));
								
								if(basic.has("sourceUrl"))
									jsonlast.put("sourceUrl", basic.getString("sourceUrl"));
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("source")){
									jsonlast.put("source_evidence", jsonObject.getJSONArray("parentIds")
											.getJSONObject(i).getString("source"));
								}
								if(jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).has("from")){
									jsonlast.put("from", jsonObject.getJSONArray("parentIds")
											.getJSONObject(i).getString("from"));
								}
								if(basic.has("dateOfBirth")){
									jsonlast.put("dateOfBirth", basic.getString("dateOfBirth"));
								}
								jsonlast.put("id", jsonObject.getJSONArray("parentIds")
										.getJSONObject(i).getString("parentId"));
								if(jsonObject.has("id") && map.has(jsonObject.getString("id")))
									map.getJSONObject(jsonObject.getString("id")).getJSONArray("parents").put(jsonlast.getString("id"));
								jsonlast.put("status", "completed");
								if(!map.has(jsonlast.getString("identifier")))
									map.put(jsonlast.getString("id"), jsonlast);
								holders.put(jsonlast.getString("identifier"), jsonlast);
								array.put(jsonlast);
							}
						}
					}else{
						if(parentJson.has("indircetPercentage")){
							double indirectPercentageSum=holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).getDouble("indirectPercentage")+parentJson.getDouble("indircetPercentage");
							holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).put("indirectPercentage", indirectPercentageSum);
						}
						if(parentJson.has("totalPercentage")){
							double totalPercentageSum=holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).getDouble("totalPercentage")+parentJson.getDouble("totalPercentage");
							holders.getJSONObject(jsonObject.getJSONArray("parentIds")
									.getJSONObject(i).getString("parentId")).put("totalPercentage", totalPercentageSum);
						}
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("level", jsonObject.getInt("level") + 1);
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("level", jsonObject.getInt("level") + 1);
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("childIdentifier", jsonObject.getString("identifier"));
						holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).put("childLevel",jsonObject.getInt("level"));
						jsonObject.getJSONArray("parents").put(holders.getJSONObject(jsonObject.getJSONArray("parentIds")
								.getJSONObject(i).getString("parentId")).getString("id"));

						/*holders.getJSONObject(jsonObject.getJSONArray("parentIds")
							.getJSONObject(i).getString("parentId")).getJSONArray("parents").put(jsonObject.getString("id"));*/
					}
				}
			}
		} 
	}

	private void getConflicts(JSONArray array,JSONObject map, Integer lowRange,Integer highRange) {
		List<String> ids=new ArrayList<String>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject obj = array.getJSONObject(i);
			JSONArray parentsIds = new JSONArray();
			if(obj.has("parents"))
				parentsIds = obj.getJSONArray("parents");
			double amount = 0;
			if(parentsIds!=null){
				for (int j = 0; j < parentsIds.length(); j++) {
					String parent = parentsIds.getString(j);
					JSONObject jsonObj =new JSONObject();
					if(map.has(parent))
						jsonObj =map.getJSONObject(parent);
					if (jsonObj.has("indirectPercentage") && (jsonObj.getDouble("indirectPercentage") >= lowRange
							&& jsonObj.getDouble("indirectPercentage") <= highRange)) {
						if (jsonObj.has("totalPercentage"))
							amount = amount + jsonObj.getDouble("totalPercentage");
					}
				}
			}
			if(amount>100){
				JSONArray parents=new JSONArray();
				if(obj.has("parents"))
					parents=obj.getJSONArray("parents");
				if(parents!=null){
					for (int j = 0; j < parents.length(); j++) {
						ids.add(parents.getString(j));
					}
				}
			}
		}
		if (ids != null) {
			for (int i = 0; i < array.length(); i++) {
				for (String id : ids) {
					if(id.equalsIgnoreCase(array.getJSONObject(i).getString("id"))){
						array.getJSONObject(i).put("isConflict", true);
					}
				}
			}
		}
		
	}

	public String getHighCredSource(JSONObject sourceCredibilities, List<String> officerSourceArray) {
		String source = null;
		
		JSONObject newObject = new JSONObject();
		JSONArray allNames = sourceCredibilities.names();
		for(int k = 0; k < allNames.length(); k++){
			if(officerSourceArray.contains(allNames.get(k))){
				newObject.put(allNames.getString(k), sourceCredibilities.getInt(allNames.getString((k))));
			}
		}
		int credibility = 0;
		JSONArray names = newObject.names();
		if (names != null && names.length() > 0) {
			for (int j = 0; j < names.length(); j++) {
				if (j == 0) {
					credibility = newObject.getInt(names.getString(j));
					source = names.getString(j);
				} else if (credibility < newObject.getInt(names.getString(j))) {
					credibility = newObject.getInt(names.getString(j));
					source = names.getString(j);
				}
			}
		}
		return source;
	}
	
	@Override
	public String getAnnualReturnsLink(String documentsLink) throws Exception {
		String finalUrl = null;
		List<JSONObject> mergedData = new ArrayList<JSONObject>();
		if (documentsLink != null) {
			String documentsString = getDataInAsynMode(documentsLink);
			if (documentsString != null) {
				JSONObject documentsJson = new JSONObject(documentsString);
				if (documentsJson.has("results")) {
					JSONArray results = documentsJson.getJSONArray("results");
					if (results.length() > 0) {
						JSONObject resultsJson = results.getJSONObject(0);
						if (resultsJson.has("documents")) {
							JSONObject documents = resultsJson.getJSONObject("documents");
							JSONArray sources = documents.names();
							if (sources != null && sources.length() > 0) {
								for (int i = 0; i < sources.length(); i++) {
									JSONArray documentsArray = new JSONArray();
									if (documents.get(sources.getString(i)) instanceof JSONArray)
										documentsArray = documents.getJSONArray(sources.getString(i));
									for (int j = 0; j < documentsArray.length(); j++) {
										JSONObject document =new JSONObject();
										if(documentsArray.get(j) instanceof JSONObject)
											document = documentsArray.getJSONObject(j);
										if (document.has("description")) {
											String description = document.getString("description");
											if (description.startsWith("Annual return"))
												mergedData.add(documentsArray.getJSONObject(j));
										}
									}
								}
							}
							Collections.sort(mergedData, new DateComparator());
						}
					}
				}
			}
		}
		JSONObject finalJson = null;
		if (mergedData.size() > 0)
			finalJson = mergedData.get(0);
		if (finalJson != null) {
			if (finalJson.has("url"))
				finalUrl = finalJson.getString("url");
			if (finalJson.has("Primary_source_url"))
				finalUrl = finalJson.getString("Primary_source_url");
		}
		return finalUrl;
	}
	
	@Override
	public String getHierarchyData(HierarchyDto hierarchyDto, Long userId,String apiKey) throws Exception {
		String response = null;
		if (hierarchyDto != null) {
			String url = hierarchyDto.getUrl();
			String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeoutWithApiKey(url, apiKey);
			if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
				response = serverResponse[1];
				if (!StringUtils.isEmpty(response) && serverResponse[1].trim().length() > 0) {
					if (userId == null) {
						return response;
					}
				} else {
					return response;
				}
			} else {
				return response;
			}
		} else {
			return response;
		}
		return response;
	}
	
	public void checkEntityType(JSONObject mapJson, String organisationName, String entityType) {
		if ("GB".equalsIgnoreCase(mapJson.getString("jurisdiction")) && "organization".equalsIgnoreCase(entityType)) {
			if (organisationName.toLowerCase().contains("trust"))
				mapJson.put("recognizedEntityType", "Trust");
			else if (organisationName.toLowerCase().contains("foundation"))
				mapJson.put("recognizedEntityType", "Foundation");
			else if (organisationName.toLowerCase().contains("nominee"))
				mapJson.put("recognizedEntityType", "Nominee");
			else if (organisationName.toLowerCase().contains("stichting"))
				mapJson.put("recognizedEntityType", "STAK");
			else if (organisationName.toLowerCase().contains("lp") || organisationName.toLowerCase().contains("llp")) {
				String[] arraySplit = organisationName.split(" ");
				if (arraySplit[arraySplit.length - 1].toLowerCase().contains("lp"))
					mapJson.put("recognizedEntityType", "LP");
				else if (arraySplit[arraySplit.length - 1].toLowerCase().contains("llp"))
					mapJson.put("recognizedEntityType", "LLP");
			} else
				mapJson.put("recognizedEntityType", "Standard");
		}
	}
	
}