package com.be.explorer.ownership.lambda.sourceCredibility.cloudserviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.be.explorer.ownership.lambda.rest.util.ServiceCallHelper;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudservice.IndustryCloudSerive;
import com.be.explorer.ownership.lambda.sourceCredibility.dto.SourceIndustryDto;


@Service
public class IndustryCloudServiceImpl implements IndustryCloudSerive {


	@Value("${source_management_cloud_url}")
	private String SOURCE_MANAGEMENT_CLOUD_URL;
	
	private static String genericEntity="listitems";

	@Override
	public List<SourceIndustryDto> getInsdustrySource(String type) {

		List<SourceIndustryDto> industryList = new ArrayList<>();
		String url = buildCloudUrl(type);
		JSONObject json = new JSONObject();
		try {
			String response = getCloudApiData(url);
			if (response != null) {
				json = new JSONObject(response);
			}
			if (json.has("data")) {
				JSONArray data = json.getJSONArray("data");

				for (int i = 0; i < data.length(); i++) {
					SourceIndustryDto singleDto= new SourceIndustryDto();
					JSONObject singleData= new JSONObject();		
					singleData=data.getJSONObject(i);
					if (singleData.has("item_id"))
						singleDto.setIndustryId(singleData.getLong("item_id"));
					if (singleData.has("name"))
						singleDto.setIndustryName(singleData.getString("name"));
					industryList.add(singleDto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return industryList;
	}
	

	public String getCloudApiData(String url) throws Exception {
		String response = null;
		String serverResponse[] = ServiceCallHelper.getDataFromServerWithoutTimeout(url);
		if (serverResponse[0] != null && Integer.parseInt(serverResponse[0]) == 200) {
			response = serverResponse[1];
			if (!StringUtils.isEmpty(response) && response.trim().length() > 0) {
				JSONObject json = new JSONObject();
				json = new JSONObject(response);
				if(json.has("data")) {
					return response;
				}
		} else {
			return response;
		}
		return response;
	}
		return response;
	}
	
	public String buildCloudUrl(String item_type) {
		String url=SOURCE_MANAGEMENT_CLOUD_URL+genericEntity+"?item_type="+item_type;
		return url;
	}
	

}
