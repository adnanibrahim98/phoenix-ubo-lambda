package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;

/**
 * @author suresh
 *
 */
public class SourceIndustryDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long industryId;

	private String industryName;

	public SourceIndustryDto() {
	}

	public SourceIndustryDto(String industryName) {
		super();
		this.industryName = industryName;
	}

	public Long getIndustryId() {
		return industryId;
	}

	public void setIndustryId(Long industryId) {
		this.industryId = industryId;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

}
