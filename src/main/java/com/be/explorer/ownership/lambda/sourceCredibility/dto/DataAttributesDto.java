package com.be.explorer.ownership.lambda.sourceCredibility.dto;

import java.io.Serializable;

import com.be.explorer.ownership.lambda.sourceCredibility.enumType.CredibilityEnums;



/**
 * @author suresh
 *
 */
public class DataAttributesDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long attributeId;

	private String sourceAttributeName;

	private String sourceAttributeSchemaGroup;

	private String sourceAttributeSchema;

	private CredibilityEnums credibilityValue;

	private boolean attributeSaved = false;

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public String getSourceAttributeName() {
		return sourceAttributeName;
	}

	public void setSourceAttributeName(String sourceAttributeName) {
		this.sourceAttributeName = sourceAttributeName;
	}

	public String getSourceAttributeSchemaGroup() {
		return sourceAttributeSchemaGroup;
	}

	public void setSourceAttributeSchemaGroup(String sourceAttributeSchemaGroup) {
		this.sourceAttributeSchemaGroup = sourceAttributeSchemaGroup;
	}

	public String getSourceAttributeSchema() {
		return sourceAttributeSchema;
	}

	public void setSourceAttributeSchema(String sourceAttributeSchema) {
		this.sourceAttributeSchema = sourceAttributeSchema;
	}

	public CredibilityEnums getCredibilityValue() {
		return credibilityValue;
	}

	public void setCredibilityValue(CredibilityEnums credibilityValue) {
		this.credibilityValue = credibilityValue;
	}

	public boolean isAttributeSaved() {
		return attributeSaved;
	}

	public void setAttributeSaved(boolean attributeSaved) {
		this.attributeSaved = attributeSaved;
	}

}
