package com.be.explorer.ownership.lambda.sourceCredibility.cloudservice;

import java.util.List;

import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.DataAttributesCloudDto;
import com.be.explorer.ownership.lambda.sourceCredibility.cloudDto.SaveDataAttributeCloudDto;



public interface DataAttributesCloudService {
	
	public List <DataAttributesCloudDto> getDatAttributes();
	
	public Boolean deleteDataAttributesById(Long attrId);
	
	public Boolean saveDataAttributes(SaveDataAttributeCloudDto saveCloudDto);
	
	public Boolean updateDataAttributes( SaveDataAttributeCloudDto dataAttributeDto);

}
