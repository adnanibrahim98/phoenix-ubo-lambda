package com.be.explorer.ownership.lambda.advancesearch.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author suresh
 *
 */
@Entity
@Table(name = "bst_advance_news_favourite")
public class AdvanceNewsFavourite implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "entity_id")
	private String entityId;

	@Column(name = "entity_name")
	private String entityName;

	@Column(name = "user_id")
	private Long userId;
	
	
	

	public AdvanceNewsFavourite() {
	}
	
	

	public AdvanceNewsFavourite(Long id, String entityId, String entityName, Long userId) {
		this.id = id;
		this.entityId = entityId;
		this.entityName = entityName;
		this.userId = userId;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
