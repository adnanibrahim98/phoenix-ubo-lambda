package com.be.explorer.ownership.lambda.entitysearch.service;

import java.io.IOException;

import com.be.explorer.ownership.lambda.entitysearch.dto.AnnualReturnResponseDto;

import net.sourceforge.tess4j.TesseractException;

public interface EntitySearchService {

	// String searchOrg(String query, Long size, Long from,String sort) throws
	// Exception;
	String searchOrg(String jsonString) throws Exception;

	String searchPerson(String query, Long size, Long from) throws Exception;

	// String entityLists(String entityType, String entityId) throws Exception;

	String getOrganizationOrCreateOrganizations(String jsonString) throws Exception;

	String getPersonOrCreatePerson(String jsonString) throws Exception;

	String getOrganizationBySuggestedName(String query) throws Exception;

	String getPersonBySuggestedName(String query) throws Exception;

	String storePersonAttributeInEntity(String identifier, String jsonString) throws Exception;

	String getOrganizationByIdentifier(String identifier) throws Exception;

	String getPersonByIdentifier(String identifier) throws Exception;

	String storeOrganizationAttributeInEntity(String identifier, String jsonString) throws Exception;

	String searchPerson(String jsonString) throws Exception;

	String getCognitiveMicrosoft(String q, String count, String offset, String mkt, String safeSearch) throws Exception;

	String getOrgNamesSuggestions(String query, String locationCode) throws Exception;

	String generateReport(String jsonString) throws Exception;
	
	AnnualReturnResponseDto getAnnualReports(String url, String identifier, String requestOrganisation, Long userId) throws IOException , TesseractException, Exception ;

	String getIndustryStandardCodeIsic(String url, String code) throws Exception;

	/*List<AdverseNews> getCognitiveMicrosoftWithAdvanceSearch(String q, String count, String offset, String mkt,
			String safeSearch) throws Exception;

	PeerResult getPeerGroupData(String jsonString, Long userId) throws Exception;

	List<AdverseNews> getBingNews(String identifier) throws Exception;*/

	String orgSearchWithMatchers(String jsonString) throws Exception;

	String searchPersonWithMatchers(String jsonString) throws Exception;

	/*String getArticleNewsList(ArticleDto articleDto) throws Exception;

	String getArticleNewsInfo(ArticleDto articleDto) throws Exception;*/

	// String getOrgByIdentifier(String identifier) throws Exception;
}
